# Résumés de cours de l'EPFL, section Informatique
Hello !

J'ai créé ce petit dépot afin de partager les résumés que j'ai fait pour me préprarer à la session d'examen du semestre de printemps 2022.

Vous n'y trouverez que des documents concernant Analyse II et AICC II, car je n'ai pas fait de résumés pour les autres cours, car ils s'y prêtent moins bien de mon point de vue.

Pour créer ces documents, je me suis basé très fortement sur les notes de cours en LaTeX de Joachim Favre, disponibles à l'adresse https://github.com/JoachimFavre/EPFLNotesIN/releases.

Un grand merci à lui pour son énorme travail, qui je pense sera très utile aux futures volées, et qui le fut pour moi :)

Ce que j'ai fait n'est qu'un petit remaniement de ces documents, mais qui je pense peut être utile en complément.

Le but des résumés sur ce git est d'offrir un condensé de la matière à connaître, et de permettre d'accèder rapidement aux informations, mais si vous ne parvenez pas à comprendre un point précis, vous ne trouverez probablement pas d'explication détaillée ici. Je vous invite pour cela à consulter directement le document de Joachim, très riche en explications et justifications de tout genre !

Si vous trouvez des erreurs ou avez des suggestions d'amélioration, n'hésitez pas à m'en parler sur Telegram, https://t.me/paultissot, ou ici-même.

Bonne chance pour vos examens !

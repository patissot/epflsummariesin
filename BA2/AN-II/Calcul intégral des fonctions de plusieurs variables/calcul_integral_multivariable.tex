\documentclass[a4paper]{article}

% Expanded on 2022-04-19 at 12:32:04.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}
\date{Tuesday 19 April 2022}

\begin{document}
\maketitle

\section[Calcul intégral]{Calcul intégral des fonctions de plusieurs variables}
\subsection{Intégrale sur un pavé fermé}

\parag{Définition: Pavé}{
    Un \important{pavé fermé} est un sous-ensemble de $\mathbb{R}^n$ qui est le produit Cartésien de $n$ intervalles fermés bornés: 
    \[P = \left[a_i, b_i\right] \times \left[a_2, b_2\right] \times \ldots \times \left[a_n, b_n\right], \mathspace a_i < b_i \ \forall i = 1, \ldots, n\]
    
    Nous notons le \important{pavé ouvert} par: 
    \[\mathring{P} = \left]a_1, b_1\right[ \times \ldots \times \left]a_n b_n\right[ \]
}

\parag{Définition: Volume}{
    Le \important{volume} d'un pavé fermé est défini par: 
    \[\left|P\right| = \left(b_1 - a_1\right)\left(b_2 - a_2\right)\cdots\left(b_n -a_n\right)\]
}

\parag{Définition: Subdivision}{
    Soit $\sigma_j$ une subdivision de $\left[a_j, b_j\right]$, où $a_j < b_j$.
    Nous avons donc: 
    \[\sigma_j = \left\{a_j = x_{j,0} < x_{j,1} < \ldots < x_{j, n_j} < b_j\right\}\]

    Alors, $\sigma = \left(\sigma_1, \ldots, \sigma_n\right)$ est appelée une \important{subdivision} de $P$.

    Nous notons $D\left(\sigma\right)$ la collection des pavés engendrés par la subdivision. 
}

\parag{Définition: Sommes de Darboux}{
    Soit $P$ un pavé fermé et soit $f: P \mapsto \mathbb{R}$ une fonction bornée sur $P$. Alors, on définit les sommes de Darboux de $f$ sur $P$.

    Soit $D\left(\sigma\right)$ une collection de pavés fermés engendrée par la subdivision $\sigma$. Alors: 
    \[\underline{S}_{\sigma}\left(f\right) \over{=}{déf} \sum_{Q \in D\left(\sigma\right)}^{} m\left(Q\right) \left|Q\right|, \mathspace \text{où } m\left(Q\right) = \inf_{\bvec{x} \in Q}\left(f\left(\bvec{x}\right)\right)\]
    \[\overline{S}_{\sigma}\left(f\right) \over{=}{déf} \sum_{Q \in D\left(\sigma\right)}^{} M\left(Q\right) \left|Q\right|, \mathspace \text{où } M\left(Q\right) = sup_{\bvec{x} \in Q}\left(f\left(\bvec{x}\right)\right)\]
    
    La \important{somme de Darboux inférieure} est définie par: 
    \[\underline{S}\left(f\right) \over{=}{déf} \sup\left\{\underline{S}_{\sigma}\left(f\right) : \sigma \text{ est une subdivision de } P\right\}\]
    
    La \important{somme de Darboux supérieure} est définie par: 
    \[\overline{S}\left(f\right) \over{=}{déf} \inf\left\{\overline{S}_{\sigma}\left(f\right) : \sigma \text{ est une subdivision de } P\right\}\]

}

\parag{Définition: Fonction intégrable}{
    Soit $P \subset \mathbb{R}^n$ un pavé fermé et $f: P \mapsto \mathbb{R}$ une fonction bornée.

    $f$ est \important{intégrable} sur $P$ si et seulement si: 
    \[\underline{S}\left(f\right) = \overline{S}\left(f\right)\]
    
    Dans ce cas, \important{l'intégrale de $f$ sur $P$} est définie par: 
    \[\int_P f\left(\bvec{x}\right)d \bvec{x} = \idotsint_P f\left(x_1, \ldots, x_n\right) dx_1 \ldots dx_n \over{=}{déf} \underline{S}\left(f\right) = \overline{S}\left(f\right)\]
}

\parag{Propriétés de l'intégrale}{
    \subparag{Propriété 1: Additivité}{
        Soit $P$ un pavé fermé, et $\left\{P_i\right\}_{i \in I}$ une famille dénombrable de pavés fermés disjoints telle que $P = \bigcup_{i \in I} P_i$.

        Alors, pour toute fonction continue $f: P \mapsto \mathbb{R}$, nous avons: 
        \[\int_{P} f\left(\bvec{x}\right) d \bvec{x} = \sum_{i \in I}^{} \int_{P_i} f\left(\bvec{x}\right) d \bvec{x}\]
    }

    \subparag{Propriété 2: Linéarité}{
        Soit $P$ un pavé fermé, et soient $f, g : P \mapsto \mathbb{R}$ deux fonctions continues. Alors, pour tout $\alpha, \beta \in \mathbb{R}$:
        \[\int_{P} \left(\alpha f\left(\bvec{x}\right) + \beta g\left(\bvec{x}\right)\right)d \bvec{x} = \alpha \int_{P} f\left(\bvec{x}\right)d \bvec{x} + \beta \int_{P} g\left(\bvec{x}\right) d \bvec{x}\]
    }
    
    \subparag{Propriété 3}{
        Soit $P$ un pavé fermé, et soit $f : P \mapsto\mathbb{R}$ une fonction bornée, intégrable sur $P$, et telle que: 
        \[\left|f\left(\bvec{x}\right)\right| \leq K \in \mathbb{R}_{\geq 0} \iff -K \leq f\left(\bvec{x}\right) \leq K, \mathspace \forall \bvec{x} \in P\]

        Alors: 
        \[-K \left|P\right| \leq \int_P f\left(\bvec{x}\right)d \bvec{x} \leq K \left|P\right|\]
    }
}

\parag{Définition: Volume}{
    Soit $P \subset\mathbb{R}^2$ un pavé fermé de dimension 2, et soit $f : P \mapsto \mathbb{R}_+$ une fonction intégrable. Alors, le \important{volume} de l'ensemble sous la surface $z = f\left(x, y\right) \geq0$ est défini par: 
    \[V \over{=}{déf} \iint_P f\left(x, y\right) dx dy\]
    
    En d'autres mots, $V$ est le volume du sous-ensemble entre $z = 0$ et $z = f\left(x, y\right) \geq 0$ au dessus du pavé fermé $P \subset \mathbb{R}^2$.
}

\parag{Théorème de Fubini}{
    Soit $P = \left[a_1, b_1\right] \times \ldots \times \left[a_n, b_n\right]\subset \mathbb{R}^{n}$ un pavé fermé, et soit $f: P \mapsto \mathbb{R}$ une fonction continue.

    Alors, $f$ est intégrable sur $P$, et on a: 
    \[\int_{P} f\left(\bvec{x}\right)d \bvec{x} = \int_{a_n}^{b_n} \left(\int_{a_{n-1}}^{b_{n-1}} \cdots \left(\int_{a_1}^{b_1} f\left(x_1, \ldots, x_n\right)dx_1\right) \cdots dx_{n-1}\right)dx_{n}\]

    Ceci marche pour n'importe quel choix de l'ordre d'intégration.    
}

\parag{Cas $n = 2$}{
    Soit $P = \left[a, b\right] \times \left[c, d\right] \subset \mathbb{R}^2$ un pavé fermé de dimension 2, et soit $f: P \mapsto \mathbb{R}$ une fonction continue. Alors, nous avons: 
    \[\int_{c}^{d} \left(\int_{a}^{b} f\left(x, y\right)dx\right)dy = \int_{a}^{b} \left(\int_{c}^{d} f\left(x, y\right)dy\right)dx = \iint_{P} f\left(x, y\right)dx dy\]
}

\subsection{Intégrales sur un ensemble borné}
\parag{Définition: Fonction intégrable sur un ensemble borné quelconque}{
    Soit $E \subset \mathbb{R}^n$ un ensemble borné. Puisqu'il est borné, il existe un pavé fermé tel que $E \subset P \subset\mathbb{R}^n$. Soit aussi $f :E \mapsto \mathbb{R}$ une fonction bornée sur $E$.

    Posons la fonction suivante: 
    \begin{functionbypart}{\hat{f}\left(\bvec{x}\right)}
        f\left(\bvec{x}\right), \mathspace \text{si } \bvec{x} \in E \\
        0, \mathspace \text{si } \bvec{x} \in P \setminus E
    \end{functionbypart}

    La fonction $f$ est \important{intégrable} sur $E$, si $\hat{f}$ est intégrable sur $P$. Dans ce cas, on pose: 
    \[\int_{E} f\left(\bvec{x}\right) d \bvec{x} \over{=}{déf} \int_P \hat{f}\left(\bvec{x}\right)d \bvec{x}\]
}

\parag{Définition: Frontière régulière}{
    Une frontière est dite régulière (de mesure nulle) si, pour tout $\epsilon > 0$, il existe un ensemble de pavé fermés $\left\{q_1, q_2, \ldots\right\}$ tels que: 
    \[\sum_{i \in I}^{} \left|q_i\right| < \epsilon \mathspace \text{et} \mathspace \partial E \subset \bigcup_{i \in I} q_i\]
    
    \subparag{Intuition}{
        En gros, cela veut dire que la frontière n'a pas d'aire. 
    } 
}

\parag{Théorème}{
    Si $f : E \mapsto \mathbb{R}$ est bornée sur $E$, continue sur l'intérieur $\mathring{E}$, et la frontière $\partial E$ est assez régulière, alors $f\left(\bvec{x}\right)$ est intégrable sur $E$.
}

\parag{Théorème de Fubini pour les domaines à frontière régulière}{
    \begin{enumerate}[left=0pt]
        \item Soient:
            \begin{itemize}
                \item $\left[a, b\right] \subset \mathbb{R}$ un intervalle, où $a < b$.
                \item $\phi_1, \phi_2 : \left[a, b\right]\mapsto \mathbb{R}$ deux fonctions continues telles que $\phi_1\left(x\right) < \phi_2\left(x\right)$ pour tout $x \in \left]a, b\right[ $.
                \item $D = \left\{\left(x, y\right) \in \mathbb{R}^2 : a < x < b, \phi_1\left(x\right) < y < \phi_2\left(x\right)\right\}$ (appelé le domaine à frontière régulière de type 1).
            \end{itemize}
            
            Alors, pour tout fonction continue $f: \bar{D} \mapsto \mathbb{R}$, nous avons: 
            \[\iint_D f\left(x, y\right) dx dy = \int_{a}^{b} \left(\int_{\phi_1\left(x\right)}^{\phi_2\left(x\right)} f\left(x, y\right)dy\right)dx\]

            Le choix du sens des variables d'intégration \textit{ne peut pas} se faire arbitrairement.

        \item Soient:
            \begin{itemize}
                \item $\left[c, d\right] \subset \mathbb{R}$ un intervalle, où $c < d$.
                \item $\psi_1, \psi_2 : \left[c, d\right] \mapsto \mathbb{R}$ deux fonctions continues telles que $\psi_1\left(y\right) < \psi_2\left(y\right)$ pour tout $y \in \left]c, d\right[ $.
                \item $D = \left\{\left(x, y\right) \in \mathbb{R}^2 : c < y < d, \psi_1\left(y\right) < x < \psi_2\left(y\right)\right\}$ (appelé le domaine à frontière régulière de type 2).
            \end{itemize}
            
            Alors, pour toute fonction continue $f: \bar{D} \mapsto \mathbb{R}$, nous avons: 
            \[\iint_D f\left(x, y\right)dxdy = \int_{c}^{d} \left(\int_{\psi_1\left(y\right)}^{\psi_2\left(y\right)} f\left(x, y\right)dx\right)dy\]
            Le choix du sens des variables d'intégration \textit{ne peut pas} se faire arbitrairement.
    \end{enumerate}

    \subparag{Exemple}{
        Voici un exemple de type 1 (à gauche) et un exemple de type 2 (à droite):
        \imagehere{FubiniType1Type2.png}
    }
}

\parag{Théorème de Fubini pour les intégrales triples}{
    Soient:
    \begin{itemize}
        \item Un intervalle $\left[a, b\right]$, où $a < b$.
        \item Deux fonctions $\phi_1, \phi_2 \left[a, b\right] \mapsto \mathbb{R}$ continues telles que $\phi_1\left(x\right) < \phi_2\left(x\right)$ pour tout $x \in \left]a, b\right[ $
        \item L'ensemble défini par:
        \[D = \left\{\left(x, y\right) \in \mathbb{R}^2 : a < x < b, \phi_1\left(x\right) < y < \phi_2\left(x\right)\right\}\]
        
        \item Deux fonctions $G, H : \bar{D} \mapsto \mathbb{R}$ continues telles que $G\left(x, y\right) < H\left(x, y\right)$ pour tout $\left(x, y\right) \in D$.
        \item L'ensemble défini par:
        \[E = \left\{\left(x, y, z\right) \in \mathbb{R}^3 : \left(x, y\right) \in D : G\left(x, y\right) < z < H\left(x, y\right)\right\}\]
        \item Une fonction $f: \bar{E} \mapsto \mathbb{R}$.
    \end{itemize}

    Alors, $f$ est intégrable sur $E$, et on a: 
    \[\int_{E} f\left(x, y, z\right)dxdydz = \int_{a}^{b} \left(\int_{\phi_1\left(x\right)}^{\phi_2\left(x\right)} \left(\int_{G\left(x, y\right)}^{H\left(x, y\right)} f\left(x, y, z\right)dz\right)dy\right)dx\]
    
    Nous ne pouvons pas choisir l'ordre d'intégration.
}

\subsection[Changement de variables]{Changement de variables dans une intégrale multiple}
\parag{Théorème}{
    Soit $E \subset \mathbb{R}^n$ un sous-ensemble tel que $\bar{E}$ est compact. Soit aussi $\psi : E \mapsto \mathbb{R}^n$ telle que $\psi \in C^1\left(E\right)$ et $\psi: E \mapsto \psi\left(E\right)$ est bijective (ce qui est équivalent à $J_{\psi}\left(\bvec{u}\right)$ est inversible pour tout $\bvec{u} \in E$). Soit finalement $f: \bar{D} \mapsto \bar{\psi\left(E\right)} \mapsto \mathbb{R}$ une fonction continue.

    Alors: 
    \[\int_{D} f\left(\bvec{x}\right)d \bvec{x} = \int_{E} f\left(\psi\left(\bvec{u}\right)\right) \left|\det\left(J_{\psi}\left(\bvec{u}\right)\right)\right|d \bvec{u}\]

    \imagehere[0.5]{TheoremeChangementDeVariablesIntegralesMultiples.png}
}

\parag{Application: Changement de variables polaire}{
    Le changement de variable polaire est donné par: 
    \[\psi : \mathbb{R}_+ \times \left[0, 2\pi\right[ \mapsto \mathbb{R}^2 \setminus \left\{0\right\}, \mathspace \psi\left(r, \phi\right) = \left(r\cos\left(\phi\right), r\sin\left(\phi\right)\right)\]
    
    Le déterminant de son jacobien est $J_{\psi}\left(r, \phi\right) = r$.
    Elle est bijective lorsque $r \neq 0$. 
}

\parag{Application: Changement de variables en coordonnées sphériques}{
    Le changement de variable vers les coordonnées sphériques est donné par: 
    \begin{functionbypart}{G\left(r, \theta, \phi\right)}
        x = r\sin\left(\theta\right)\cos\left(\phi\right) \\
        y = r\sin\left(\theta\right)\sin\left(\phi\right) \\
        z = r\cos\left(\theta\right)
    \end{functionbypart}
    où $G : \left]0, +\infty\right[ \times \left[0, \pi\right] \times \left[0, 2\pi\right[  \mapsto \mathbb{R}^3 \setminus \left\{0\right\}$.

    Le déterminant de son jacobien est $\left|\det J_{G}\left(r, \theta, \phi\right)\right| = r^2 \sin\left(\theta\right)$

    Quand $r > 0$ et $\sin\left(\theta\right) > 0$ (c'est-à-dire $\theta \neq 0$ et $\theta \neq \pi$), alors $G$ est bijective.

    \imagehere[0.3]{ChangementDeVariablesCoordonneesSpheriques.png}


}

\parag{Application: Changement de variables en coordonnées cylindriques}{
    Le changement de variable vers les coordonnées cylindrique est donné par: 

    \begin{functionbypart}{G\left(r, \phi, z\right)}
        x = r\cos\left(\phi\right) \\
        y = r\sin\left(\phi\right) \\
        z = z
    \end{functionbypart}
    où $G : \left[0, +\infty\right[ \times \left[0, 2\pi\right[ \times \mathbb{R} \mapsto \mathbb{R}^3$.

    Le déterminant de son jacobien est $J_{G}\left(r, \phi, z\right) = r$.

    \imagehere[0.3]{ChangementDeVariableCylindrique.png}

}




\end{document}

\documentclass[a4paper]{article}

% Expanded on 2022-04-22 at 19:34:26.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Fonctions à valeurs dans $\mathbb{R}^{n}$}

\parag{Introduction}{
    Nous considérons les fonctions à valeurs dans $\mathbb{R}^m$, $\bvec{f} : E \mapsto \mathbb{R}^m$ où $E \subset \mathbb{R}^n$:
    \[\bvec{f}\left(\bvec{x}\right) = \begin{pmatrix} f_1\left(\bvec{x}\right) \\ \vdots \\ f_m\left(\bvec{x}\right) \end{pmatrix} \in \mathbb{R}^m\]
    
    Chaque composante $f_i$ est une fonction réelle de $n$ variables réelles.
}

\parag{Définition: $k$-ème dérivée partielle}{
    Soit $E \subset \mathbb{R}^n$.

    La \important{$k$-ème dérivée partielle} de $\bvec{f}: E \mapsto \mathbb{R}^m$ en $\bvec{a} \in E$, est définie par: 
    \[\frac{\partial \bvec{f}}{\partial x_k}\left(\bvec{a}\right) \over{=}{déf} \begin{pmatrix} \frac{\partial f_1}{\partial x_k}\left(\bvec{a}\right) \\ \vdots \\ \frac{\partial f_m}{\partial x_k}\left(\bvec{a}\right) \end{pmatrix}\]
    si chacune des fonctions $f_1, \ldots, f_m$ admet la dérivée partielle $\frac{\partial}{\partial x_k}$ en $\bvec{a}$.
}

\parag{Définition: Dérivée directionnelle}{
    Soit $E \subset \mathbb{R}^n$, et soit $\bvec{v} \in \mathbb{R}^n$ tel que $\bvec{v} \neq \bvec{0}$. 

    La \important{dérivée directionnelle} de $\bvec{f}: E \mapsto \mathbb{R}^m$ suivant $\bvec{v}$ en $\bvec{a} \in E$, est: 
    \[D \bvec{f}\left(\bvec{a}, \bvec{v}\right) \over{=}{déf} \begin{pmatrix} Df_1\left(\bvec{a}, \bvec{v}\right) \\ \vdots \\ Df_m\left(\bvec{a}, \bvec{v}\right) \end{pmatrix}\]
    si $Df_i\left(\bvec{a}, \bvec{v}\right)$ existent pour tout $i = 1, \ldots, m$.
}

\parag{Définition: Limite}{
    Soit $E \subset \mathbb{R}^{n}$. 

    Une fonction $\bvec{f} : E \mapsto \mathbb{R}^m$ admet $\bvec{\ell} \in \mathbb{R}^m$ pour \important{limite} lorsque $\bvec{x}$ tend vers $\bvec{a}$ si $\forall \epsilon > 0$, $\exists \delta > 0$ tel que pour tout $\bvec{x} \in E$, on a: 
    \[0 < \left\|\bvec{x} - \bvec{a}\right\| \leq \delta \implies \left\|f\left(\bvec{x}\right) - \bvec{\ell}\right\| \leq \epsilon\]
    \subparag{Remarque}{
        En particulier, nous avons: 
        \[\lim_{\bvec{x} \to \bvec{a}} \bvec{f}\left(\bvec{x}\right) = \begin{pmatrix} \lim\limits_{\bvec{x} \to \bvec{a}} f_1\left(\bvec{x}\right) \\ \vdots \\ \lim\limits_{\bvec{x} \to \bvec{a}} f_m\left(\bvec{x}\right) \end{pmatrix} \]        
    }
}

\parag{Définition: Dérivabilité}{
    Soit $E \subset \mathbb{R}^n$.

    $\bvec{f}: E \mapsto \mathbb{R}^m$ est \important{dérivable} au point $\bvec{a} \in E$ s'il existe une transformation linéaire $\bvec{L}_{\bvec{a}}: \mathbb{R}^n \mapsto \mathbb{R}^m$ et une fonction $\bvec{r}: E \mapsto \mathbb{R}^m$ telles que: 
    \[\bvec{f}\left(\bvec{x}\right) = \bvec{f}\left(\bvec{a}\right) + \bvec{L}_{\bvec{a}}\left(\bvec{x} - \bvec{a}\right) + \bvec{r}\left(\bvec{x}\right)\]

    De plus, il faut aussi que: 
    \[\lim_{\bvec{x} \to \bvec{a}} \frac{\bvec{r}\left(\bvec{x}\right)}{\left\|\bvec{x} - \bvec{a}\right\|} = 0\]
    
    Si $\bvec{f}$ est dérivable, alors $\bvec{L}_{\bvec{a}}: \mathbb{R}^n \mapsto \mathbb{R}^m$ est appelée la \important{différentielle} de $\bvec{f}$ en $\bvec{a}$.
}

\parag{Proposition: Dérivabilité pour chaque composante}{
    Soit $E \subset \mathbb{R}^n$.

    $\bvec{f} = \left(f_1, \ldots, f_m\right): E \mapsto \mathbb{R}^m$ est dérivable en $\bvec{a} \in E$ \textit{si et seulement si} chaque composante $f_i : E \mapsto \mathbb{R}$ est dérivable en $\bvec{a} \in E$ pour $i = 1, \ldots, m$. De plus, nous pouvons construire: 
    \[\bvec{L}_{\bvec{a}}\left(\bvec{v}\right) = \begin{pmatrix} L_{1, \bvec{a}}\left(\bvec{v}\right) \\ \vdots \\ L_{m, \bvec{a}}\left(\bvec{v}\right) \end{pmatrix}, \mathspace \bvec{v} \in \mathbb{R}^n, \bvec{v} \neq \bvec{0}\]
    où $L_{i, \bvec{a}}\left(\bvec{v}\right)$ est la différentielle de $f_i$ calculée en $\bvec{a}$ et appliquée en $\bvec{v}$. En d'autres mots: 
    \[L_{i, \bvec{a}}\left(\bvec{v}\right) = D f_i\left(\bvec{a}, \bvec{v}\right) = \left<\nabla f_i\left(\bvec{a}\right), \bvec{v}\right>\]

}

\parag{Définition: Matrice Jacobienne}{
    Soit $E \subset \mathbb{R}^n$. Si $\bvec{f}: E \mapsto \mathbb{R}^m$ possède toutes ses dérivées partielles en $\bvec{a}\in E$, alors sa \important{matrice Jacobienne} (matrice de Jacobi) est définie par:
    \[J_{\bvec{f}}\left(\bvec{a}\right) \over{=}{déf} \begin{pmatrix} \dfrac{\partial f_1}{\partial x_1}\left(\bvec{a}\right) & \dfrac{\partial f_1}{\partial x_2}\left(\bvec{a}\right) & \cdots & \dfrac{\partial f_1}{\partial x_n}\left(\bvec{a}\right) \\ \dfrac{\partial f_2}{\partial x_1}\left(\bvec{a}\right) & \dfrac{\partial f_2}{\partial x_2}\left(\bvec{a}\right) & \cdots & \dfrac{\partial f_2}{\partial x_n}\left(\bvec{a}\right) \\ \vdots & \vdots & \ddots & \vdots \\ \dfrac{\partial f_m}{\partial x_1}\left(\bvec{a}\right) & \dfrac{\partial f_m}{\partial x_2}\left(\bvec{a}\right) & \cdots & \dfrac{\partial f_m}{\partial x_n}\left(\bvec{a}\right) \end{pmatrix} = \begin{pmatrix} \nabla f_1\left(\bvec{a}\right) \\ \nabla f_2\left(\bvec{a}\right) \\ \vdots \\ \nabla f_m\left(\bvec{a}\right) \end{pmatrix}\]

    \subparag{Observations}{
         \begin{itemize}
            \item Si $g : \mathbb{R}^n \mapsto \mathbb{R}$, alors $J_g\left(\bvec{x}\right) = \nabla g\left(\bvec{x}\right)$.
            \item Si $g: \mathbb{R} \mapsto \mathbb{R}^n$, alors $J_{\bvec{g}}\left(x\right) = \frac{\partial \bvec{g}}{\partial x}$.
            \item Si $g: \mathbb{R}\mapsto\mathbb{R}$, alors $J_g\left(x\right) = g'\left(x\right)$.
        \end{itemize}
    }
    

    \subparag{Remarque 1}{
        Si $\bvec{f}$ est dérivable en $\bvec{a} \in E$, alors nous avons: 
        \[J_{\bvec{f}}\left(\bvec{a}\right) = \begin{pmatrix} L_{1, \bvec{a}}\left(\bvec{e_1}\right) & \cdots & L_{1, \bvec{a}}\left(\bvec{e_n}\right) \\ \vdots & \ddots & \vdots \\ L_{m, \bvec{a}}\left(\bvec{e_1}\right)  & \cdots & L_{m, \bvec{a}}\left(\bvec{e_n}\right) \end{pmatrix} \]
        
        Ainsi, si $\bvec{f}$ est dérivable en $\bvec{a}$, la matrice nous donne la matrice de la différentielle de $\bvec{f}$.
    }

    \subparag{Remarque 2}{
        Si $\bvec{f}$ est dérivable, alors:
        \begin{multiequality}
        D \bvec{f}\left(\bvec{a}, \bvec{v}\right) =\ & \left(J_{\bvec{f}}\left(\bvec{a}\right)\right)\cdot \bvec{v}
        \end{multiequality}
    }
}

\parag{Définition: Jacobien}{
    Soit $E \subset \mathbb{R}^n$. Si $\bvec{f}: E \mapsto \mathbb{R}^m$ possède toutes ses dérivées partielles en $\bvec{a}\in E$, et si $m = n$, alors on définit le \important{déterminant de Jacobi}, aussi appelé \important{le Jacobien}, de $\bvec{f}$ en $\bvec{a}$ comme: 
    \[\left|J_{\bvec{f}}\left(\bvec{a}\right)\right| = \det\left(J_{\bvec{f}}\left(\bvec{a}\right)\right) \over{=}{déf} \det\begin{pmatrix} \dfrac{\partial f_1}{\partial x_1}\left(\bvec{a}\right) & \cdots & \dfrac{\partial f_1}{\partial x_n}\left(\bvec{a}\right) \\ \vdots & \ddots & \vdots \\ \dfrac{\partial f_m}{\partial x_1}\left(\bvec{a}\right) & \cdots & \dfrac{\partial f_m}{\partial x_n}\left(\bvec{a}\right) \end{pmatrix}\]
}

\parag{Théorème}{
    Soient $A, B$ deux ensembles tels que $A \subset \mathbb{R}^n$ et $\bvec{g}\left(A\right) \subset B \subset \mathbb{R}^p$. Soient $\bvec{g}: A \mapsto \mathbb{R}^p$ et $\bvec{f} : B \mapsto \mathbb{R}^q$. En d'autres mots, nous avons: 
    \[\mathbb{R}^n \over{\mapsto}{$\bvec{g}$} \mathbb{R}^p \over{\mapsto}{$\bvec{f}$} \mathbb{R}^q\]
    
    Soient $\bvec{a} \in A$ et $\bvec{b} = \bvec{g}\left(\bvec{a}\right) \in B$. Supposons que $\bvec{g}$ est dérivable en $\bvec{a}$ avec la différentielle $L_{\bvec{g}, \bvec{a}}$ et $\bvec{f}$ est dérivable en $\bvec{b}$ avec la différentielle $L_{\bvec{f}, \bvec{b}}$.

    \vspace{1em}

    Alors $\bvec{f} \circ \bvec{g}$ est dérivable en $\bvec{a}$, et on a:
    \begin{enumerate}
        \item $\displaystyle \bvec{L}_{\bvec{f} \circ \bvec{g}, \bvec{a}} = \bvec{L}_{\bvec{f}, \bvec{b}} \circ \bvec{L}_{\bvec{g}, \bvec{a}}$
        \item $\displaystyle J_{\bvec{f}\circ \bvec{g}}\left(\bvec{a}\right) = J_{\bvec{f}}\left(\bvec{g}\left(\bvec{a}\right)\right) \cdot J_{\bvec{g}}\left(\bvec{a}\right)$
        \item Si $n = p = q$, alors $\displaystyle \left|J_{\bvec{f} \circ \bvec{g}}\left(\bvec{a}\right)\right| = \left|J_{\bvec{f}}\left(\bvec{g}\left(\bvec{a}\right)\right)\right|\cdot \left|J_{\bvec{g}}\left(\bvec{a}\right)\right|$
    \end{enumerate}
}

\parag{Application: Changement de variable}{
    Supposons que nous avons le schéma de fonction suivant:
    \[\mathbb{R}^n \over{\mapsto}{$\bvec{h}$} \mathbb{R}^n \over{\mapsto}{$\bvec{g}$} \mathbb{R}^n\]
    tels que $\bvec{h}$ est un changement de variable et $\bvec{g}$ est sa fonction réciproque, i.e.:
    \[\bvec{g} \circ \bvec{h}\left(x_1, \ldots, x_n\right) = \left(x_1, \ldots, x_n\right)\]

    Nous savons donc que le Jacobien de notre composée est:
    \[J_{\bvec{g} \circ \bvec{h}} = \begin{pmatrix} 1 & 0 & \cdots & 0 \\ 0 & 1 & \cdots & 0 \\ \vdots & \vdots & \ddots & \vdots \\ 0 & 0 & \cdots & 1 \end{pmatrix} = I_{n\times n}\]

    Supposons maintenant aussi que $\bvec{h}$ et $\bvec{g}$ sont dérivables sur leurs domaines. Par le théorème de la fonction composée, nous obtenons:
    \begin{multiequation}
    & J_{\bvec{g}}\left(\bvec{h}\left(\bvec{a}\right)\right) \cdot J_{\bvec{h}}\left(\bvec{a}\right) = I_{n \times n} \\
    \implies & J_{\bvec{g}}\left(\bvec{h}\left(\bvec{a}\right)\right) = \left(J_{\bvec{h}}\left(\bvec{a}\right)\right)^{-1} \text{ et } \det\left(J_{\bvec{g}}\right) \det\left(J_{\bvec{h}}\right) = 1
    \end{multiequation}

    Puisqu'une matrice est bijective si et seulement si elle est inversible, nous en déduisons donc la proposition suivante.

    \subparag{Proposition}{
        Soit $\bvec{g} : \mathbb{R}^n \mapsto \mathbb{R}^n$ une fonction dérivable en $\bvec{a}$. $\bvec{g}$ est bijective dans un voisinage de $\bvec{a}$ si et seulement si $\det\left(J_{\bvec{g}}\left(\bvec{a}\right)\right)\neq 0$.
    }
}

\subsection{Dérivée d'une intégrale qui dépend d'un paramètre}
\parag{Théorème}{
    Soit $I \subset \mathbb{R}$ un ensemble ouvert, soit $f: \left[a, b\right] \times I \mapsto \mathbb{R}$ telle que $\frac{\partial f}{\partial y}$ est continue sur $\left[a, b\right] \times I$, et soit:
    \[g\left(y\right) = \int_{a}^{b} f\left(x, y\right)dx\]

    Alors, $g\left(y\right)$ est de classe $C^1$ sur $I$, et nous avons:
    \[g'\left(y\right) = \int_{a}^{b} \frac{\partial f}{\partial y}\left(x, y\right)dx, \mathspace \forall y \in I\]

}

\parag{Rappel: Théorème Fondamental du calcul intégral}{
    Soit $f$ une fonction continue. Alors, nous avons:
    \[\frac{d}{dt}\left(\int_{a}^{t} f\left(y\right)dy\right) = f\left(t\right), \mathspace \frac{d}{dt}\left(\int_{t}^{b} f\left(y\right)dx\right) = \frac{d}{dt}\left(- \int_{b}^{t} f\left(y\right) dx\right) = -f\left(t\right)\]
}

\parag{Théorème}{
    Soient $I, J \subset \mathbb{R}$ deux en ensembles ouverts, soient $g, h : I \mapsto \mathbb{R}$ des fonctions continûment dérivables, et soit $f : J \times I \mapsto \mathbb{R}$ une fonction telle que $\frac{\partial f}{\partial t}\left(x, t\right)$ est continue sur $I$. Finalement, soit: 
    \[A\left(t\right) = \int_{h\left(t\right)}^{g\left(t\right)} f\left(x, t\right)dx\]
    
    Alors, $a\left(t\right)$ est continûment dérivable sur $I$, et on a: 
    \[A'\left(t\right) = f\left(g\left(t\right), t\right) g'\left(t\right) - f\left(h\left(t\right), t\right)h'\left(t\right) + \int_{h\left(t\right)}^{g\left(t\right)} \frac{\partial f}{\partial t}\left(x, t\right)dt \]
    
}

\subsection{Application du gradient et du Laplacien en coordonnées polaires}
\parag{Définition: Laplacien}{
    Soit $E \subset \mathbb{R}^n$ un ensemble, et soit $f : E \mapsto \mathbb{R}$ de classe $C^2$ sur $E$.

    La fonction $\Delta f : E \mapsto \mathbb{R}$ suivante est le \important{Laplacien} de $f$: 
    \[\Delta f\left(x_1, \ldots, x_n\right) = \frac{\partial^{2} f}{\partial x_1^{2}} + \ldots + \frac{\partial^{2} f}{\partial x_n^{2}}\]
}

\parag{Proposition}{
    Soit $f : \mathbb{R}^2 \mapsto\mathbb{R}$ une fonction de classe $C^2$, et soit $\widetilde{f} = f \circ g\left(r, \phi\right)$, où $g$ est la fonction changement de variable vers les coordonnées polaires. Alors: 
    \[\nabla f\left(x, y\right) = \left(\cos\left(\phi\right) \frac{\partial \widetilde{f}}{\partial r} - \frac{1}{r} \sin\left(\phi\right) \frac{\partial \widetilde{f}}{\partial \phi}, \sin\left(\phi\right) \frac{\partial \widetilde{f}}{\partial r} + \frac{1}{r} \cos\left(\phi\right) \frac{\partial \widetilde{f}}{\partial \phi}\right)\]

    \[\Delta f\left(x, y\right) = \frac{\partial^{2} \widetilde{f}}{\partial r^{2}} + \frac{1}{r^2} \frac{\partial^{2} \widetilde{f}}{\partial \phi^{2}} + \frac{1}{r} \frac{\partial f}{\partial r}\]    
}

\parag{Définition: Fonctions harmoniques}{
    Une fonction telle que $\Delta f = 0$ sur $E \subset \mathbb{R}^2$ s'appelle \important{harmonique}.
}

\parag{Proposition}{
    Une fonction harmonique sur un domaine compact atteint son minimum et son maximum sur la frontière du domaine.
}

\subsection{Formule de Taylor}
\parag{Théorème}{
    Soit $E \subset \mathbb{R}^n$, et soit $f : E \mapsto \mathbb{R}$ une fonction de classe $C^{p+1}$ au voisinage de $\bvec{a} \in E$. 

    Alors, il existe $\delta > 0$ tel que, pour tout $\bvec{x} \in B\left(\bvec{a}, \delta\right) \cap E$, il existe $0 < \theta < 1$ tel que: 
    \[f\left(\bvec{x}\right) = F\left(0\right) + F'\left(0\right) + \ldots + \frac{1}{p!} F^{\left(p\right)}\left(0\right) + \frac{1}{\left(p+1\right)!}F^{\left(p+1\right)}\left(\theta\right)\]
    où $F: I \mapsto \mathbb{R}$, avec $I \subset \left[0, 1\right]$, est définie par $F\left(t\right) = f\left(\bvec{a} + t\left(\bvec{x} - \bvec{a}\right)\right)$.
    
    \subparag{Terminologie}{
        Ce théorème nous dit que nous pouvons écrire:
        \[f\left(\bvec{x}\right) = F\left(0\right) + F'\left(0\right) + \ldots + \frac{1}{p!} F^{\left(p\right)}\left(0\right) + \text{ le reste}\]

        Nous l'appelons le \important{polynôme de Taylor} de $f$ d'ordre $p$ au point $\bvec{a}$.
    }
}

\parag{Cas où $n = 2$}{
    Soit $\bvec{a} = \left(a, b\right), \bvec{x} = \left(x, y\right)$ et $f\left(x, y\right): E \mapsto \mathbb{R}$ une fonction de classe $C^{p+1}$ où $p \geq 2$. On peut calculer $F^{\left(p\right)}\left(0\right)$ avec la formule:
    \[F^{\left(p\right)}\left(0\right) = \sum_{k=0}^{p} \frac{\partial^p f}{\partial x^k \partial y^{p - k}} C_p^k \left(x - a\right)^{k} \left(y - b\right)^{p - k}\]
    
    Souvent, on utilise l'approximation de Taylor d'ordre 2, donnée par: 
    \begin{multiequality}
    f\left(x, y\right) =\ & f\left(a, b\right) + \frac{\partial f}{\partial x}\left(a, b\right)\left(x - a\right) + \frac{\partial f}{\partial y}\left(a, b\right)\left(y - b\right)  \\
     & + \frac{1}{2}\left(\frac{\partial^2 f}{\partial x^2}\left(a, b\right)\left(x - a\right)^2 + 2 \frac{\partial^2 f}{\partial x \partial y}\left(a, b\right)\left(x - a\right)\left(y- b\right) + \frac{\partial^2 f}{\partial y^2}\left(a, b\right)\left(y - b\right)^2\right) \\
     & + \epsilon\left(\left\|\left(x, y\right) - \left(a, b\right)\right\|^2\right) 
    \end{multiequality}    
}

\parag{Cas où $n = 3$}{
    Nous avons $f\left(x, y, z\right)$, une fonction de classe $C^3$, et nous voulons calculer son développement limité autour de $\left(a, b, c\right) \in E \subset \mathbb{R}^3$.

    Nous avons: 
    \[F'\left(0\right) = \frac{\partial f}{\partial x}\left(a, b, c\right)\left(x - a\right) + \frac{\partial f}{\partial y}\left(a, b, c\right)\left(y - b\right) + \frac{\partial f}{\partial z}\left(a, b, c\right)\left(z - c\right)\]
    
    Ainsi que:
    \begin{multiequality}
    F''\left(0\right) =\ & \frac{\partial^{2} f}{\partial x^{2}}\left(a, b, c\right)\left(x - a\right)^2 + \frac{\partial^{2} f}{\partial y^{2}}\left(a, b, c\right)\left(y - b\right)^2 + \frac{\partial^{2} f}{\partial z^{2}}\left(a, b, c\right)\left(z - c\right)^2  \\
     & + 2 \frac{\partial^2 f}{\partial x \partial y}\left(x - a\right)\left(y - b\right) + 2 \frac{\partial^2 f}{\partial y \partial z}\left(y - b\right)\left(z - c\right) + 2 \frac{\partial^2 f}{\partial x \partial z}\left(x - a\right)\left(z - c\right) 
    \end{multiequality}    
}

\subsection{Extrema d'une fonction de plusieurs variables}
\parag{Définition: Point stationnaire}{
    Soit $E \subset \mathbb{R}^n$ et $f : E \mapsto \mathbb{R}$.

    $\bvec{a} \in E$ est un \important{point stationnaire} de $f$ si et seulement si: 
    \[\nabla f\left(\bvec{a}\right) = \left(\frac{\partial f}{\partial x_1}\left(\bvec{a}\right), \ldots, \frac{\partial f}{\partial x_n}\left(\bvec{a}\right)\right) = \bvec{0}\]
}

\parag{Définition: Maximum local}{
    $f: E \mapsto \mathbb{R}$ admet un \important{maximum local} au point $\bvec{a} \in E$ s'il existe $\delta > 0$ tel que $f\left(\bvec{x}\right) \leq f\left(\bvec{a}\right)$ pour tout $\bvec{x} \in E \cap B\left(\bvec{a}, \delta\right)$.
}

\parag{Définition: Minimum local}{
    $f: E \mapsto \mathbb{R}$ admet un \important{minimum local} au point $\bvec{a} \in E$ s'il existe $\delta > 0$ tel que ${\color{red}f\left(\bvec{x}\right) \geq f\left(\bvec{a}\right)}$ pour tout $\bvec{x} \in E \cap B\left(\bvec{a}, \delta\right)$.
}

\parag{Proposition: Condition nécessaire pour un extremum local}{
    Soit $E \subset \mathbb{R}^n$ et $f: E \mapsto \mathbb{R}$ une fonction admettant un extremum local au point $\bvec{a} \in E$ et telle que $\frac{\partial f}{\partial x_i}\left(\bvec{a}\right)$ existent $\forall i = 1, \ldots, n$. 

    Alors, $\bvec{a}$ est un point stationnaire de $f$, i.e. $\nabla f\left(\bvec{a}\right) = \bvec{0}$.
}

\parag{Définition: Point critique}{
    $\bvec{a} \in E$ est un \important{point critique} de $f: E \mapsto \mathbb{R}$ si $\bvec{a}$ est un point stationnaire, ou si au moins une des dérivées partielles de $f$ n'existe pas en $\bvec{x} = \bvec{a}$.

    \subparag{Remarque}{
        En utilisant notre théorème, nous avons la proposition suivante:

        Si $\bvec{a}$ est un point d'extremum local, alors $\bvec{a}$ est un point critique.
    }
}

\parag{Théorème: Condition suffisante pour un extremum local}{
    Soit $f: E \mapsto \mathbb{R}$ une fonction de classe $C^2$ sur $E$, et soit $\bvec{a} \in E$ un point stationnaire ($\nabla f\left(\bvec{a}\right) = \bvec{0}$).

    Si toutes les valeurs propres de la matrice Hessienne de $f$ en $\bvec{a}$ sont strictement positives (\important{$> 0$}), alors $f$ possède un \important{minimum} local en $\bvec{a}$.

    Si toutes les valeurs propres de la matrice Hessienne de $f$ en $\bvec{a}$ sont strictement négatives (\important{$< 0$}), alors $f$ possède un \important{maximum} local en $\bvec{a}$.

    S'il y a au moins une valeur propre strictement négative et au moins une strictement positive (\important{$< 0, \ldots, >0$}), alors $\bvec{a}$ n'est \important{pas un point d'extremum local}. 
}

\parag{Proposition: Hypothèses équivalentes pour le théorème de la condition suffisante pour un extremum local quand $n = 2$}{
    Dans le cas où $n = 2$, nous pouvons réécrire les conditions de notre théorème. 

    Notre matrice Hessienne est donnée par: 
    \[\Hess_f\left(\bvec{a}\right) = \begin{pmatrix} \frac{\partial^{2} f}{\partial x^{2}} & \frac{\partial^2 f}{\partial y \partial x} \\ \frac{\partial^2 f}{\partial x \partial y} & \frac{\partial^{2} f}{\partial y^{2}}  \end{pmatrix} = \begin{pmatrix} r & s \\ s & t \end{pmatrix} \]
    
    Nous avons les équivalences suivantes:
    \begin{enumerate}
        \item $\lambda_1 > 0, \lambda_2 > 0 \iff \det \Hess_f\left(\bvec{a}\right) > 0 \text{ et } r > 0$ 
        \item $\lambda_1 < 0, \lambda_2 < 0 \iff \det \Hess_f\left(\bvec{a}\right) > 0 \text{ et } r < 0$ 
        \item $\lambda_1 > 0, \lambda_2 < 0 \text{ ou } \lambda_1 < 0, \lambda_2 > 0 \iff \det \Hess_f\left(\bvec{a}\right) < 0$ 
    \end{enumerate}
}

\parag{Résumé du cas $n = 2$}{
    Soit $f$ une fonction de classe $C^2$ au voisinage de $\bvec{a} = \left(a_1, a_2\right)$, et soit sa matrice Hessienne: 
    \[\Hess_f\left(\bvec{a}\right) = \begin{pmatrix} r & s \\ s & t \end{pmatrix} \]
    
    \begin{enumerate}[left=0pt]
        \item Si $\det\Hess_f\left(\bvec{a}\right) = rt - s^2 > 0$ et $r > 0$, alors nous avons un minimum local: 
        \[f\left(u, v\right) - f\left(\bvec{a}\right) > 0\]
        \item Si $\det\Hess_f\left(\bvec{a}\right) = rt - s^2 > 0$ et $r < 0$, alors nous avons un un maximum local: 
        \[f\left(u, v\right) - f\left(\bvec{a}\right) < 0\]
    \item Si $\det\Hess_f\left(\bvec{a}\right) = rt - s^2 < 0$, alors il existe $u, v$ dans tout voisinage de $\bvec{a}$ tels que nous n'avons pas d'extremum local.    
    \item Si $\det\Hess_f\left(\bvec{a}\right) = 0$, alors nous n'avons pas de conclusion.
    \end{enumerate}
}

\parag{Conditions équivalentes aux conditions suffisantes pour $n = 3$}{
    Soit $f$ une fonction de classe $C^2$ au voisinage de $\bvec{a}$, et soit sa matrice Hessienne: 
    \[\Hess_f\left(\bvec{a}\right) = \begin{pmatrix} \frac{\partial^{2} f}{\partial x_1^{2}} & \frac{\partial^2 f}{\partial x_2 \partial x_1} & \frac{\partial^2 f}{\partial x_3 \partial x_1} \\ \frac{\partial^2 f}{\partial x_1 \partial x_2} & \frac{\partial^{2} f}{\partial x_1^{2}} & \frac{\partial^2 f}{\partial x_3 \partial x_2} \\ \frac{\partial^2 f}{\partial x_1 \partial x_3} & \frac{\partial^2 f}{\partial x_2 \partial x_3} & \frac{\partial^{2} f}{\partial x_3^{2}} \end{pmatrix} \]
    
    Nous définissons $\Delta_1$ le déterminant de la matrice $1\times1$ avec le coin en haut à gauche (mineure principale de taille $1 \times 1$), $\Delta_2$ le déterminant de la matrice $2 \times 2$ avec le coin au même endroit (mineure principale de taille $2 \times 2$), et $\Delta_3 = \det\Hess_f\left(\bvec{a}\right)$:
    \imagehere[0.3]{MineuresPrincipalesMatriceHessienne.png}

    Nous avons donc:
    \[\Delta_1 = \frac{\partial^{2} f}{\partial x_1^{2}}, \mathspace \Delta_2 = \frac{\partial^{2} f}{\partial x_1^{2}} \cdot \frac{\partial^{2} f}{\partial x_2^{2}} - \frac{\partial^2 f}{\partial x_2 \partial x_1} \cdot \frac{\partial^2 f}{\partial x_1 \partial x_2}, \mathspace \Delta_3 = \det\Hess_f\]
    
    \begin{enumerate}[left=0pt]
        \item Si $\Delta_1 > 0, \Delta_2 > 0, \Delta_3 > 0$, alors $\bvec{a}$ est un point de minimum local (et $\Hess_f\left(\bvec{a}\right)$ est dite définie positive).
        \item Si $\Delta_1 < 0, \Delta_2 > 0, \Delta_3 < 0$, alors $\bvec{a}$ est un point de maximum local.
        \item Autrement, si $\Delta_3 \neq 0$, alors il n'y a pas d'extremum local en $\bvec{a}$.
        \item Si $\Delta_3 = 0$, alors nous ne pouvons rien conclure.
    \end{enumerate}
}


\subsection[Min et max sur un compact]{Minimum et maximum d'une fonction continue sur un compact}

\parag{Rappel}{
    Une fonction continue sur un sous-ensemble compact $D \subset \mathbb{R}^n$ atteint son minimum et son maximum. En d'autres mots, $\exists \bvec{c_1}, \bvec{c_2}$ tels que: 
    \[f\left(\bvec{c_1}\right) = \min_{\bvec{x} \in D} f\left(\bvec{x}\right), \mathspace f\left(\bvec{c_2}\right) = \max_{\bvec{x} \in D} f\left(\bvec{x}\right)\]
}

\parag{Méthode}{
    Voici une méthode pour trouver ces $\bvec{c_1}, \bvec{c_2}$.
    \begin{enumerate}
       
        \item Trouver les points critiques $\left\{\bvec{c_i}\right\}$ de $f$ sur $\mathring{D}$ (l'intérieur de
            $D$). Calculer les valeurs $f\left(\bvec{c_i}\right)$.
        \item Trouver les points $\left\{\bvec{d_j}\right\}$ de minimum et maximum de $f\left(\partial D\right)$ ($\partial D$ est la frontière de $D$). Calculer les valeurs $f\left(\bvec{d_j}\right)$.
        \item Choisir le minimum et le maximum parmi les valeurs qu'on a trouvées.
    \end{enumerate}
}

\subsection{Théorème des fonctions implicites}
\parag{Définition: Fonction implicite}{
    Une \important{fonction implicite} est une dépendance $f = f\left(\bvec{x}\right)$ qui est définie par une équation.
}

\parag{Définition: Surface de niveau}{
    Une \important{surface de niveau} d'une fonction $F\left(x, y, z\right)$ est la surface définie par l'équation $F\left(x, y, z\right) = C \in \mathbb{R}$. $C$ s'appelle le \important{niveau}.

    De manière similaire, une \textcolor{red}{ligne} de niveau d'une fonction ${\color{red}F\left(x, y\right)}$ est la \textcolor{red}{ligne} définie par l'équation ${\color{red}F\left(x, y\right)} = C \in \mathbb{R}$. $C$ s'appelle le niveau.
}

\parag{Théorème des fonctions implicites (TFI)}{
    Soit $n \geq 2$ et $E \subset \mathbb{R}^n$. Soit aussi $F: E \mapsto \mathbb{R}$ une fonction de classe $C^1$ au voisinage de $\bvec{a} = \left(a_1, \ldots, a_n\right) \in E$ telle que:
    \begin{enumerate}
        \item $F\left(\bvec{a}\right) = 0$
        \item $\frac{\partial F}{\partial x_n}\left(\bvec{a}\right)\neq 0$
    \end{enumerate}

    Alors, il existe un voisinage $B\left(\bcheck{a}, \delta\right)$ de $\bcheck{a} = \left(a_1, \ldots, a_{n-1}\right) \in \mathbb{R}^{n-1}$ et une fonction $f: B\left(\bcheck{a}, \delta\right) \mapsto\mathbb{R}$ telle que:
    \begin{enumerate}
        \item $a_n = f\left(a_1, \ldots, a_{n-1}\right)$
        \item $F\left(x_1, \ldots, x_{n-1}, f\left(x_1, \ldots, x_{n-1}\right)\right) = 0, \mathspace \forall \left(x_1, \ldots, x_{n-1}\right) \in B\left(\bcheck{a}, \delta\right)$
        \item $f$ est de classe $C^1$ dans un voisinage de $\bcheck{a}$, et on a:
            \[\frac{\partial f}{\partial x_p}\left(x_1, \ldots, x_{n-1}\right) = - \frac{\frac{\partial F}{\partial x_p}\left(x_1, \ldots, x_{n-1}, f\left(x_1, \ldots, x_{n-1}\right)\right)}{\frac{\partial F}{\partial x_n}\left(x_1, \ldots, x_{n-1}, f\left(x_1, \ldots, x_{n-1}\right)\right)}, \mathspace\forall p = 1, \ldots, n-1\]

    \end{enumerate}
}

\parag{Cas $n = 2$}{
    Soit $E \subset \mathbb{R}^2$, et soit $F\left(x, y\right) : E \mapsto \mathbb{R}$ une fonction de classe $C^1$ telle que $F\left(a, b\right) = 0$ et $\frac{\partial F}{\partial y}\left(a, b\right) \neq 0$.

    Alors, l'équation $F\left(x, y\right) = 0$ définit localement autour de $\left(a, b\right)$ une fonction $y = f\left(x\right)$ telle que $f\left(a\right) = b$, $F\left(x, f\left(x\right)\right) = 0$ pour tout $x$ dans un voisinage de $x = a$, et:
    \[f'\left(x\right) = - \frac{\frac{\partial F}{\partial x}\left(x, f\left(x\right)\right)}{\frac{\partial F}{\partial y}\left(x, f\left(x\right)\right)}\]

    Nous pouvons donc calculer $f'\left(a\right)$ sans savoir la formule pour $f\left(x\right)$.
}

\parag{Cas $n = 3$}{
    Soit $E \subset \mathbb{R}^3$ et $F\left(x, y, z\right) : E \mapsto \mathbb{R}$ de classe $C^1$ telle que $F\left(a, b, c\right) = 0$ et $\frac{\partial F}{\partial z}\left(a, b, c\right) \neq 0$.

    Alors, il existe localement une fonction $z = f\left(x, y\right)$ telle que $f\left(a, b\right) = c$, $F\left(x, y, f\left(x, y\right)\right) = 0$ pour tout couple $\left(x, y\right)$ dans un voisinage de $\left(a, b\right)$ et:
    \[\frac{\partial f}{\partial x}\left(x, y\right) = -\frac{\frac{\partial F}{\partial x}\left(x, y, f\left(x, y\right)\right)}{\frac{\partial F}{\partial z}\left(x, y, f\left(x, y\right)\right)}\]
    \[\frac{\partial f}{\partial y}\left(x, y\right) = -\frac{\frac{\partial F}{\partial y}\left(x, y, f\left(x, y\right)\right)}{\frac{\partial F}{\partial z}\left(x, y, f\left(x, y\right)\right)}\]
}

\parag{Application: Équation de l'hyperplan tangent}{
    Soit $F\left(x_1, \ldots, x_n\right)$ une fonction de classe $C^1$ sur $E \subset \mathbb{R}^n$ telle qu'il existe un $i$, où $1 \leq i \leq n$, tel que, pour un $\bvec{a} \in E$, nous avons $F\left(\bvec{a}\right) = 0$ et:
    \[\frac{\partial F}{\partial x_i}\left(\bvec{a}\right) \neq 0\]

    L'équation de l'hyperplan tangent à $F\left(\bvec{x}\right) = 0$ au point $\bvec{a}$ tel que $F\left(\bvec{a}\right) = 0$ est: 
    \[\left<\nabla F\left(\bvec{a}\right), \bvec{x} - \bvec{a}\right> = 0\]  
}

\subsection[Multiplicateurs de Lagrange]{Extrema liés --- Méthode des multiplicateurs de Lagrange}
\parag{Théorème: Condition nécessaire pour un extremum sous contrainte quand $n=2$}{
    Soit l'ensemble $E \subset \mathbb{R}^2$ et soient les fonctions $f, g : E \mapsto \mathbb{R}$ de classe $C^1$. Supposons que $f\left(x, y\right)$ admette un extremum en $\left(a, b\right) \in E$ sous la contrainte $g\left(x, y\right) = 0$, et que $\nabla g\left(a, b\right) \neq \bvec{0}$.

    Alors, il existe $\lambda \in \mathbb{R}$, appelé le \important{multiplicateur de Lagrange}, tel que: 
    \[\nabla f\left(a, b\right) = \lambda \nabla g\left(a, b\right)\]    
}

\parag{Théorème: Condition nécessaire pour un extremum sous contrainte}{
    Soit $E \subset \mathbb{R}^n$ et soient $f, g_1, \ldots, g_m E \mapsto \mathbb{R}$ des fonctions de classe $C^1$, où $m \leq n-1$. Soit $\bvec{a} \in E$ un extremum de $f\left(\bvec{x}\right)$ sous les contraintes $g_1\left(\bvec{x}\right) = \ldots = g_m\left(\bvec{x}\right) = 0$.

    Supposons que les vecteurs $\nabla g_1\left(\bvec{a}\right), \ldots, \nabla g_m\left(\bvec{a}\right)$ sont linéairement indépendants. Alors, il existe un vecteur $\bvec{\lambda} = \left(\lambda_1, \ldots, \lambda_m\right) \in \mathbb{R}^m$ tel que: 
    \[\nabla f\left(\bvec{a}\right) = \sum_{k=1}^{m} \lambda_k \nabla g_k\left(\bvec{a}\right) = \lambda_1 \nabla g_1\left(\bvec{a}\right) + \ldots + \lambda_m \nabla g_m\left(\bvec{a}\right)\]
    
    En particulier, si on cherche un extremum de $f\left(\bvec{x}\right)$ sous une seule contrainte $g\left(\bvec{x}\right) = 0$, on obtient les équations:
    \[\begin{systemofequations} \nabla f\left(\bvec{x}\right) = \lambda \nabla g\left(\bvec{x}\right) \\ g\left(\bvec{x}\right) = 0 \end{systemofequations} \mathspace \text{si } \nabla g\left(\bvec{x}\right) \neq 0\]
}



\end{document}



\documentclass[a4paper]{article}

% Expanded on 2022-04-20 at 14:43:32.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}
\date{Wednesday 20 April 2022}

\begin{document}
\maketitle

\section{Espace $\mathbb{R}^n$}
\subsection{$\mathbb{R}^n$ est un espace vectoriel normé}

\parag{Définition}{
    $\mathbb{R}^n$ est un \important{ensemble de tous les $n$-tuples} ordonnés de nombres réels. 
    \[\bvec{x} = \begin{pmatrix} x_1 & \cdots & x_n \end{pmatrix} \in \mathbb{R}^{n}\]
    
    On dit parfois que $\bvec{x}$ est un point (élément) de $\mathbb{R}^n$.
}

\parag{Opérations de $\mathbb{R}^n$}{
    $\mathbb{R}^n$ est muni de deux opérations. La première est l'addition $+$: 
    \[\bvec{x} = \begin{pmatrix} x_1 & \cdots & x_n \end{pmatrix}, \bvec{y} = \begin{pmatrix} y_1 & \cdots & y_n \end{pmatrix} \implies \bvec{x} + \bvec{y} \over{=}{déf} \begin{pmatrix} x_1 + y_1 & \cdots & x_n + y_n \end{pmatrix}\]
    
    La deuxième est la multiplication par un nombre réel $\lambda \in \mathbb{R}$: 
    \[\bvec{x} = \begin{pmatrix} x_1 & \cdots & x_n \end{pmatrix} \implies \lambda \bvec{x} \over{=}{déf} \begin{pmatrix} \lambda x_1 & \cdots & \lambda x_n \end{pmatrix}\]
            }

\parag{Grandeurs utiles dans $\mathbb{R}^{n}$}{
    On a deux autres opérations à connaître, même si elle ne sont pas nécessaire pour définir l'espace $\mathbb{R}^{n}$.

            1. Le produit scalaire: 
            \[\left<\bvec{x}, \bvec{y}\right> \over{=}{déf} \sum_{i=1}^{n}  x_i y_i = x_1 y_1 + \ldots + x_n y_n \]
            2. La norme euclidienne:
            \[\left\|\bvec{x}\right\| \over{=}{déf} \sqrt{\left<\bvec{x}, \bvec{x}\right>} = \sqrt{\sum_{i=1}^{n} x_i^2}\]
    

}


\parag{Propriétés de la norme euclidienne}{
            \begin{enumerate}[left=0pt]
            \item $\left\|\bvec{x}\right\| \geq 0 \mathspace \forall \bvec{x} \in \mathbb{R}^n$

            \item Si $\left\|\bvec{x}\right\| = 0$, alors $\bvec{x} = \bvec{0}$.
            
            \item Linéarité: $\left\|\lambda \bvec{x}\right\| = \left|\lambda\right| \left\|\bvec{x}\right\|, \mathspace \bvec{x} \in \mathbb{R}^n, \lambda \in \mathbb{R}$

            \item Inégalité de Cauchy-Schwartz: 
            \[\left|\left<\bvec{x}, \bvec{y}\right>\right| \leq \left\|\bvec{x}\right\|\cdot \left\|\bvec{y}\right\|\]

            \item Inégalité triangulaire 1: 
            \[\left\|\bvec{x} + \bvec{y}\right\| \leq \left\|\bvec{x}\right\| + \left\|\bvec{y}\right\|, \mathspace \forall \bvec{x}, \bvec{y} \in \mathbb{R}^n\]

            \item Inégalité triangulaire 2: 
            \[\left\|\bvec{x} - \bvec{y}\right\| \geq \left|\left\|\bvec{x}\right\| - \left\|\bvec{y}\right\|\right|\mathspace \forall \bvec{x}, \bvec{y} \in \mathbb{R}^n\]
        \end{enumerate}
        
    
}

\parag{Base}{
    On prend comme base canonique la base:
    \[\left\{\bvec{e}_i = \begin{pmatrix} 0 & \cdots & 0 & 1 & 0 & \cdots & 0 \end{pmatrix} \right\}_{i=1^n} \implies \bvec{e}_i \in \mathbb{R}^{n}, \mathspace i = 1, \ldots, n\]
    où \important{$\bvec{e}_i$ a uniquement un 1 à la $i$-ème position.
    }
    N'importe quel élément de $\mathbb{R}^n$ peut s'écrire comme combinaison linéaire de cette base: 
    \[\bvec{x} = \sum_{i=1}^{n} x_i \bvec{e}_i = \begin{pmatrix} x_1 & \cdots & x_n \end{pmatrix} \]
}

\parag{Définition: Distance}{
    L'expression $\left\|\bvec{x} - \bvec{y}\right\| = d\left(\bvec{x}, \bvec{y}\right)$ est appelée \important{la distance} entre $\bvec{x}$ et $\bvec{y}$ dans $\mathbb{R}^n$.

    \subparag{Propriétés}{
        \begin{enumerate}[left=0pt]
            \item $d\left(\bvec{x}, \bvec{y}\right) = d\left(\bvec{y}, \bvec{x}\right)$
            \item $d\left(\bvec{x}, \bvec{y}\right) = 0 \iff \bvec{x} = \bvec{y}$
            \item $d\left(\bvec{x}, \bvec{y}\right) \leq d\left(\bvec{x}, \bvec{z}\right) + d\left(\bvec{z}, \bvec{y}\right)$
        \end{enumerate}
    }
}

\subsection{Topologie dans $\mathbb{R}^n$}
\parag{Définition: Boule ouverte}{
    Pour tout $\bvec{x} \in \mathbb{R}^n$ et nombre réel $\delta > 0$, soit:
    \[B\left(\bvec{x}, \delta\right) = \left\{\bvec{y} \in\mathbb{R}^n \telque \left|\bvec{x} - \bvec{y}\right| < \delta\right\}\]

    $B\left(\bvec{x}, \delta\right) \subset \mathbb{R}^n$ est appelé la \important{boule ouverte} de centre $\bvec{x}$ et rayon $\delta$.

}

\parag{Définition: Ensemble ouvert}{
    Nous définissons que $E \subset \mathbb{R}^n$ est \important{ouvert} si et seulement si:
    \[\forall \bvec{x}\in E\ \exists \delta > 0 \telque B\left(\bvec{x}, \delta\right) \subset E\]

    \subparag{Remarques}{
        \begin{enumerate}
            \item L'ensemble vide $\o$ est ouvert car $\forall x \in \o\ P\left(x\right)$ est une tautologie, peu importe $P\left(x\right)$.
            \item Une boule ouverte $B\left(\bvec{x}, \delta\right)$ est un ensemble ouvert.

        \end{enumerate}
           }
}

\parag{Définition: Point intérieur}{
    Soit $E \subset \mathbb{R}^n$ non-vide. Alors, $\bvec{x} \in E$ est un \important{point intérieur} de $E$ s'il existe $\delta > 0$ tel que $B\left(\bvec{x}, \delta\right) \subset E$.

    L'ensemble des points intérieurs de $E$ est appelé \important{l'intérieur} de $E$, noté $\mathring{E}$.

    Clairement: 
    \[\mathring{E} \subset E\]
    
    \subparag{Observation}{
        $E \subset\mathbb{R}^n \neq \o$ est ouvert si et seulement si $\mathring{E} = E$.
    }
}

\parag{Propriétés des ensembles ouverts}{
    \begin{enumerate}[left=0pt]
        \item Toute réunion (même infinie) $\bigcup_{i \in I} E_i$ de sous-ensembles ouverts est un sous-ensemble ouvert. 
        \item Toute intersection \textit{finie} $\bigcap_{i=1}^n E_i$ de sous-ensembles ouverts est un sous-ensemble ouvert. 
    \end{enumerate}

    \subparag{Note}{
        Une intersection infinie de sous-ensembles ouverts de $\mathbb{R}^n$ n'est pas nécessairement ouvert: 
            \[\bigcap_{k=1}^{\infty} B\left(\bvec{0}, \frac{1}{k}\right) = \left\{\bvec{0}\right\} \subset \mathbb{R}^n\]
    }
    
}

\parag{Définition: Complémentaire d'un ensemble}{
    Soit $E \subset \mathbb{R}^n$. Son \important{complémentaire}, noté $CE$, est défini par: 
    \[CE \over{=}{déf} \left\{\bvec{x} \in \mathbb{R}^n \telque \bvec{x} \not\in E\right\} = \mathbb{R}^n \setminus E\]
}

\parag{Définition: Ensemble fermé}{
    Soit $E \subset \mathbb{R}^n$ un sous-ensemble. $E$ est \important{fermé} dans $\mathbb{R}^n$ si son complémentaire $CE$ est ouvert.
}

\parag{Propriétés des ensembles fermés}{
    \begin{enumerate}[left=0pt]
        \item Toute intersection (même infinie) de sous-ensembles fermés est un sous-ensemble fermé.
        \item Toute réunion finie de sous-ensembles fermés est un sous-ensemble fermé.
    \end{enumerate}
}

\parag{Définition: Frontière}{
    Soit un ensemble $E \subset \mathbb{R}^n$ non vide, où $E \neq \mathbb{R}^n$. 

    Un point $\bvec{x} \in \mathbb{R}^n$ est un \important{point de frontière} de $E$ si toute boule ouverte de centre $\bvec{x}$ contient au moins un point de $E$ et au moins un point de $CE$.

    L'ensemble des points de frontière de $E$ s'appelle la \important{frontière} de $E$, notée $\partial E$.

    Nous définissons aussi les deux frontières suivantes: 
    \[\partial \o = \o, \mathspace \partial \mathbb{R}^n = \o\]
    
}

\parag{Définition: Adhérence}{
    Soit $E \subset \mathbb{R}^n$ un sous-ensemble non-vide.

    L'intersection de tous les sous-ensembles fermés contenant $E$ est appelée \important{l'adhérence} de $E$ et est notée $\bar{E}$.
    
    On voit que si $E \subset \mathbb{R}^n$ est fermé, alors on a $E = \bar{E}$ par définition.
}

\parag{Propriétés}{
    Soit $E \subset \mathbb{R}^n$ non-vide. Alors, nous avons les propriétés suivantes:
    \begin{enumerate}
        \item $\mathring{E} \cap \partial E = \o$
        \item $\mathring{E} \cup \partial E = \bar{E}$
        \item $\partial E = \bar{E} \setminus \mathring{E}$
    \end{enumerate}
}

\subsection{Suites dans $\mathbb{R}^n$}
\parag{Définition: Suite dans $\mathbb{R}^n$}{
    Une suite d'éléments de $\mathbb{R}^n$ est une application $f: \mathbb{N} \mapsto \mathbb{R}^n$: 
    \[f: k \mapsto \bvec{x_k} = \begin{pmatrix} x_{1k} & \cdots & x_{nk} \end{pmatrix} \in \mathbb{R}^n\]
    
    $\left\{\bvec{x_k}\right\}_{k=0}^{\infty}$ est une suite d'éléments de $\mathbb{R}^n$.
}

\parag{Définition: Convergence}{
    Une suite $\left\{x_k\right\}_{k=0}^{\infty}$ est \important{convergente} et admet pour \important{limite} $\bvec{x} \in \mathbb{R}^n$ si pour tout $\epsilon > 0$, il existe $k_0 \in \mathbb{N}$ tel que $\forall k \geq k_0$, nous avons: 
    \[\left\|\bvec{x_k} - \bvec{x}\right\| \leq \epsilon\]
    
    Nous notons: 
    \[\lim_{k \to \infty} \bvec{x_k} = \bvec{x}\]

    \subparag{Remarque}{
        Notez que $\left\|\bvec{x_k} - \bvec{x}\right\| \leq \epsilon$ est équivalent à $\bvec{x_k} \in \bar{B\left(\bvec{x}, \epsilon\right)}$.
    }
}

\parag{Proposition}{
    Soit $\bvec{x} = \begin{pmatrix} x_1 & \cdots & x_n \end{pmatrix} \in \mathbb{R}^n$. Alors:
    \[\lim_{k \to \infty} \bvec{x_k} = \bvec{x} \iff \lim_{k \to \infty} x_{j, k} = x_j, \mathspace \forall j = 1, \ldots, n\]
}

\parag{Définition: Suite bornée}{
    Une suite $\left\{\bvec{x_k}\right\}$ est \important{bornée} s'il existe un $M > 0$ tel que $\left\{\bvec{x_k}\right\}$ est contenue dans la boule fermée $\bar{B\left(\bvec{0}, M\right)}$.
}

\parag{Propriétés}{
    \begin{enumerate}[left=0pt]
        \item La liste d'une suite $\left\{\bvec{x_k}\right\}$, si elle existe, est unique.
        \item Toute suite convergente $\left\{\bvec{x_k}\right\}$ est bornée.
    \end{enumerate}    
}

\parag{Théorème de Bolzano-Weierstrass}{
    Nous pouvons extraire une sous-suite convergente de toute suite bornée $\left\{\bvec{x_k}\right\} \subset \mathbb{R}^n$.
}

\parag{Théorème: Lien entre les suites dans $\mathbb{R}^n$ et la topologie}{
    Un sous-ensemble non-vide $E \subset \mathbb{R}^n$ est fermé si et seulement si toute suite $\left\{\bvec{x_k}\right\} \subset E$ d'éléments de $E$ qui converge a pour limite un élément de $E$.
}

\parag{Définition: Ensemble borné}{
    Un ensemble $E \subset \mathbb{R}^n$ est \important{borné} s'il existe un $M > 0$ tel que: 
    \[E \subset \bar{B\left(\bvec{0}, M\right)}\]
    
}

\parag{Définition: Ensemble compact}{
    Un sous-ensemble non-vide $E \subset \mathbb{R}^n$ est \important{compact} s'il est fermé et borné.
}

\parag{Définition: Recouvrement}{
    Un \important{recouvrement} d'un ensemble $E$ est défini par: 
    \[E \subset \bigcup_{i \in I} A_i, \mathspace A_i \subset \mathbb{R}^n \text{ ouverts}\]

    Notez que $I$ peut être innombrable.

    \imagehere[0.4]{DefinitionRecouvrement.png}
}

\parag{Théorème de Heine-Borel-Lebesgue}{
    Un sous-ensemble non-vide $E \subset \mathbb{R}^n$ est compact si et seulement si de \textit{tout} recouvrement de $E$ par des sous-ensembles ouverts dans $\mathbb{R}^n$ on peut extraire une famille finie d'ensembles qui forment un recouvrement de $E$.
}

\parag{Ensembles utiles}{
    \begin{itemize}[left=0pt]
        \item \important{Le Point}: $E = \{\bvec{x}\} \in \mathbb{R}^{n}$ est fermé.
        \item \important{La Boule Ouverte}: $B\left(\bvec{x}, \delta\right) = \left\{\bvec{y} \in\mathbb{R}^n \telque \left|\bvec{x} - \bvec{y}\right| < \delta\right\}$ est ouverte.
        \item \important{La Boule Fermée}: $\bar{B\left(\bvec{x}, \delta\right)} = \left\{\bvec{y} \in \mathbb{R}^n \telque \left\|\bvec{x} - \bvec{y}\right\| \leq \delta\right\}$ est fermée et bornée, et donc compacte.
        \item \important{La Droite}: une droite dans $\mathbb{R}^n$, où $n \geq 2$, est fermée mais pas bornée, et donc pas compacte.
        \item \important{L'Intervalle Ouvert}: un intervalle ouvert dans $\mathbb{R}^1 = \mathbb{R}$, $E = \left]0, 1\right[ \subset \mathbb{R}$ n'est pas compact. Il est bien borné, mais pas fermé.
        \item \important{Les Spéciaux}: $\mathbb{R}^{n}$ et $\o$ sont les seules ensembles à la fois ouvert et fermés.


    \end{itemize}
    
}


\end{document}

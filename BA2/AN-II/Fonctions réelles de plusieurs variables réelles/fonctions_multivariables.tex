\documentclass[a4paper]{article}

% Expanded on 2022-04-20 at 21:30:52.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Fonctions réelles de plusieurs variables réelles}

\parag{Définition}{
    Soit $E \subset \mathbb{R}^n$ un sous-ensemble non-vide où $n \geq 1$.

    Une \important{fonction} $E \mapsto \mathbb{R}^n$ est une application qui envoie chaque point $\bvec{x} = \begin{pmatrix} x_1 & \ldots & x_n \end{pmatrix} \in E$ dans $\mathbb{R}$. $E$ est \important{le domaine de définition} de $f$, et $f\left(E\right) \subset \mathbb{R}$ est \important{l'ensemble image}.
}

\parag{Définition}{
    Soit $f : E \mapsto \mathbb{R}^n$ et $c \in f\left(E\right) \subset \mathbb{R}$. Alors, l'ensemble suivant est appelé \important{l'ensemble de niveau} de $f$:
    \[N_f\left(c\right) = \left\{\bvec{x} \in E \telque f\left(\bvec{x}\right) = c\right\} \subset E\]
}

\subsection{Limites et continuité}
\parag{Définition: Définition au voisinage}{
    Une fonction est \important{définie au voisinage} de $\bvec{x_0}$ si:
    \[\exists \delta >0 \telque B\left(\bvec{x_0}, \delta\right) \subset E \cup \left\{x_0\right\}\]

    \subparag{Remarque}{
        La fonction n'a pas besoin d'être définie en $\bvec{x_0}$ pour être définie au voisinage de ce point.
    }
}

\parag{Définition: Limite}{
    Une fonction définie au voisinage de $\bvec{x_0}$ admet pour \important{limite} le nombre réel $\ell$ lorsque $\bvec{x}$ tend vers $\bvec{x_0}$, si pour tout $\epsilon > 0$, il existe $\delta > 0$ tel que pour tout $\bvec{x} \in E$: 
    \[0 < \left\|\bvec{x} - \bvec{x_0}\right\| \leq \delta \implies \left|f\left(\bvec{x}\right) - \ell\right| \leq \epsilon\]
    
    Dans ce cas, nous notons: 
    \[\lim_{\bvec{x} \to \bvec{x_0}} f\left(\bvec{x}\right) = \ell\]
}

\parag{Définition: Continuité}{
    Soit $\bvec{x_0} \in E$ un point intérieur de $E$.

    $f : E \mapsto \mathbb{R}$ est continue en $\bvec{x} = \bvec{x_0}$ si et seulement si: 
    \[\lim_{\bvec{x} \to \bvec{x_0}} f\left(\bvec{x}\right) = f\left(\bvec{x_0}\right)\]

    Il faut donc à la fois que la limite existe, et qu'elle soit égale à la valeur de la fonction.
}

\parag{Théorème: Caractérisation des limites à partir des suites convergentes}{
    Une fonction $f: E \mapsto \mathbb{R}$ définie au voisinage de $\bvec{x_0}$ admet pour limite $\ell \in \mathbb{R}$ lorsque $\bvec{x} \to \bvec{x_0}$ si et seulement si \textit{pour toute} suite d'éléments $\left\{\bvec{a_k}\right\}$ de $\left\{\bvec{x} \in E \telque \bvec{x} \neq \bvec{x_0}\right\}$, qui converge vers $\bvec{x_0}$, la suite $\left\{f\left(\bvec{a_k}\right)\right\}$ converge vers $\ell$.

    En d'autres mots: 
    \[\left(\lim_{\bvec{x} \to \bvec{x_0}} f\left(\bvec{x}\right) = \ell\right) \iff \left(\lim_{k \to \infty} f\left(\bvec{a_k}\right) = \ell,\ \forall \left\{\bvec{a_k}\right\} \subset E \setminus \left\{\bvec{x_0}\right\} \text{ telle que } \lim_{k \to \infty} \bvec{a_k} = \bvec{x_0}\right)\]
}

\parag{Opérations algébriques}{
    Soient $f,g$ deux fonctions $E_{\mathbb{R}^n} \mapsto \mathbb{R}$ telles que: 
    \[\lim_{\bvec{x} \to \bvec{x_0}} f\left(\bvec{x}\right) = \ell_1 \mathspace \text{et} \mathspace \lim_{\bvec{x} \to \bvec{x_0}} g\left(\bvec{x}\right) = \ell_2\]

    Alors, nous avons:
    \begin{enumerate}
        \item $\displaystyle \lim_{\bvec{x} \to \bvec{x_0}} \left(\alpha f + \beta g\right)\left(\bvec{x}\right) = \alpha \ell_1 + \beta \ell_2$ pour $\alpha, \beta \in \mathbb{R}$
        \item $\displaystyle \lim_{\bvec{x} \to \bvec{x_0}} \left(f\cdot g\right)\left(\bvec{x}\right) = \ell_1 \ell_2$
        \item Si $\ell_2 \neq 0$, alors $\displaystyle \lim_{\bvec{x} \to \bvec{x_0}} \left(\frac{f}{g}\right)\left(\bvec{x}\right) = \frac{\ell_1}{\ell_2}$
    \end{enumerate}
    
    \subparag{Implication}{
        On peut en déduire que tous les polynômes de plusieurs variables et toutes les fonctions rationnelles sont continues sur leur domaines de définition.
    }
}

\parag{Remarque}{
    La caractérisation de la limite à partir des suites convergentes est pratique pour montrer qu'une fonction n'admet pas de limite en $\bvec{x_0} \in \mathbb{R}^n$. En effet, il nous suffit de trouver deux suites $\left\{\bvec{a_k}\right\}$ et $\left\{\bvec{b_k}\right\}$ d'éléments de $E \setminus \left\{\bvec{x_0}\right\}$, convergentes vers $\bvec{x_0} \in \mathbb{R}^n$, et telles que:
    \[\lim_{k \to \infty} f\left(\bvec{a_k}\right) \neq \lim_{k \to \infty}  f\left(\bvec{b_k}\right)\]
}

\parag{Calcul de limite par changement de variable}{
    Soit la fonction $f: \mathbb{R}^2 \mapsto \mathbb{R}$ définie par: 
    \begin{functionbypart}{f\left(x\right)}
        \frac{x^3 + y^3}{x^2 + y^2}, \mathspace \text{si } \left(x, y\right) \neq \left(0, 0\right) \\
        0, \mathspace \text{autrement}
    \end{functionbypart}

    On peut facilement calculer la limite quand $\left(x,y\right) \to \left(0,0\right)$ en faisant le changement de variable:
\[x = r\cos\left(\phi\right), \mathspace y = r\sin\left(\phi\right)\]
        où $r \in \mathbb{R}_{\geq 0}$ et, si $r \neq 0$, $\phi \in \left[0, 2\pi\right[$.

        De cette manière, $x^2 + y^2$ devient $r^2 \cos^2\left(\phi\right) + r^2 \sin^2\left(\phi\right) = r^2\left(\cos^2\left(\phi\right) + \sin^2\left(\phi\right) = r^2$, ce qui nous permet de nous débarrasser de la fraction, étant donné que le terme du haut aura un $r^{3}$.   

}

\parag{Remarque 1}{
    Nous ne pouvons pas calculer la limite d'une fonction de plusieurs variables de la façon suivante: 
    \[\lim_{\left(x, y\right) \to \left(x_0, y_0\right)} f\left(x, y\right) \neq \lim_{x \to x_0} \left(\lim_{y \to y_0} f\left(x, y\right)\right)\]        
}

\parag{Remarque 2}{
    Si la limite $\lim_{\left(x, y\right) \to \left(a, b\right)} f\left(x, y\right)$ existe, et les limites par rapport à chaque variables existent pour tout $x$ et tout $y$ dans le domaine de $f$, alors on peut échanger l'ordre des limites: 
    \[\lim_{x \to a} \left(\lim_{y \to b} f\left(x, y\right)\right) = \lim_{y \to b} \left(\lim_{x \to a} f\left(x, y\right)\right)\]
}

\parag{Remarque 3}{
    L'existence de la limite $\lim_{\left(x, y\right) \to \left(a, b\right)} f\left(x, y\right)$ n'implique pas en général l'existence des limites $\lim_{x \to a} f\left(x, y\right)$ et $\lim_{y \to b} f\left(x, y\right)$.
}

\parag{Théorème des 2 gendarmes}{
    Soient $f, g, h : E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$, telles que:
    \begin{enumerate}
        \item $\displaystyle \lim_{\bvec{x} \to \bvec{x_0}} = \lim_{\bvec{x} \to \bvec{x_0}} g\left(\bvec{x}\right) = \ell$
        \item Il existe un $\alpha > 0$ tel que pour tout $x \in \left\{x \in E : 0 < \left\|\bvec{x} - \bvec{x_0}\right\| \leq \alpha\right\} = \bar{B\left(\bvec{x_0}, \alpha\right)} \setminus \left\{\bvec{x_0}\right\}$, on a:
            \[f\left(\bvec{x}\right) \leq h\left(\bvec{x}\right) \leq g\left(\bvec{x}\right)\]
    \end{enumerate}
    
    Alors: 
    \[\lim_{\bvec{x} \to \bvec{x_0}} h\left(\bvec{x}\right) = \ell\]
}

\parag{Proposition: Continuité d'une fonction composée}{
    Soient 2 sous-ensembles ensembles $A \subset \mathbb{R}^n$, $B \subset \mathbb{R}^p$. De plus, soient deux fonctions $\bvec{g} : A \mapsto B$ et $f: B \mapsto \mathbb{R}$ où: 
    \[\bvec{g}\left(\bvec{x}\right) = \left(g_1\left(\bvec{x}\right), \ldots, g_p\left(\bvec{x}\right)\right)\]
    
    Si $g_1, \ldots, g_p$ sont continues en $\bvec{a} \in A$, et $f\left(\bvec{y}\right)$ est continue en $\left(g_1\left(\bvec{a}\right), \ldots, g_p\left(\bvec{a}\right)\right)$, alors $f \circ \bvec{g}\left(\bvec{x}\right)$ est continue en $\bvec{x} = \bvec{a}$.
}

\subsection{Maximum et minimum d'une fonction sur un ensemble compact}
\parag{Définition: Maximum}{
    Soit la fonction $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$. 

    Si $M \in \mathbb{R}$ satisfait:
    \begin{enumerate}
        \item $\displaystyle f\left(\bvec{x}\right) \leq M$ pour tout $\bvec{x} \in E$
        \item $\displaystyle M \in f\left(E\right)$
    \end{enumerate}
    
    Alors $M$ est le \important{maximum} de la fonction $f$ sur $E$.
}

\parag{Définition: Minimum}{
    Soit la fonction $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$. 

    Si $m \in \mathbb{R}$ satisfait:
    \begin{enumerate}
        \item $\displaystyle f\left(\bvec{x}\right)\ {\color{red}\geq}\ m$ pour tout $\bvec{x} \in E$
        \item $\displaystyle m \in f\left(E\right)$
    \end{enumerate}
    
    Alors $m$ est le \important{minimum} de la fonction $f$ sur $E$.
}

\parag{Théorème du min et du max sur un compact}{
    Une fonction continue sur un sous-ensemble compact $E \subset \mathbb{R}^2$ atteint son maximum et son minimum, i.e.: 
    \[\exists \max_{\bvec{x} \in E} f\left(\bvec{x}\right), \mathspace \exists \min_{\bvec{x} \in E} f\left(\bvec{x}\right)\]

}



\end{document}

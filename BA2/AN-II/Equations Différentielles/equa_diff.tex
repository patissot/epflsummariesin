\documentclass[a4paper]{article}

% Expanded on 2022-04-19 at 12:32:04.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}
\date{Tuesday 19 April 2022}

\begin{document}
\maketitle

\section{Équations différentielles ordinaires}
\subsection{Définitions}

\parag{Définition}{
    Une \important{équation différentielle ordinaire} est une expression: 
    \[E\left(x, y, y', \ldots, y^{\left(n\right)}\right) = 0\]
    où $E$ est une expression fonctionnelle, $n \in \mathbb{N}_0$, et $y = y\left(x\right)$ est une fonction inconnue de $x$.

    On cherche un intervalle ouvert $I \subset \mathbb{R}$ et une fonction $y : I \mapsto \mathbb{R}$ de classe $C^n$ telle que l'équation donnée est satisfaite $\forall x \in I$.    
}

\parag{Terminologie}{
    On appelle $E\left(X, y, \ldots, y^{\left(n\right)}\right) = 0$ une équation différentielle (ED).

    \subparag{Ordre}{
        Un nombre naturel $n \in \mathbb{N}^*$ est \important{l'ordre} d'une équation différentielle si $n$ est l'ordre maximal de dérivée de $y\left(x\right)$ dans l'équation.
    }

    \subparag{Équation linéaire}{
        Si notre équation différentielle est un polynôme linéaire en $y, y', \ldots, y^{\left(n\right)}$, alors l'équation est dite \important{linéaire}.
    }

    \subparag{Équation autonome}{
        Si l'expression ne contient pas de $x$, l'équation est dite \important{autonome}.
    }

    \subparag{Solution générale}{
        La \important{solution générale} d'une équation différentielle est l'ensemble de toutes les solutions de l'équation.
    }
}

\parag{Définition du problème de Cauchy}{
    Résoudre le \important{problème de Cauchy} (équation différentielle avec des conditions initiales) pour l'équation $E\left(x, y, y', \ldots, y^{\left(n\right)}\right) = 0$ consiste à trouver l'intervalle ouvert $I \subset \mathbb{R}$ et une fonction $y : I \mapsto \mathbb{R}$ de classe $C^n\left(I\right)$ telle que $E\left(x, y, \ldots, y^{\left(n\right)}\right) = 0$ sur $I$ et pour laquelle des conditions initiales sont satisfaites:
    \[y\left(x_0\right) = b_0, y\left(x_1\right) = b_1, y'\left(x_2\right) = b_2, \ldots\]
    
    Le nombre des conditions initiales dépend du type de l'équation différentielle.
}

\subsection{Équations différentielles à variables séparées}
\parag{EDVS}{
    On appelle une \important{équation différentielle à variables séparées} (EDVS) une équation sous la forme: 
    \[f\left(y\right) \cdot y' = g\left(x\right)\]
    où $f: I \mapsto \mathbb{R}$ est une fonction continue sur $I \subset \mathbb{R}$ et $g : J \mapsto \mathbb{R}$ est une fonction continue sur $J \in\mathbb{R}$.

    Une fonction $y: J' \subset J \mapsto I$ de classe $C^1$ satisfaisant l'équation $f\left(y\right) y' = g\left(x\right)$ est une solution.

}

\parag{Théorème (existence et unicité d'une solution des EDVS)}{
    Soit $f: I \mapsto \mathbb{R}$ une fonction continue telle que \important{$f\left(y\right) \neq 0$ pour tout $y \in I$}, et soit $g : J \mapsto \mathbb{R}$ une fonction continue.

    \important{Existence:} Alors, pour tout couple $\left(x_0, b_0\right)$ où $x_0 \in J$ et $b_0 \in I$, l'équation 
    \[f\left(y\right) y' = g\left(x\right)\]
    admet une solution $y : J' \subset J \mapsto I$ vérifiant la condition initiale $y\left(x_0\right) = b_0$.

    \important{Unicité:} Si $y_1 : J_1 \mapsto I$ et $y_2 : J_2 \mapsto I$ sont deux solutions telles que $y_1\left(x_0\right) = y_2\left(x_0\right) = b_0$, alors: 
    \[y_1\left(x\right) = y_2\left(x\right), \mathspace \forall x \in J_1 \cap J_2\]

}    


\parag{Définition: Solution maximale}{
    Une \important{solution maximale} d'une EDVS avec la condition initiale $y\left(x_0\right) = b_0$ où $x_0 \in J$ et $b_0 \in I$ est une fonction $y\left(x\right)$ de classe $C^1$ satisfaisant l'équation, la condition initiale, et qui est définie sur le plus grand intervalle possible.

}

\subsection[EDL1]{Équations différentielles linéaires du premier ordre}
\parag{EDL1}{
    Soit $I \subset \mathbb{R}$ un intervalle ouvert. Nous appelons \important{équation différentielle linéaire du premier ordre} (EDL1) une équation de la forme suivante:
    \[y'\left(x\right) + p\left(x\right) y\left(x\right) = f\left(x\right)\]
    où $p, f : I \mapsto \mathbb{R}$ sont continues.


    Une \important{solution} est une fonction $y : I \mapsto \mathbb{R}$ de classe $C^1$ satisfaisant l'équation.
}

\parag{Équation homogène associée}{
       Considérons l'équation suivante: 
       \[y'\left(x\right) + p\left(x\right) y\left(x\right) = 0\]

    Cette équation s'appelle \important{l'équation homogène associée} à l'EDL1 $y'\left(x\right) + p\left(x\right)y\left(x\right) = f\left(x\right)$.

    On a que la solution générale de cette équation sur $I \subset \mathbb{R}$ est :
    \[y\left(x\right) = Ce^{-P\left(x\right)}, \mathspace \forall x \in I, \forall C \in \mathbb{R}\]

    où $P\left(x\right)$ est une primitive de $p\left(x\right)$ sur $I$.

}

\parag{Principe de superposition de solutions}{
    Soit $I \subset \mathbb{R}$ un intervalle ouvert, et $p, f_1, f_2 : I \mapsto \mathbb{R}$ des fonction continues.

    Supposons que $v_1 : I \mapsto \mathbb{R}$ de classe $C^1$ est une solution de l'équation:
    \[y' + p\left(x\right) y = f_1\left(x\right)\]

    Supposons aussi que $v_2 : I \mapsto \mathbb{R}$ de classe $C^1$ est une solution de l'équation:
    \[y' + p\left(x\right) y = f_2\left(x\right)\]

    Alors, pour tout couple $C_1, C_2 \in\mathbb{R}$, la fonction $v\left(x\right) =  C_1 v_1\left(x\right) + C_2 v_2\left(x\right)$ est une solution de l'équation:
    \[y' + p\left(x\right)y = C_1 f_1\left(x\right) + C_2 f_2\left(x\right)\]

}

\parag{Méthode de la variation de constante}{
    Afin de trouver une solution particulière de $y'\left(x\right) + p\left(x\right)y\left(x\right) = f\left(x\right)$ où $p, f : I\mapsto \mathbb{R}$ sont des fonctions continues, on peut utiliser la formule suivante:
    \[v\left(x\right) = C\left(x\right) e^{-P\left(x\right)}= \left(\int f\left(x\right) e^{P\left(x\right)} dx\right) e^{-P\left(x\right)}\]
    où $P\left(x\right)$ est une primitive de $p\left(x\right)$ sur $I$.
}

\parag{Proposition pour les EDL1}{
    Soient $p, f : I \mapsto \mathbb{R}$ des fonctions continues. Supposons que $v_0 : I \mapsto \mathbb{R}$ est une solution particulière de l'équation suivante:
    \[y'\left(x\right) + p\left(x\right) y\left(x\right) = f\left(x\right)\]

    Alors, la solution générale de cette équation est:
    \[v\left(x\right) = v_0\left(x\right) + Ce^{-P\left(x\right)}, \mathspace \forall C \in \mathbb{R}\]
    où $P\left(x\right)$ est une primitive de $p\left(x\right)$ sur $I$.
}
\parag{Résumé}{
    En mettant tout en commun, on obtient que la solution générale d'une EDL1 est:
    \[y\left(x\right) = y_{hom} + y_{part} = Ce^{-P\left(x\right)} + \left(\int f\left(x\right) e^{P\left(x\right)} dx\right) e^{-P\left(x\right)}, \mathspace \forall C \in \mathbb{R}, x \in I\]
}

\subsection{Équations différentielles linéaires du second ordre}
\parag{EDL2}{
    Soit $I$ un intervalle ouvert. On appelle \important{équation différentielle linéaire du second ordre} (EDL2) une équation de la forme: 
    \[y''\left(x\right) + p\left(x\right)y'\left(x\right) + q\left(x\right)y\left(x\right) = f\left(x\right)\]
    où $p, q, f: I \mapsto \mathbb{R}$ sont des fonctions continues.
}

\parag{ EDL2 homogène}{
    Nous appelons \important{EDL2 homogène} une équation de la forme suivante:
    \[y''\left(x\right) + p\left(x\right)y'\left(x\right) + q\left(x\right)y\left(x\right) = 0\]

    Nous cherchons une solution de cette équation de classe $C^2\left(I, \mathbb{R}\right)$.
}


\parag{EDL2 homogène à coefficients constants}{
    L'équation différentielle suivante:
        \[y''\left(x\right) + py'\left(x\right) + qy\left(x\right) = 0, \mathspace p, q \in \mathbb{R}\]
        est appelée \important{équation différentielle linéaire du second ordre à coefficients constants}.
        
        Soient $a, b \in \mathbb{C}$ les racines de l'équation $\lambda^2 + p\lambda + q = 0$. Alors, la solution générale de cette équation est:
        \begin{functionbypart}{y\left(x\right)}
        C_1 e^{ax} + C_2 e^{bx}, \mathspace \text{ si } a, b \in \mathbb{R}, a \neq b \\
        C_1 e^{ax} + C_2 xe^{ax}, \mathspace \text{ si } a = b \\
        C_1 e^{\alpha x} \cos\left(\beta x\right) + C_2 e^{\alpha x} \sin\left(\beta x\right), \mathspace \text{ si } a = \alpha + i\beta = \bar{b} \not \in \mathbb{R}
        \end{functionbypart}
        pour des constantes arbitraires $C_1, C_2 \in \mathbb{R}$ et pour tout $x \in \mathbb{R}$.    
}

\parag{Théorème}{
    Une EDL2 homogène admet \textit{une seule solution} $y\left(x\right) : I \mapsto \mathbb{R}$ de classe $C^2$ telle que $y\left(x_0\right) = t$ et $y'\left(x_0\right) = s$ pour un $x_0 \in I$ et les nombres arbitraires $s, t \in \mathbb{R}$.
}

\parag{Indépendance linéaire}{
    Deux solutions $y_1\left(x\right), y_2\left(x\right) : I \mapsto \mathbb{R}$ sont \important{linéairement indépendantes} s'il n'existe pas de constante $C \in \mathbb{R}$ telle que: 
    \[y_2\left(x\right) = Cy_1\left(x\right) \text{ ou } y_1\left(x\right) = Cy_2\left(x\right), \mathspace \forall x \in I\]
    
    En particulier, cela implique que $y_1\left(x\right)$ et $y_2\left(x\right)$ ne sont pas des fonctions constantes égales à 0 sur $I$.
}

\parag{Construction d'une deuxième solution}{
        Supposons que $v_1\left(x\right)$ est une solution de $y''\left(x\right) + p\left(x\right)y'\left(x\right) + q\left(x\right) y\left(x\right) = 0$ telle que $v_1\left(x\right) \neq 0$ pour tout $x \in I$. 

        Alors on peut obtenir une deuxième solution linéairement indépendante avec la formule:
        \[v_2\left(x\right) = c\left(x\right)v_1\left(x\right) = v_1\left(x\right) \int \frac{e^{-P\left(x\right)}}{v_1^2\left(x\right)}dx\]

}

\parag{Théorème: Forme des solutions aux EDL2 homogènes}{
    Soient $v_1, v_2 : I \mapsto \mathbb{R}$ deux solutions linéairement indépendantes de l'équation $y''\left(x\right) + p\left(x\right) y'\left(x\right) + q\left(x\right) y\left(x\right) = 0$.

    Alors, la solution générale de cette équation est de la forme: 
    \[v\left(x\right) = C_1 v_1\left(x\right) + C_2 v_2\left(x\right), \mathspace C_1, C_2 \in \mathbb{R}, x \in I\]
}


\subsection{Wronskien}
\parag{Définition: Wronskien}{
    Soient $v_1, v_2 : I \mapsto \mathbb{R}$ deux fonctions dérivables sur $I \subset \mathbb{R}$. Nous appelons la fonction $W\left[v_1, v_2\right] : I \mapsto \mathbb{R}$ définie par: 
    \[W\left[v_1, v_2\right] = \det\begin{pmatrix} v_1\left(x\right) & v_2\left(x\right) \\ v_1'\left(x\right) & v_2'\left(x\right) \end{pmatrix} = v_1\left(x\right) v_2'\left(x\right) - v_2\left(x\right) v_1'\left(x\right)\]
    le \important{Wronskien} de $v_1$ et $v_2$.
}

\parag{Proposition pour le Wronskien}{
    Soient $v_1, v_2 : O \mapsto \mathbb{R}$ deux solutions de l'équation $y''\left(x\right) + p\left(x\right)y'\left(x\right) + q\left(x\right)y\left(x\right) = 0$ (EDL2 homogène). 

     Si $W\left[v_1, v_2\right]\left(x\right) \neq 0$ pour tout $x \in I$, alors $v_1\left(x\right)$ et $v_2\left(x\right)$ sont linéairement indépendantes.

}

\subsection{Équation différentielle linéaire d'ordre 2 complète}
\parag{Introduction}{
    Nous considérons maintenant l'équation complète: 
\[y''\left(x\right) + p\left(x\right) y'\left(x\right) + q\left(x\right) y\left(x\right) = f\left(x\right)\]
}

\parag{Superposition des solutions}{
    Si $v\left(x\right)$ est une solution de l'équation $y''\left(x\right) + p\left(x\right) y'\left(x\right) + q\left(x\right) y\left(x\right) = f\left(x\right)$ et $u\left(x\right)$ est une solution de l'équation homogène associée, alors $v\left(x\right) + u\left(x\right)$ est aussi une solution de cette équation.
}

\parag{Méthode de la variation de la constante}{
    Afin de trouver une \important{solution particulière} de l'équation complète ci dessus, supposant que nous connaissons deux solutions linéairement indépendantes de l'équation \important{homogène associée}: $v_1, v_2 : I \mapsto \mathbb{R}$, nous pouvons utiliser la formule suivante: 
    \[v_0\left(x\right) = c_1\left(x\right) v_1\left(x\right) + c_2\left(x\right) v_2\left(x\right)\]
    où
    \[c_1\left(x\right) = \important{--} \int \frac{f\left(x\right) v_2\left(x\right)}{W\left[v_1, v_2\right]\left(x\right)}dx, \mathspace c_2\left(x\right) = \important{+} \int \frac{f\left(x\right) v_1\left(x\right)}{W\left[v_1, v_2\right]\left(x\right)}dx\] dans lesquelles on supprime les constantes.
        
Il nous est maintenant possible de déterminer la solution générale à cette équation: 
     \[v\left(x\right) = C_1 v_1\left(x\right) + C_2 v_2\left(x\right) + v_0\left(x\right), \mathspace \text{où } C_1, C_2 \in \mathbb{R}, x \in I\]
}

\parag{Méthode des coefficients indéterminés}{
    Cette méthode permet de trouver une \important{solution particulière} à l'équation 
    \[y''\left(x\right) + p y'\left(x\right) + qy\left(x\right) = f\left(x\right), \mathspace p, q \in \mathbb{R}\]
     Attention, ici les coefficients sont constants.

    Pour que cette méthode fonctionne, il faut que $f\left(x\right)$ soit une combinaison linéaire de:
    \[e^{cx} R_n\left(x\right) \mathspace \text{ et } \mathspace e^{ax}\left(\cos\left(bx\right) P_n\left(x\right) + \sin\left(bx\right)Q_m\left(x\right)\right)\]
    où $R_n\left(x\right), P_n\left(x\right), Q_m\left(x\right)$ sont des polynômes de degré $n$ et $m$, et $c, a, b \in \mathbb{R}$.

    Si $f\left(x\right) = e^{cx} R_n\left(x\right)$:
    \begin{center}
    \begin{tabularx}{\textwidth}{|>{\hsize=.25\textwidth}C|C|}
        \hline
        $c$ est une racine de $\lambda^2 + p\lambda + q = 0$ & Ansatz \\
        \hhline{|=|=|}
         Non & $y_{part} = e^{cx} T_n\left(x\right)$  \\
         \hline
         Oui & $y_{part} = x^r e^{cx} T_n\left(x\right)$ \\
        \hline
    \end{tabularx}
    \end{center}
    où $r$ est la multiplicité de la racine $\lambda = c$, et $T_n\left(x\right)$ est polynôme de degré $n$ à coefficients indéterminé.

    Si $f\left(x\right) = e^{ax}\left(\cos\left(bx\right) P_n\left(x\right) + \sin\left(bx\right) Q_n\left(x\right)\right)$:
    \begin{center}
    \begin{tabularx}{\textwidth}{|>{\hsize=.25\textwidth}C|C|}
        \hline
        $a + ib$ est une racine de $\lambda^2 + p\lambda + q = 0$ & Ansatz \\
        \hhline{|=|=|}
        Non & $y_{part} = e^{ax} \left(T_N\left(x\right) \cos\left(bx\right) + S_N\left(x\right) \sin\left(bx\right)\right)$  \\
         \hline
         Oui & $y_{part} = xe^{ax} \left(T_N\left(x\right) \cos\left(bx\right) + S_N\left(x\right) \sin\left(bx\right)\right)$ \\
        \hline
    \end{tabularx}
    \end{center}
    où $N = \max\left(n, m\right)$, et $T_N\left(x\right)$ et $S_N\left(x\right)$ sont des polynômes de degré $N$ à coefficients indéterminés.

    Ensuite, on remplace l'Ansatz dans l'équation différentielle afin de déterminer les coefficients.
}

\parag{Résumé}{
    Pour résoudre une équation différentielle de deuxième ordre complète, on peut suivre cet algorithme:
    \begin{enumerate}[left=0pt]
        \item Deviner une solution à l'équation homogène.
        \item Trouver une deuxième solution à l'équation homogène grâce à la formule décrite au paragraphe ``Construction d’une deuxième solution''.
        \item Si les coefficients sont constants, on peut utiliser les coefficients indéterminés, sinon on utilise la méthode de la variation de constante.
    \end{enumerate}
    
}


\end{document}

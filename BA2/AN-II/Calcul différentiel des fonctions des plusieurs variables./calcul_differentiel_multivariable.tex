\documentclass[a4paper]{article}

% Expanded on 2022-04-22 at 19:34:26.

\usepackage{../../../style}

\title{Résumé d'Analyse 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Calcul différentiel des fonctions des plusieurs variables.}

\subsection{Dérivée partielles et le gradient}

\parag{Définition: dérivée partielle}{
    Soit $f: E \mapsto \mathbb{R}^n$ une fonction, où $E \subset \mathbb{R}^n$ est un ensemble ouvert, et soit la fonction d'une seule variable suivante: 
    \[g\left(s\right) = f\left(a_1, a_2, \ldots, a_{k-1}, s, a_{k+1}, \ldots, a_n\right), \mathspace \text{où } \bvec{a} = \left(a_1, \ldots, a_n\right) \in E\]
    
    Le domaine de définition de $g$ est:
    \[D_g = \left\{s \in \mathbb{R} \telque \left(a_1, a_2, \ldots, a_k, s, a_{k+1}, \ldots, a_n\right) \in E\right\}\]
     
    Alors, si $g$ est dérivable en $a_k \in D_g$, on dit que la \important{$k$-ème dérivée partielle} de $f$ en $\bvec{a} \in E$ existe est égale à $g'\left(a_k\right)$, notée: 
    \[g'\left(a_k\right) \over{=}{déf} \frac{\partial f}{\partial x_k}\left(\bvec{a}\right) = D_k f\left(\bvec{a}\right)\]
    
    Nous avons: 
    \[\frac{\partial f}{\partial x_k}\left(\bvec{a}\right) = \lim_{t \to 0} \frac{g\left(a_k + t\right) - g\left(a_k\right)}{t} = \lim_{t \to 0} \frac{f\left(\bvec{a} + t \bvec{e_k}\right) - f\left(\bvec{a}\right)}{t}\]
}

\parag{Définition: Gradient}{
    Si toutes les dérivées partielles existent en $\bvec{a} \in E$: 
    \[\frac{\partial f}{\partial x_1}\left(\bvec{a}\right), \mathspace \ldots, \mathspace \frac{\partial f}{\partial x_n}\left(\bvec{a}\right)\]
    
    Alors, on définit le \important{gradient} de $f$ en $\bvec{a}$ comme: 
    \[\nabla f\left(\bvec{a}\right) = \left(\frac{\partial f}{\partial x_1}\left(\bvec{a}\right), \ldots, \frac{\partial f}{\partial x_n}\left(\bvec{a}\right)\right)\]
    
    \subparag{Intuition}{
        Le gradient montre la direction de la plus grande pente de la fonction. 
    }
}

\parag{Définition: Dérivée directionnelle}{
    Soit $E \subset \mathbb{R}^n$ un sous-ensemble ouvert. Soient aussi $\bvec{a} \in E$, et $\bvec{v} \in\mathbb{R}^n$ où $\bvec{v} \neq \bvec{0}$.
    
    Considérons une fonction $f :E \mapsto \mathbb{R}$, et soit la fonction d'une variable $t$ suivante:
    \[g\left(t\right) \over{=}{déf} f\left(\bvec{a} + t \bvec{v}\right), \mathspace \forall t \in \left\{t \in \mathbb{R} \telque \bvec{a} + t \bvec{v} \in E\right\}\]

    Si $g$ est dérivable en $t = 0$, on dit qu'il existe la \important{dérivée directionnelle} de $f$ en $\bvec{a}$ suivant le vecteur $\bvec{v}$ (dans la direction de $\bvec{v}$) et celle-ci est donnée par: 
    \[\lim_{t \to 0} \frac{g\left(t\right) - g\left(0\right)}{t} = \lim_{t \to 0} \frac{f\left(\bvec{a} + t \bvec{v}\right) - f\left(\bvec{a}\right)}{t} \over{=}{déf}  Df\left(\bvec{a}, \bvec{v}\right) = \frac{\partial f}{\partial \bvec{v}}\left(\bvec{a}\right)\]

    \subparag{Remarque}{
        \begin{enumerate}[left=0pt]
            \item Si $\bvec{v} = \bvec{e_i}$, où $\bvec{e_i} = \left(0, \ldots, 0, 1, 0, \ldots, 0\right)$, alors: 
                \[Df\left(\bvec{a}, \bvec{e_i}\right) = \lim_{t \to 0} \frac{f\left(\bvec{a} + t \bvec{e_i}\right) - f\left(\bvec{a}\right)}{t} = \frac{\partial f}{\partial x_i}\left(\bvec{a}\right)\]
            
                En d'autres mots, la dérivée partielle est un cas particulier de la dérivée directionnelle.

            \item $Df\left(\bvec{a}, \lambda \bvec{v}\right)  = Df\left(\bvec{a}, \bvec{v}\right) \lambda$
        \end{enumerate}
        
    }
}

\subsection{Dérivabilité et différentielle}
\parag{Définition: dérivabilité}{
    Soit $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$ est ouvert. De plus, soit $\bvec{a} \in E$.

    On dit que $f$ est \important{dérivable} (ou \important{différentiable}) au point $\bvec{a}$ s'il existe une transformation linéaire $L_{\bvec{a}} : \mathbb{R}^n \mapsto \mathbb{R}$ et une fonction $r: E \mapsto \mathbb{R}$ telles que: 
    \[f\left(\bvec{x}\right) = f\left(\bvec{a}\right) + L_{\bvec{a}}\left(\bvec{x} - \bvec{a}\right) + r\left(\bvec{x}\right), \mathspace\forall x \in E\]
    et aussi $r$ doit être telle que: 
    \[\lim_{\bvec{x} \to \bvec{a}} \frac{r\left(\bvec{x}\right)}{\left\|\bvec{x} - \bvec{a}\right\|} = 0\]
    
    $L_{\bvec{a}}$ s'appelle la \important{différentielle} de $f$ au point $\bvec{a} \in E$, est aussi parfois notée: 
    \[L_{\bvec{a}} = df\left(\bvec{a}\right)\]
}

\parag{Théorème 1 sur la dérivabilité}{
    Soit $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$, une fonction dérivable en $\bvec{a} \in E$ et de différentielle $L_{\bvec{a}} : \mathbb{R}^n \mapsto \mathbb{R}$. Alors:
    \begin{enumerate}
        \item $f$ est continue en $\bvec{a} \in E$.
        \item Pour tout $\bvec{v} \in \mathbb{R}^n$, où $\bvec{v} \neq \bvec{0}$, la dérivée directionnelle $Df\left(\bvec{a}, \bvec{v}\right)$ existe et: 
            \[Df\left(\bvec{a}, \bvec{v}\right)= L_{\bvec{a}}\left(\bvec{v}\right)\]
        \item Toutes les dérivées partielles de $f$ en $\bvec{a}$ existent, et: 
        \[\frac{\partial f}{\partial x_{k}}\left(\bvec{a}\right) = L_{\bvec{a}}\left(\bvec{e_k}\right)\]
        où $\bvec{e_k}$ est le $k$-ème vecteur de la base canonique.
    
        Le gradient de $f$ existe en $\bvec{a}$ et: 
        \[\nabla f\left(\bvec{a}\right) = \left(L_{\bvec{a}}\left(\bvec{e_1}\right), \ldots, L_{\bvec{a}}\left(\bvec{e_n}\right)\right)\]
        
        \item Pour tout $\bvec{v} \in \mathbb{R}^n$, où $\bvec{v} \neq \bvec{0}$, alors: 
        \[L_{\bvec{a}}\left(\bvec{v}\right) = Df\left(\bvec{a}, \bvec{v}\right) = \left<\nabla f\left(\bvec{a}\right), \bvec{v}\right>\]
        \item Pour tout $\bvec{v} \in \mathbb{R}^n$ tel que $\left\|\bvec{v}\right\| = 1$, nous avons: 
            \[Df\left(\bvec{a}, \bvec{v}\right) \leq \left\|\nabla f\left(\bvec{a}\right)\right\|\]
        
            De plus: 
            \[Df\left(\bvec{a}, \frac{\nabla f\left(\bvec{a}\right)}{\left\|\nabla f\left(\bvec{a}\right)\right\|}\right) = \left\|\nabla f\left(\bvec{a}\right)\right\|\]
    \end{enumerate}
}

\parag{Plan tangent à une surface}{
    Soit $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^2$, une fonction dérivable sur $E$. Soit aussi $\bvec{a} = \left(x_0, y_0, f\left(x_0, y_0\right)\right) \in \mathbb{R}^3$ un point du graphique de $f$. L'équation du plan tangent à $z = f\left(x,y\right)$ en ce point est donnée par:

    \[z = f\left(x_0, y_0\right) + \left<\nabla f\left(x_0, y_0\right), \left(x - x_0, y - y_0\right)\right>\] 

    Si $f$ n'est pas dérivable en ce point, alors ce plan n'est pas un plan tangent, même si le gradient $\nabla f\left(x_0, y_0\right)$ existe. Le plan tangent n'est simplement pas défini dans ce cas.

}


\parag{Théorème 2 sur la dérivabilité}{
    Soit $E \subset \mathbb{R}^n$ un ensemble ouvert, $f : E \mapsto \mathbb{R}$ et un point $\bvec{a} \in E$.

    Supposons qu'il existe $\delta > 0$ tel que toutes les dérivées partielles $\frac{\partial f}{\partial x_k}\left(\bvec{a}\right)$ existent sur $B\left(\bvec{a}, \delta\right)$ et sont continues en $\bvec{a}$. Alors, $f$ est dérivable en $\bvec{a}\in E$.
}

\subsection{Dérivées partielles d'ordre supérieur}
\parag{Définition: Fonction dérivée partielle}{
    Soit $f: E \mapsto \mathbb{R}$, où $E \subset \mathbb{R}^n$ est ouvert, une fonction telle que $\frac{\partial f}{\partial x_k}$ existe pour un $k$, avec $1 \leq k \leq n$, en tout point de $E$. Alors $\frac{\partial f}{\partial x_k}\left(\bvec{x}\right)$ où $\bvec{x} \in E$, est la \important{fonction $k$-ème dérivée partielle}.
}

\parag{Définition: Dérivée partielle d'ordre supérieur}{
    Soit $f: E \mapsto \mathbb{R}$ une fonction telle que $\frac{\partial f}{\partial x_k}$ existe en tout $\bvec{x} \in E$. Si la fonction $\frac{\partial f}{\partial x_k}$ admet à son tour une dérivée partielle par rapport à $x_i$ (potentiellement une autre variable), on pose: 
    \[\frac{\partial }{\partial x_i} \left(\frac{\partial f}{\partial x_k}\right) \over{=}{déf} \frac{\partial^2 f}{\partial x_i \partial x_k}\]

    Nous appelons ceci la \important{dérivée partielle seconde}. Nous pouvons définir ainsi, lorsqu'elles existent, les dérivées partielles d'ordre $p$. Par exemple: 
    \[\frac{\partial}{\partial x_j} \left(\frac{\partial}{\partial x_i} \left(\frac{\partial f}{\partial x_k}\right)\right) = \frac{\partial^3 f}{\partial x_j \partial x_i \partial x_k}\]
}


\parag{Définition: Classe}{
    Soit $E \subset \mathbb{R}^n$ un ensemble ouvert.

    Une fonction $f: E \mapsto \mathbb{R}$ est dite de \important{classe} $C^p$ sur $E$, si toutes les dérivées partielles d'ordre $\leq p$ existent et sont continues sur $E$.
    
    \subparag{Remarque}{
       Le théorème 2 sur la dérivabilité nous dit que, si $f$ est de classe $C^1$ sur $E$, alors $f$ est dérivable sur $E$.
    }
}

\parag{Théorème de Schwartz}{
    Soit $f : E \mapsto \mathbb{R}$ et $\bvec{a} \in E$ tel que les dérivées partielles secondes $\frac{\partial^2 f}{\partial x_i \partial x_j}$ et $\frac{\partial^2 f}{\partial x_j \partial x_i}$ existent dans un voisinage de $\bvec{a}$ et sont continues en $\bvec{a}$ (en d'autres mots, $f$ est de classe $C^2$ sur un ensemble ouvert contenant $\bvec{a}$).

    Alors, nous avons: 
    \[\frac{\partial^2 f}{\partial x_i \partial x_j}\left(\bvec{a}\right) = \frac{\partial^2 f}{\partial x_j \partial x_i}\left(\bvec{a}\right)\]

    \subparag{Remarque}{
        De manière générale, on peut démontrer que si $f$ est de classe $C^{p}$ sur $E$, alors nous pouvons échanger de manière cyclique l'ordre des dérivées partielles jusqu'à l'ordre $p$.
    }
}

\parag{Définition: Matrice Hessienne}{
    La \important{matrice Hessienne} est la matrice des dérivées partielles d'ordre 2 pour une fonction $E \mapsto \mathbb{R}^n$, où $E \subset \mathbb{R}^n$ est un sous-ensemble ouvert, notée: 
    \[\Hess\left(f\right)\left(\bvec{a}\right) = \begin{pmatrix} \dfrac{\partial^2 f}{\partial x_1^2}\left(\bvec{a}\right) & \dfrac{\partial^2 f}{\partial x_2 x_1}\left(\bvec{a}\right) & \cdots & \dfrac{\partial^2 f}{\partial x_n \partial x_1}\left(\bvec{a}\right) \\ \dfrac{\partial^2 f}{\partial x_1 \partial x_2}\left(\bvec{a}\right) & \dfrac{\partial^2 f}{\partial x_2^2}\left(\bvec{a}\right) & \cdots & \dfrac{\partial^2 f}{\partial x_n \partial x_2}\left(\bvec{a}\right) \\ \vdots & \vdots & \ddots  & \vdots \\ \dfrac{\partial^2 f}{\partial x_1 \partial x_n}\left(\bvec{a}\right) & \dfrac{\partial^2 f}{\partial x_2 \partial x_n}\left(\bvec{a}\right) & \cdots & \dfrac{\partial^2 f}{\partial x_n^2}\left(\bvec{a}\right) \end{pmatrix} \]
    
    \subparag{Remarque}{
        Si $f$ est de classe $C^2$ sur $E$, alors la matrice Hessienne est symétrique, c'est-à-dire: 
        \[\Hess\left(f\right)\left(\bvec{a}\right) = \Hess\left(f\right)\left(\bvec{a}\right)^T\]
    }
}

\parag{Démonstration de la dérivabilité}{
    On peut démontrer qu'une fonction est dérivable (ou non) avec les deux méthodes suivantes:
    \subparag{Méthode 1}{
        Nous savons que si toutes les dérivées partielles d'ordre 1 sont continues au point donné, alors nous savons que cela implique que $f$ est dérivable.
        
        Attention, le fait qu'une ou plusieurs dérivées partielles $\frac{\partial f}{\partial x_i}$ ne soient pas continues en $\bvec{a}$ n'implique pas nécessairement que $f$ n'est pas dérivable en $\bvec{a}$.    }

    \subparag{Méthode 2}{
        Si le gradient $\nabla f\left(\bvec{a}\right)$ n'existe pas, alors nous savons que $f$ n'est pas dérivable en $\bvec{a}$. S'il existe, nous pouvons poser: 
        \[r\left(\bvec{x}\right) = f\left(\bvec{x}\right) - f\left(\bvec{a}\right) - \left<\nabla f\left(\bvec{a}\right), \bvec{x} - \bvec{a}\right>\]
        
        Alors, si $\lim_{\bvec{x} \to \bvec{a}} \frac{r\left(\bvec{x}\right)}{\left\|\bvec{x} - \bvec{a}\right\|} = 0$, nous savons que $f$ est dérivable en $\bvec{a}$ par définition.

        De manière similaire, si $\lim_{\bvec{x} \to \bvec{a}} \frac{r\left(\bvec{x}\right)}{\left\|\bvec{x} - \bvec{a}\right\|} \neq 0$, alors $f$ n'est pas dérivable par notre premier théorème.    }
}


\parag{Résumé}{
    Voici un schéma résumant la théorie de ce chapitre.

    Soit $f: E \mapsto \mathbb{R}$, où $E$ est ouvert, alors:
    \svghere[0.5]{DiagrammeDerivabilite.svg}
}

\end{document}

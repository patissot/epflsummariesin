\documentclass[a4paper]{article}

% Expanded on 2022-04-21 at 11:09:31.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Cryptography}
\parag{Introduction}{
    Cryptography serve three main purposes:
    \begin{itemize}[left=0pt]
        \item Preserve \important{privacy and confidentiality}
        \item Assert \important{authenticity} of communications
        \item Preserve \important{integrity} of communications
    \end{itemize}
}

\parag{Basic setup for confidentiality}{
    We have the following diagram:
    \imagehere{BasicSetupForConfidentiality.png}

    $t$ is the \important{plain text} and $c$ is the \important{cipertext or the cryptogram}. For our encryption and decryption algorithms, we also have a key $k_A$ and $k_B$. We finally have the two functions: 
    \[E_{k_A}\left(t\right) = c, \mathspace D_{k_{B}}\left(c\right) = t\]
}

\parag{Caesar's cipher}{
    This cipher uses the concept of permutation, doing a cyclic shift. For exemple with a shift of 3, we get:
    \begin{center}
    \begin{tabular}{c|cccccccc}
        Plain text & A & B & C & D & \ldots & X & Y & Z \\
        \hline
        Cryptogram & D & E & F & G & \ldots & A & B & C \\
    \end{tabular}
    \end{center}

    Another way of seeing this is considering that our alphabet is linked to numbers: 
    \begin{center}
    \begin{tabular}{cccccccccccc}
        A & B & C & \ldots & X & Y & Z &  & , & . \\
        0 & 1 & 2 & \ldots & 23 & 24 & 25 & 26 & 27 & 28
    \end{tabular}
    \end{center}
    where the \nth{26} character is a space.
    
    The algorithm can be described as: 
    \[E_{k} = t + k \Mod 29, \mathspace D_k = t - k \Mod 29, \mathspace k \in \left\{0, \ldots, 28\right\}\]
}

\parag{Monoalphabetic cipher}{
    More generally than a Caesar cipher, a monoalphabetic cipher is an arbitrary permutation of the alphabet. For example: 
    \begin{center}
    \begin{tabular}{cccc}
        A & B & C & \ldots \\
        P & V & X & \ldots 
    \end{tabular}
    \end{center}

    The ``monoalphabetic'' is here since we are using the same permutation table for every letter.
}

\parag{Vigenère Cipher}{
    The idea of Vigenère Cipher is to use Caesar cipher, but with a different key for every letter. If we have more letters to encode than keys, then we begin the sequence of keys again. Remembering the sequence of key can be complicated, so we sometimes use a word instead (abc to mean $k_1 = 0$, $k_2 = 1$ and $k_3 = 2$, for example). If we have a word of 7 letters and a key of 4 letters, our algorithm can be summed up as:
    \begin{multiequality}
    V_K\left(t\right) =\ & V_{k_1, k_2, k_3, k_4}\left(t_1, t_2, t_3, t_4, t_5, t_6, t_7\right)  \\
    =\ & \left(t_1 \oplus k_1, t_2 \oplus k_2, t_3 \oplus k_3, t_4 \oplus k_4, t_5 \oplus k_1, t_6 \oplus k_2, t_7 \oplus k_3\right) 
    \end{multiequality}
    where $\oplus$ is an addition modulo.
}

\parag{Key assumption}{
    In modern cryptography, we consider that the algorithm is public, but the key is private. We must not base our cryptographic secrecy on the algorithm. Indeed, when the algorithm is out, it is out, whereas if our key is out, we can change it.
}

\parag{One-time pad}{
    Let's assume that we are using binary, for simplicity. We take a key as long as the ciphertext, thus: 
    \[t, k, c \in \left\{0, 1\right\}^{n}\]
    
    Moreover, we also take $k$ so that it is a \textit{random} (uniform) IID sequence. The encryption is $c = k + t$, and the decryption is $t = c - k = k + c$, since we are using $n$-bits encryption ($\congruent{b + b}{0}{2}$ for $b \in \left\{0, 1\right\}$).
   
    Note that this is exactly Vigenère's code, where the key and the plain text are of the same length, and $k$ is random.
}

\parag{Definition: perfect secrecy}{
    \important{Perfect secrecy} is reached when the plaintext $t$ and $c$ are statistically independent.
}

\parag{Lemma: entropy}{
    If we have $n$ IID random variables uniformly distributed over $\mathcal{A}$, $K_1, \ldots, K_n$, then: 
    \[H\left(K_1, \ldots, K_n\right) = n\log_2\left|\mathcal{A}\right|\]
    
    More precisely, if we are working over binary: 
    \[H\left(K_1, \ldots, K_n\right) = n\]
}

\parag{Theorem}{
    The one-time pad is perfectly secret.

    \subparag{Remark}{
        The problem for this code is that we need a huge amount of huge keys.
    }
}

\parag{Theorem}{
    If we have perfect secrecy, then: 
    \[H\left(T\right) \leq H\left(K\right)\]    
}

\subsection{Public channel}
\parag{Idea}{
    We wonder if two people who have never met agree on a secret key over a public channel. We cannot give a key, since other people would be able to see it.
}

\parag{Definition}{
    Let $p$ be a prime number. If $\left\{g^i \Mod p : i = 0, 1, \ldots, p-2\right\}$ is a permutation of $\left\{1, 2, \ldots, p-1\right\}$, then $g$ is called a \important{generator}.
}

\parag{Definition: One-way function}{
    A \important{one-way function} is a function so for which one way is easy to compute, but its reciprocal is really hard.

    \subparag{Example}{
        $g^a$, where $g$ is known but $a$ is unknown, is conjectured to be one-way. 
    }
}

\parag{Definition: Trapdoor one-way function}{
    A \important{trapdoor one-way function} is a one-way function, but for which the reciprocal is easy to compute when we have the key $k$ to a trapdoor.

    \subparag{Remark}{
        $g^a$, where $g$ is known but $a$ is unknown, is \textit{not} a trapdoor one-way function.
    }
}

\parag{Diffie-Hellman cryptosystem}{
   First, Alice and Bob agree publicly on a prime number $p$ and a generator $g$. Then, Alice picks a secret $a \in \left\{0, 1, \ldots, p-1\right\} = \mathcal{A}$, and calculates $A = g^a \Mod p$. Similarly, Bob picks a secret $b \in \mathcal{A}$, and calculates $B = g^b \Mod p$. Then, both write $A$ and $B$ in a public directory.

    Now, Alice gets $B$ from the directory, and she computes:
    \[B^a = \left(g^{b}\right)^{a} \Mod p = g^{ab} \Mod p\]

    Similarly, Bob get $A$ from the directory, and computes:
    \[A^b = \left(g^{a}\right)^{b} \Mod p = g^{ab} \Mod p\]

    They thus get the same key, and can use it for sending private messages.

    \svghere{DiffieHellmanSystem.svg}

}

\parag{El Gammal's cryptosystem}{
    This ciphering system works with trapdoor one-way functions.
    Alice and Bob choose a $g$ publicly. Then, Alice picks a random number $y$, and Bob a random number $x$. Alice publishes $g^y$ and Bob $g^x$. If Alice wants to send her message, she can compute $c = g^{xy}t\ \Mod\ \left|\mathcal{A}\right|$, where $g^{xy}$ can be easily computed as in the Diffie-Hellman cryptosystem. Then, Bob can decipher the message by using the multiplicative inverse of $g^{xy}$. Indeed, if $a$ is the multiplicative inverse of $g^{xy}$:
    \[ac = a\left(g^{xy} t\right) = a g^{xy} t = t\]
}

\subsection{RSA cryptosystem}
\parag{Encryption and Decryption}{
    Let's suppose that we can find three integers $m$ (modulus), $e$ (encoding exponent) and $d$ (decoding exponent) such that, for all integers $t \in \mathbb{Z} / m\mathbb{Z}$ (our plaintext), we have:
    \[\left[\left(t^e\right)^d\right]_m = \left[t\right]_m\]

    Then, the receiver can generate those $m, e, d$. The $\left(m, e\right)$ is the public encoding key, announced in a public directory, and $\left(m, d\right)$ is the private decoding key ($d$ never leaves the receiver). To send the plaintext $t \in \mathbb{Z} / m\mathbb{Z}$, the encoder forms the cryptogram by doing:
    \[c = t^e \Mod m\]

    Then, the intended decoder performs $c^d \Mod m$ and obtains the plaintext $t$.

    \subparag{Diagram}{
        We can make the following diagram to sum up this algorithm:
        \svghere{RSACryptosystem.svg}
    }

    \subparag{Remark}{
        We have the equivalence:
        \[ed = \ell k + 1 \iff ed + \widetilde{\ell} k = 1\]

        We can exactly use Bézout to generate such $d$ and $\widetilde{\ell }$, when we know $k$ and $e$ if they are such that $\gcd\left(k, e\right) = 1$.
    }
}

\parag{Key generation}{
    We use the following algorithm to generate our keys:
    \begin{itemize}
        \item We generate large primes $p$ and $q$ at random.
        \item We compute the modulus as $m = pq$.
        \item We compute $k$, a number we keep secret, to be a multiple of $\left(p-1\right)$ and $\left(q-1\right)$. For instance, we can take $k = \phi\left(pq\right)$ or $k = \lcm\left(p-1, q-1\right)$.
        \item We produce the public encoding exponent by taking a number $e$ such that $\gcd\left(e, k\right) = 1$. A common choice is the prime number $e = 2^{16} + 1$. Note that there is no need for $e$ to be distinct for each recipient.
        \item We use Bézout's construction (the extended Euclid's algorithm) to produce the positive decoding exponent $d$ such that $de + k \ell = 1 \iff de = \hat{k} \ell + 1$.
    \end{itemize}

    The public key is $\left(m, e\right)$ and the private key is $\left(m, d\right)$.
}

\parag{Decoding exponent}{
    The $d$ and $\ell$ used for decoding ar not unique. Let's say we found $ed = k \ell + 1$. We can add $ek \alpha$ (where $\alpha$ is an arbitrary number) on both sides, yielding:  
    \[ed + ek \alpha = k \ell + 1 + ek \alpha = e\left(d + k \alpha\right) = k\left(\ell + e \alpha\right) + 1\]
    
    Thus, picking $\widetilde{d} = d + k\alpha$ and $\widetilde{\ell} = \ell + e\alpha$, we get: 
    \[e\widetilde{d} = k\widetilde{\ell} + 1\]
    
    This can be useful when we found a negative $d$, and actually wanted a positive one for RSA.
}

\parag{Fast exponentiation}{
    RSA is based on the efficiency of our algorithms to compute $a^k \Mod m$. 

    Let $k$ be such that $0 \leq k \leq m-1$ (we do not need to go higher since we know $a^{m} \Mod m = m$). We know we can write it in its binary representation: 
    \[k = \sum_{i=0}^{L-1} b_i 2^{i}, \mathspace \text{where } b_i \in \left\{0, 1\right\}\]
    
    Thus, $a^k$ is given by: 
    \[a^k = a^{\sum_{i=0}^{L-1} b_i 2^i} = \prod_{i=0}^{L-1} a^{b_i 2^i} = \prod_{i=0}^{L - 1} \left(a^{2^i}\right)^{b_i} = \prod_{i=0}^{L - 1}  a_i^{b_i}\]
    where $a_i$ will be defined right after.

    Also, taking everything mod $m$, we get: 
    \[a^k \Mod m = \left(\prod_{i=0}^{L-1} a_i^{b_i}\right) \Mod m = \left(\prod_{i=0}^{L-1} \left(a_i \Mod m\right)^{b_i}\right) \Mod m\]
    
    We only need to compute all the $a_i$, and multiply all for which $b_i = 1$, taking a mod at each step. To compute $a_i = a^{2^{i}}$, we can compute them recursively very easily: 
    \[a_0 = a \Mod m, \mathspace a_i = \left(a_{i-1}\right)^2\]
    
   This algorithm~---~named fast exponentiation~---~is $\Theta\left(\log\left(m\right)\right)$.
}

\parag{Trapdoor one-way functions}{
    We can take a more abstract view of encryption, considering everything through trapdoor one-way functions.

    For privacy, Bob can put the forward function $f_B\left(x\right)$ in the public directory, and he can keep the inverse function for himself. To send text to Bob, Alice can publish $c = f_B\left(t\right)$. But then, the only person who can get $t$ is Bob, who knows $f^{-1}_B$, and who can compute $f^{-1}_B\left(f_B\left(t\right)\right) = t$.
    \svghere[0.75]{PrivacyTrapdoorOneWayFunction.svg}
}

\parag{Digital signature}{
     In order to sign a document, Alice can publish $\left(t, f_A^{-1}\left(t\right)\right)$. On assumption, nobody can compute $s = f_A^{-1}\left(t\right)$ except for her. Then, Bob can compute $f_A\left(s\right)$, and verify it matches $t$. This allows him to be sure that Alice sent this message and that it was not modified.
    \svghere[0.75]{SigningTrapdoorOneWayFunction.svg}
}

\parag{Digital signature and RSA}{
     With RSA, Alice would generate a public and a private key, as always. She can then compute $\left(t, \left[t^{d}\right]_{m}\right)$ and send it to Bob. She could only have done that by knowing $d$, which is private. Then, Bob can compute: 
     \[\left(\left[t^{d}\right]_m\right)^e = \left[t\right]_m\]
     and thus be sure that Alice really sent the message.
}



\end{document}

\documentclass[a4paper]{article}

% Expanded on 2022-04-21 at 10:12:45.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle


\section{Discrete probabilities}
\parag{Definitions}{
    The \important{sample space}, $\Omega = \left\{\omega_1, \ldots, \omega_n\right\}$, is the set of all possible outcomes. An \important{event} $E$ is a subset of $\Omega$.
}

\parag{Probability of an event}{
    The probability of an event when all event are equally likely is computed: 
    \[p\left(E\right) = \frac{\left|E\right|}{\left|\Omega\right|}\]

}

\parag{Probability Distribution}{
     We define the \important{probability distribution} (probability mass function) $p$ to be a function $p: \Omega \mapsto \left[0, 1\right]$ such that: 
    \[\sum_{\omega \in \Omega}^{} p\left(\omega\right) = 1\]

    The probability of an event can be computed the following way:
    \[p\left(E\right) = \sum_{\omega \in E}^{} p\left(\omega\right)\]

}

\parag{Conditional probability}{
    To compute the probability that E occurs, knowing that the event F has occurred we use:
    \[p\left(E | F\right) = \frac{p\left(E \cap F\right)}{p\left(F\right)}\]
}

\parag{Independent events}{
    We say that two event $E$ and $F$ are \important{independent} if and only if: 
    \[p\left(E | F\right) = p\left(E\right)\]
}

\parag{Disjoint events}{
    We say that two events $E_1$ and $E_2$ are \important{disjoint} if $E_1 \cap E_2 = \o$. Then, we have the following property: 
    \[p\left(E_1 \cup E_2\right) = p\left(E_1\right) + p\left(E_2\right)\]
     
    Note that two disjoint events \textbf{cannot} be independent.
}

\parag{Law of Total probability}{    
    If $\Omega$ is the union of disjoint events $F_1, \ldots, F_n$, then: 
    \[p\left(E\right) = p\left(E|F_1\right)p\left(F_1\right) + \ldots + p\left(E|F_n\right)p\left(F_n\right)\]

    \subparag{Visualization}{
        We can see that well with a tree:
        \svghere[0.7]{LawOfTotalProbabilityTree.svg}
    }        
}

\parag{Bayes' Rule}{
    Let's say we know $p\left(E\right), p\left(F\right)$ and $p\left(E | F\right)$, and we want $p\left(F | E\right)$. We can use the following relation to get $p\left(F | E\right)$: 
    \[p\left(F | E\right) = \frac{p\left(E |F\right)p\left(F\right)}{p\left(E\right)}\]    
}

\parag{Random variable}{
    We define \important{random variables} to be functions $X : \Omega \mapsto \mathbb{R}$, that map events to real numbers.
}

\parag{Probability distribution of a random variable}{
    $p_X\left(x\right)$ is the probability that $X = x$, i.e $p\left(E\right)$ where: 
    \[E = \left\{\omega \in \Omega : X\left(\omega\right) = x\right\}\]
    
    We can compute $p_X\left(x\right)$ in the following way:
    \[p_X\left(x\right) = \sum_{\omega \in E}^{} p\left(\omega\right)\]    
}

\parag{Intervals}{
    We wonder what is the probability that $X \in \left[a, b\right]$. We can compute it with: 
    \[\sum_{\omega \in G}^{} p\left(\omega\right), \mathspace \text{where } G = \left\{\omega \in \Omega : X\left(\omega\right) \in \left[a, b\right]\right\}\]

    We can also do it using $p_X$: 
    \[\sum_{x \in\left[a, b\right]}^{} p_X\left(x\right)\]
}

\parag{Two random variables}{
    Let $X : \Omega \mapsto \mathbb{R}$ and $Y : \Omega \mapsto \mathbb{R}$ be two random variables. In other words, we map every event to two real lines. 

    We can find the probability of an event $E_{\left(x, y\right)} = \left\{\omega \in \Omega : X\left(\omega\right) = x \text{ and } Y\left(\omega\right) = y\right\}$, denoted $p_{X, Y}\left(x, y\right)$ with: 
    \[p_{X, Y}\left(x, y\right) = \sum_{\omega \in E_{\left(x, y\right)}}^{} p\left(\omega\right)\]    
}

\parag{Marginal Distribution}{
    We can compute $p_X$ from $p_{X, Y}$: 
    \[p_X\left(x\right) = \sum_{y}^{} p_{X, Y}\left(x, y\right)\]
    
    We call $p_X$ the \important{marginal distribution} of $p_{X, Y}\left(x, y\right)$ with respect to $x$. $p_Y$ can be computed similarly.

}

\parag{Expected value}{
    The \important{Expected Value} $\exval\left[X\right]$ of a random variable $X : \Omega \mapsto \mathbb{R}$ is given by: 
    \[\exval\left[X\right] = \sum_{\omega}^{} X\left(\omega\right)p\left(\omega\right) \]
}

\parag{Theorem}{
    We can compute an expected value of a random variable $X : \Omega \mapsto \mathbb{R}$ the following way:
    \[\exval\left[X\right] = \sum_{x}^{} xp_X\left(x\right)\]    
}

\parag{Theorem: linearity of expectations}{
    Expectation is a linear operation. In other words, let $X_1, \ldots, X_n$ be random variables and $\alpha_1, \ldots, \alpha_n$ be scalars. Then: 
    \[\exval\left[\sum_{i=1}^{n} \alpha_i X_i\right] = \sum_{i=1}^{n} \alpha_i \mathbb{E}\left[X_i\right]\]
}

\parag{Definition: Independent random variables}{
    Two random variables $X$ and $Y$ are \important{independent} if and only if, for all realisations $x$ and $y$: 
    \[p\left(\left\{X = x\right\} \cap \left\{Y = y\right\}\right) = p\left(\left\{X = x\right\}\right) p\left(\left\{Y = y\right\}\right)\]
    
    More concisely, if and only if:
    \[p_{X, Y}\left(x, y\right) = p_X\left(x\right)p_Y\left(y\right) \]
    
    We can generalise this to $n$ random variables. The random variables $X_1, \ldots, X_n$ are independent if and only if: 
    \[p_{X_1, \ldots, X_n}\left(x_1, \ldots, x_n\right) = \prod_{i=1}^{n} p_{X_i}\left(x_i\right)\]
}

\parag{Definition: Conditional distribution}{
    The \important{conditional distribution} of $Y$ given $X$ is the function $p_{Y | X}$ defined by: 
    \[p_{Y|X}\left(y | x\right) = \frac{p_{X, Y}\left(x, y\right)}{p_{X\left(x\right)}}\]
}

\parag{Independence}{
    All the following sentences are equivalent:
    \begin{itemize}
        \item $X$ and $Y$ are independent.
        \item $\displaystyle p_{X, Y} = p_{X} p_Y$
        \item $\displaystyle p_{Y | X}\left(y | x\right) = p_Y\left(y\right)$ 
        \item $\displaystyle p_{X | Y}\left(x|y\right) = p_{X}\left(x\right)$
    \end{itemize}
}

\parag{Product of expected values}{
    If $X$ and $Y$ are \important{independent}, then: 
    \[\exval\left[XY\right] = \exval\left[X\right] \exval\left[Y\right]\]
}

\end{document}

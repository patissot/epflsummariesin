\documentclass[a4paper]{article}

% Expanded on 2022-04-22 at 15:16:10.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Commutative Groups}
\parag{Definition: Commutative group}{
    Let $G$ be a finite set, and $\star$ be an operation on pairs of elements of $G$, such that:
    \begin{enumerate}
        \item Closure: $\forall a, b \in G$, $a \star b \in G$
        \item Associativity: $a \star \left(b \star c\right) = \left(a \star b\right) \star c$
        \item Commutativity: $a\star b = b \star a$
        \item Existence of identity: $\exists e \in G$ such that $e \star a = a\star e = a$
        \item Existence of inverse: $\forall a \in G, \exists b \in G$ such that $a \star b = b\star a = e$
    \end{enumerate}

    Then, a \important{commutative group} (or Abelian group) is $\left(G, \star\right)$.
}


\parag{Useful examples}{
    The following are groups: 
    \[\left(\mathbb{R}, +\right), \mathspace \left(\mathbb{R} \setminus \left\{0\right\}, \cdot\right), \mathspace \left(\mathbb{C}, +\right), \mathspace \left(\mathbb{Z} / m\mathbb{Z}, +\right), \mathspace \left(\mathbb{Z} / p\mathbb{Z} \setminus \left\{\left[0\right]_p\right\}, \cdot\right)\]
    where $p$ is a prime number.
    
    However, the following are not groups: 
    \[\left(\mathbb{R}, \cdot\right), \mathspace \left(\mathbb{Z} / m\mathbb{Z}, \cdot\right), \mathspace \left(\mathbb{Z} / q \mathbb{Z} \setminus \left\{\left[0\right]_q\right\}, \cdot\right),\mathspace \left(\mathbb{N}, +\right), \mathspace \left(\mathbb{Z} \setminus \left\{0\right\}, \cdot\right)\]
    where $q$ is not a prime number. For instance, $\left(\mathbb{R}, \cdot\right)$ is not a group since $0$ does not have an inverse.
}

\parag{Definition}{
    We define $\mathbb{Z} / m\mathbb{Z}^*$ to be the set of elements of $\mathbb{Z} / m\mathbb{Z}$ that have a multiplicative inverse.    
}

\parag{Theorem}{
    For every integer $m > 1$, $\left(\mathbb{Z} / m\mathbb{Z}^*, \cdot\right)$ is a commutative group.
}

\parag{Number of elements}{
    We wonder how many elements there are in $\mathbb{Z} / m\mathbb{Z}^*$. 

    We have the following candidates: 
    \[\mathbb{Z} / m\mathbb{Z} = \left\{0, 1, \ldots, m -1\right\}\]
    
    But we have, \important{$a$ is in $\mathbb{Z} / m\mathbb{Z}^*$ if and only if $\gcd\left(a, m\right) = 1$}.
}

\parag{Definition: Euler's totient function}{
    We define \important{Euler's totient function}, $\phi\left(n\right)$, to be the number of $i \in\left\{1, 2, \ldots, n\right\}$ such that $i$ and $n$ are coprime (meaning $\gcd\left(i, n\right) = 1$).

    \subparag{Properties}{
        We have that $\phi\left(m\right)$ is the cardinality of $\mathbb{Z} / m\mathbb{Z}^*$. 
    }
}

\parag{Theorem}{
    Let $p$ be a prime number, and $k \in \mathbb{Z}_+$. Then: 
    \[\phi\left(p^k\right) = p^k - p^{k-1}\]
}

\parag{Theorem}{
    Let $p$ and $q$ be two distinct primes. Then: 
    \[\phi\left(pq\right) = \left(p-1\right)\left(q -1\right)\]
}

\parag{Definition: Regular Cartesian product}{
    Let $\mathcal{A}_1$ and $\mathcal{A}_2$ be two sets. The \important{Cartesian product} $\mathcal{A}_1 \times \mathcal{A}_2$ is the set: 
    \[\mathcal{A} = \mathcal{A}_1 \times \mathcal{A}_2 = \left\{\left(a_1, a_2\right) \text{ such that } a_1 \in \mathcal{A}_1, a_2 \in \mathcal{A}_2\right\}\]
}

\parag{Definition: Cartesian product for groups}{
    The \important{Cartesian product of two groups}, shorten the \important{product group}, $\left(G, \star\right) = \left(G_1, \star_1\right)\times \left(G_2, \star_2\right)$ is the set $G_1 \times G_2$ endowed with the binary operation $\star$, defined by: 
    \[\left(a_1, a_2\right) \star \left(b_1, b_2\right) = \left(a_1 \star_1 b_1, a_2 \star_2 b_2\right)\]

}

\parag{Theorem}{
    If $\left(G_1, \star_1\right)$ and $\left(G_2, \star_2\right)$ are commutative groups, then the product group $\left(G, \star\right) = \left(G_1, \star_1\right) \times \left(G_2, \star_2\right)$ is also a commutative group.
}

\parag{Definition: Isomorphism}{
    Two groups $\left(G, \star\right)$ and $\left(H, \otimes\right)$ are \important{isomorphic} if there exists a mapping $\psi: G \mapsto H$ such that:
    \[\psi\left(a \star b\right) = \psi\left(a\right) \otimes \psi\left(b\right), \mathspace \forall a, b \in G\]
}

\parag{Properties}{
    Let $\left(G, \star\right)$ and $\left(H, \otimes\right)$ be two groups, which are isomorphic. Then:
    \begin{itemize}
        \item If $\left(G, \star\right)$ is a commutative group, so is $\left(H, \otimes\right)$.
        \item If $e$ is the identity element of $\left(G, \star\right)$, then $\psi\left(e\right)$ is the identity element of $\left(H, \otimes\right)$.
        \item If $a$ and $b$ are inverse of each other in $\left(G, \star\right)$, then $\psi\left(a\right)$ and $\psi\left(b\right)$ are inverse of each other in $\left(H, \otimes\right)$.
    \end{itemize}
}

\parag{Theorem}{
    Let $\left(G, \star\right)$ be a finite commutative group with identity element $e$.

    For every element $a \in G$, there exists an integer $k \geq 1$ such that:
    \[\underbrace{a \star a \star \cdots \star a}_{\text{$k$ elements}} = e\]
}

\parag{Definition: Order}{
    Let $\left(G, \star\right)$ be a finite commutative group with identity element $e$, and let $a \in G$.

    The smallest positive integer $k$ such that:
    \[\underbrace{a \star a \star \cdots \star a}_{\text{$k$ elements}} = e\]
    is called the \important{order} (or period) of $a$.
}

\parag{Definition: Set of orders}{
    The set of order of each element of a group is called the \important{set of orders} of this group. Each order must divide the cardinality of the group.
}

\parag{Theorem}{
    Two groups are isomorphic if and only if they must have the same set of orders.
}

\parag{Theorem}{
    Let us consider a group $\left(G, \star\right)$, with identity element $e$.

    $a^k = e$ if and only if $k$ is a multiple of the order of $a$.
}

\parag{Lagrange's theorem}{
    Let $\left(G, \star\right)$ be a finite commutative group of cardinality $n$.

    The order of each of its element divides $n$.
}

\parag{Corollary}{
    Let $\left(G, \star\right)$ be a finite commutative group of order $n$ and identity $e$. Then, for all $a \in G$:
    \[a^n = e\]
}


\parag{Corollary: Euler's theorem}{
    Let $m \leq 2$ be an integer. Then, for all $a \in \left(\mathbb{Z} / m\mathbb{Z}^*, \cdot\right)$, we have:
    \[a^{\phi\left(m\right)} = \left[1\right]_m\]

    Equivalently, for all integers $b$ relatively prime to $m$, we have:
    \[\congruent{b^{\phi\left(m\right)}}{1}{m}\]
}

\parag{Corollary: Fermat's little theorem}{
    Let $p$ be a prime number. Then, for all $a \in \mathbb{Z} / p\mathbb{Z}$, we have:
    \[a^p = a\]

    Equivalently, for all integers $a$:
    \[\congruent{a^p}{a}{p}\]
}

\parag{Theorem: Chinese remainders}{
    If $m_1$ and $m_2$ are relatively prime, then the map defined by:
    \[\begin{split}
        \psi: \mathbb{Z} / m_1 m_2 \mathbb{Z} &\longmapsto \left(\mathbb{Z} / m_1 \mathbb{Z}\right) \times \left(\mathbb{Z} / m_2\mathbb{Z}\right) \\
        \left[k\right]_{m_1 \cdot m_2} &\longmapsto \left(\left[k\right]_{m_1}, \left[k\right]_{m_2}\right)
    \end{split}\]
    is bijective and an isomorphism with respect to $+$ and $\cdot$.

    Note that, if $m_1$ and $m_2$ are not relatively prime, then $\psi$ is neither onto nor one-to-one.

    \subparag{Intuition}{
        If $\gcd\left(m_1, m_2\right) = 1$, then this theorem states that we can compute sums and products in $\mathbb{Z} / m_1 m_2 \mathbb{Z}$ or in $\mathbb{Z} / m_1 \mathbb{Z} \times \mathbb{Z} / m_2 \mathbb{Z}$ (whichever is more convenient).
    }
}

\parag{Backward mapping}{
    We can use this method to get $\left[k\right]_{pq}$ when having $\left(\left[k_1\right]_p, \left[k_2\right]_q\right)$.

    First, we use extended Euclid's algorithm to find integers $u$ and $v$ such that:
    \[1 = \gcd\left(p, q\right) = pu + qv\]

    Let $a = qv$ and $b = pu$.

    We have that the inverse of our mapping is given by:
    \[\psi^{-1}\left(\left[k_1\right]_p, \left[k_2\right]_q\right) = \left[a k_1 + b k_2\right]_{pq}\]
}

\parag{Theorem: Mix of Chinese remainders and Fermat}{
    Let $p$ and $q$ be distinct prime numbers, and let $k$ be a multiple of both $\left(p - 1\right)$ and $\left(q - 1\right)$.

    Then, for every integer $a$ and every non-negative integer $\ell$, we have:
    \[\left(\left[a\right]_{pq}\right)^{\ell k + 1} = \left[a\right]_{pq}\]
}

\parag{Bonus: Cyclic groups}{
        If there exists a $g \in G$  such that $\left\{g, g^2, \ldots, g^{\left|G\right| - 1}, e\right\} = G$, then $\left(G, \star\right)$ is called a \important{cyclic group}. $g$ is named a \important{generator}.

        In other words, the generator has order $\left|G\right|$.

    \subparag{Theorem}{
        All cyclic groups that have the same order (meaning that $G$ has the same cardinality) are isomorphic. 

        In other words, there is only one cyclic group of order $n$.
    }
}



\end{document}

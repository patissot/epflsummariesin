\documentclass[a4paper]{article}

% Expanded on 2022-06-10 at 14:45:03.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\subsection{Finite fields}
\parag{Definition: Field}{
    A \important{field} (in French, ``un corps'') $\left(K, +, \times\right)$ is such that:
    \begin{itemize}
        \item $\left(\mathcal{K}, +\right)$ is a commutative group with identity element $0$.
        \item $\left(\mathcal{K} \setminus \left\{0\right\}, \times\right)$ is a commutative group.
        \item The operations $+$ and $\times$ follow the distributive property:
            \[a \times\left(b + c\right)= a \times b + a \times c, \mathspace \forall a, b, c \in \mathcal{K}\]
\item If $\mathcal{K}$ is finite, then $\left(\mathcal{K}, +, \times\right)$ is a \important{finite field}.

    \end{itemize}
    
    \subparag{Use}{
        Any algebraic manipulation in a finite filed behaves similarly to $\mathbb{R}$. For instance, we can solve equations, and do linear algebra using vectors and matrices.
    }
}

\parag{Useful examples}{
    The following are fields: 
    \[\left(\mathbb{R}, +, \cdot\right), \mathspace \left(\mathbb{C}, +, \cdot\right), \mathspace \left(\mathbb{Q}, +, \cdot\right)\]

    However, the following are not fields:
    \[\left(\mathbb{N}, +, \cdot\right), \mathspace \left(\mathbb{Z}, +, \cdot\right)\]
    since they both do not have a multiplicative inverse (and the first one does not even have an additive inverse).
}

\parag{Properties}{
    Let $\left(\mathcal{K}, +, \cdot\right)$ be a group with $0$ and $1$ being its additive and multiplicative identity, respectively. Then:
    \begin{enumerate}
        \item $0\cdot x = 0$ for all $x \in \mathcal{K}$.
        \item Zero-product property: $xy = 0 \implies x = 0 \lor y = 0$.
        \item For all $x \in \mathcal{K}, k \in \mathbb{Z}$, we have $x^k = 0 \implies x = 0$.
        \item $\left(-1\right)\cdot x= -x$.
        \item $\left(a + b\right)^2 = a^2 + 2ab + b^2$
    \end{enumerate}
}

\parag{Definition: Characteristic}{
    Every finite field has a multiplicative identity, let's call it 1. 

    The order of 1 with respect to $+$, is called the \important{characteristic} of the finite field.

    \subparag{Remark}{
        Let $p$ be the characteristic of a finite field. Then, for any element $a$: 
        \[\underbrace{a + a + \ldots + a}_{\text{$p$ times}} = a\left(1 + 1 + \ldots + 1\right) = a\cdot 0 = 0\]
    }
}

\parag{Theorem}{
    The characteristic of a finite field $\left(F, +, \cdot\right)$ is a prime number.
}

\parag{Definition: Isomorphism}{
    An \important{isomorphism} between two finite fields $\mathbb{F} = \left(\mathcal{F}, +, \times\right)$ and $\mathbb{K} = \left(\mathcal{K}, \oplus, \otimes\right)$ is a bijection $\phi : \mathcal{F} \mapsto \mathcal{K}$ such that, for all $a, b \in \mathcal{F}$: 
    \[\phi\left(a + b\right) = \phi\left(a\right) \oplus \phi\left(b\right)\]
    \[\phi\left(a \times b\right) = \phi\left(a\right) \otimes \phi\left(b\right)\]

    We say that $\mathbb{F}$ and $\mathbb{K}$ are \important{isomorphic} if there exists an isomorphism between them.
}

\parag{Theorem}{
    \begin{enumerate}[left=0pt]
        \item The cardinality of a finite field is an integer power of its characteristic (hence all finite fields have cardinality $p^m$ for some prime $p$ and some integer $m$).
        \item All finite fields of the same cardinality are isomorphic.
        \item For every prime number $p$ and positive integer $m$, there is a finite field of cardinality $p^m$.
    \end{enumerate}
    
    \subparag{Implication}{
        \begin{itemize}[left=0pt]
            \item There is exactly one finite field of cardinality $k$ if $k = p^m$, and none if the cardinality is not an integer power of a prime.
            \item $\left(\mathbb{Z} / k\mathbb{Z}, +, \cdot\right)$ is a finite field if and only if $k = p$ for some prime $p$.
            \item A field that has $p$ elements is isomorphic to $\left(\mathbb{Z} / p\mathbb{Z}, +, \cdot\right)$
            \item In $\left(\mathbb{Z} / p\mathbb{Z}, +, \cdot\right)$, we know how to add and multiply without tables, thus it is easier to use them for computations.
        \end{itemize}
    }
}

\subsection{Vector spaces}

\parag{Definition: Vector space}{
    $\left(\mathcal{V}, +, \cdot\right)$, where $\mathcal{V}$ is a non-empty set $\mathcal{V}$, is a \important{vector space} over a field $\mathbb{F}$ if:
    \begin{enumerate}
        \item Addition closure: $\bvec{a} + \bvec{b} \in \mathcal{V}$, $\forall \bvec{a}, \bvec{b} \in \mathcal{V}$
        \item Scalar multiplication closure: $\alpha \bvec{v} \in \mathcal{V}$ for all $\alpha \in \mathbb{F}$ and $\bvec{v} \in \mathcal{V}$.
        \item $\left(\mathcal{V}, +\right)$ is a commutative group.
        \item Associativity: $\alpha\left(\beta \bvec{v}\right) = \left(\alpha \beta\right)\bvec{v}$
        \item Identity element: there exists $1 \in \mathbb{F}$ such that $1 \bvec{v} = \bvec{v}$
        \item Distributive law: $\alpha\left(\bvec{u} + \bvec{v}\right) = \alpha \bvec{u} + \alpha \bvec{v}$
    \end{enumerate}
}

\parag{Definition: Subspace}{
    Let $V$ be a vector space. 

    A set $S \subseteq V$ is called a \important{subspace} if it is non-empty and closed under the vector space operations, meaning that:  
    \[\forall \alpha \in \mathbb{F}\ \left(\bvec{s} \in S \implies \alpha \bvec{s} \in S\right)\]
    \[\bvec{s_1}, \bvec{s_2} \in S \implies \bvec{s_1} + \bvec{s_2} \in S\]

    A subspace is a vector space.

    \subparag{Observation}{
        Note that $\bvec{0} \in S$ is a necessary condition.
    }
}

\parag{Definition: Linear combination}{
    A \important{linear combination} of a list of vectors $\left\{\bvec{v_1}, \ldots, \bvec{v_r}\right\}$ is any vector of the form: 
    \[\sum_{i=1}^{r} \lambda_i \bvec{v}_i, \mathspace \lambda_i \in \mathbb{F}\]
    
}

\parag{Definition: Span}{
    The \important{span} $S$ of a list of vectors is the set of all linear combinations we can make from the list. 
}

\parag{Definition: Finite-dimensional vector space}{
    A vector space $V$ that is the span of a list of vectors is a \important{finite-dimensional vector space}.
}

\parag{Definition: Linear independence}{
    The vectors $\left\{\bvec{v_1}, \ldots, \bvec{v_n}\right\}$ are said to be \important{linearly independent}, if: 
    \[\sum_{i=1}^{n} \lambda_i \bvec{v_i} = \bvec{0} \implies \lambda_i = 0 \text{ for all } i\]
    
    In other words, no vector can be written as a linear combination of the others.
}

\parag{Definition: Basis}{
    A \important{basis} for a finite-dimensional vector space is a list of linearly independent vectors that span $V$.
}

\parag{Theorem}{
    A list $\left\{\bvec{v_1}, \ldots, \bvec{v_n}\right\}$ is a basis \textit{if and only if} every $\bvec{v} \in V$ can be written \textit{uniquely} as a linear combination of this list $\sum_{i=1}^{n} \lambda_i \bvec{v_i}$.

}

\parag{Theorem}{
    Every spanning list in a vector space can be reduced to a basis of the vector space.
}

\parag{Theorem}{
    Any two bases of a finite-dimensional vector space have the same length.
}

\parag{Definition: Dimension}{
    The \important{dimension} of a finite-dimensional vector space $V$, denoted by $\dim\left(V\right)$, is defined to be the length of any basis of $V$.
}

\parag{Dimension: Properties}{
    Let $V$ be a vector space with $\dim\left(V\right) = n$. Then, we have the following properties.

    \begin{itemize}
        \item A list of $n$ linearly independent vectors in $V$, is a basis of $V$.
        \item A list of $n$ vectors in $V$ spanning $V$, is a basis of $V$.
        \item Any list of $m > n$ vectors in $V$ cannot be linearly independent.
        \item Any list of $m < n$ vectors cannot span $V$.
    \end{itemize}
}

\parag{Canonical basis}{
    Let $\mathbb{F}$ be a finite field with $0$ being its additive neutral element and $1$ being its multiplicative element. Then, the \important{canonical basis} of $\mathbb{F}^n$, is: 
    \[\left(\left(1, 0, 0, \ldots, 0, 0\right), \left(0, 1, 0, \ldots, 0, 0\right), \ldots, \left(0, 0, 0, \ldots, 0, 1\right)\right)\]
}

\subsection{Vector spaces over finite fields}
\parag{Lemma}{
    Let $\mathbb{F}$ be a finite field with cardinality $\left|\mathbb{F}\right|$. Then, the vector space $V = \mathbb{F}^n$ is finite and has cardinality: 
    \[\left|V\right| = \left|\mathbb{F}\right|^n\]

    \subparag{Implication}{
       $\mathbb{F}_p^n$ has cardinality $p^n$.
    }
}

\parag{Theorem}{
    A $k$-dimensional vector space $V$ over a finite field $\mathbb{F}$ is finite and has cardinality: 
    \[\left|V\right| = \left|\mathbb{F}\right|^k\]
}

\parag{Subspaces}{
    Let $S \subseteq \mathbb{F}^n$, where $\mathbb{F}$ is a finite field. This is described by either:
    \begin{enumerate}
        \item A list of vectors that span $S$.
        \item By a system of linear homogeneous equations.
    \end{enumerate}
}

\parag{Definition: Rank}{
    The number of linearly independent rows of a matrix is called the \important{rank}. 

    \subparag{Remark}{
        Note that this is equal to the number of linearly independent columns of the matrix.
    }
}

\parag{Definition}{
    A vector $\bvec{v} = \left(v_1, \ldots, v_n\right) \in \mathbb{F}^n$ \important{fulfills a system of linear homogeneous equations} with coefficients in $\mathbb{F}$ if it satisfies $\bvec{v} A^T = \bvec{0}$ where $A$ is the matrix of equations.
}

\parag{Theorem}{
    \begin{itemize}[left=0pt]
        \item The set of solutions in $\mathbb{F}^n$ of a set of $m$ homogeneous linear equations in $n$ variables and coefficients in $\mathbb{F}$ is a subspace $S$ of $\mathbb{F}^n$.
        \item Let $A$ be the coefficient matrix, and $r$ be the rank of $A$. Then, $\dim\left(S\right) = n - r$.
        \item Conversely, if $S$ is a subspace of dimension $\dim\left(S\right) = k$, then there exists a system of $m = n - k$ linear homogeneous equations with coefficients in $\mathbb{F}$, the solution of which is exactly $S$.
    \end{itemize}
}

\parag{Summary}{
    We will mostly work on $\mathbb{F}_p^n$, where $p$ is a prime number. This contains $p^n$ vectors.

    We can do everything we did in Linear Algebra: linear independence, subspaces, matrices rank, and so on. \important{The only thing we do not have is dot product.}
}








\end{document}

\documentclass[a4paper]{article}

% Expanded on 2022-04-22 at 13:57:38.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle
\section{Modulo Arithmetoc}

\parag{Euclid's Division Algorithm}{
    For all integers $a$ and $m \neq 0$, there exist unique integers $q$ (the quotient) and $r$ (the remainder) such that: 
    \[a = mq + r\]
    where $0 \leq r < \left|m\right|$
}

\parag{Mod operation}{
    By $r = a \Mod m$, we denote the remainder when dividing $a$ by $m$. Thus: 
    \[a = mq + r \implies a \Mod m = r\]
}

\parag{Congruence}{
    Two integers $a$ and $b$ are called \important{congruent} modulo $m$ if $m \divides a - b$, i.e. if $m$ divides $a - b$. This is denoted by: 
    \[\congruent{a}{b}{m}\]
    
    \subparag{Equivalent definitions}{
        All the following statements are equivalent:
        \begin{itemize}
            \item $\congruent{a}{b}{m}$
            \item $\left(a - b\right) \Mod m = 0$
            \item $a \Mod m = b \Mod m$
            \item $r_a = r_b$
        \end{itemize}
    }
}

\parag{Lemma: Equivalence relation}{
    Congruence modulo $m$ is an equivalence relation on the set of integers.
}

\parag{Theorem: Properties}{
    If $\congruent{a}{a'}{m}$ and $\congruent{b}{b'}{m}$, then:
    \begin{enumerate}
        \item $\congruent{a + b}{a' + b'}{m}$
        \item $\congruent{ab}{a'b'}{m}$
        \item $\congruent{a^n}{\left(a'\right)^n}{m}$
    \end{enumerate}

    \vspace{1em}

    An equivalent stating of this theorem is: 
    \begin{enumerate}
        \item $\left(a + b\right) \Mod m = \left(a \Mod m + b \Mod m\right) \Mod m$
        \item $\left(ab\right) \Mod m = \left(\left(a \Mod m\right)\left(b \Mod m\right)\right) \Mod m$
        \item $a^n \Mod m = \left(\left(a \Mod m\right)^n\right) \Mod m$
    \end{enumerate}

    Note that the mod does not get distributed into the $n$ of the exponent.
}

\parag{Definition: Prime number}{
    A \important{prime number} (or a \important{prime}) is an integer $> 1$ that has no positive divisors other than 1 and itself. 

    Numbers that are not prime are called \important{composite.}
}

\parag{Theorem: Prime factorisation}{
    Every integer greater than 1 has a unique prime factorisation (except for order).
}

\parag{Definition: Divide}{
    We write $a \divides b$ if $a$ divides $b$, i.e. if $\exists c \in \mathbb{Z}$ such that: 
    \[b = ac\]
    
    Equivalently in terms of prime factors: 
    \[a = p_1^{\gamma_1} \cdots p_n^{\gamma_n}\]
    \[b = p_1^{\widetilde{\gamma}_1} \cdots p_n^{\widetilde{\gamma}_n} \cdot p_{n+1}^{\widetilde{\gamma}_{n+1}} \cdots p_m^{\widetilde{\gamma}_m}\]
    where $\widetilde{\gamma}_i \geq \gamma_i$, for $i = 1, \ldots, n$.
}

\parag{Definition: GCD}{
    Let $a$ and $b$ be integers, not both zero. The largest integer that divides both is called the \important{greatest common divisor} (GCD) of $a$ and $b$. It is denoted by $\gcd\left(a, b\right)$.
}

\parag{Theorem}{
    Let $a$ and $b$ be positive integers, not both zero, and let $p_1, \ldots, p_k$ be the sequence of prime numbers that divide $a$ or $b$. We can write: 
    \[a = p_1^{\alpha_1} \cdots p_k^{\alpha_k}\]
    \[b = p_1^{\beta_1} \cdots p_k^{\beta_k}\]
    where $\alpha_i \geq 0$ and $\beta_i \geq 0$.

    Then, letting $\gamma_i = \min\left(\alpha_i, \beta_i\right)$, we have:
    \[\gcd\left(a, b\right) = p_1^{\gamma_1} \cdots p_k^{\gamma_k}\]
}

\parag{Definition: Coprime numbers}{
    Two numbers $a$ and $b$ are called \important{coprime} (or relatively prime, or mutually prime) when: 
    \[\gcd\left(a, b\right) = 1\]
    
    This is equivalent to saying that they have no common prime factor.
}

\parag{Theorem}{
    Let $p$ be a prime number, and let $a$ be an integer such that $0 < a < p$. Then: 
    \[\gcd\left(p, a\right) = 1\]
}

\parag{Properties}{
    If $ab \divides c$, then $a \divides c$ and $b \divides c$. 

    If $a \divides c$, $b \divides c$ and $\gcd\left(a, b\right) = 1$, then $ab \divides c$
}

\parag{Theorem: Infinite number of primes}{
    There are an infinite number of primes.    
}


\subsection{$\mathbb{Z}$ modulo $m$}
\parag{Definition: Congruence class}{
    Let $m \in\mathbb{Z}$, where $m > 1$. Also, let $a \in \mathbb{Z}$. We define the \important{congruence class} of $a$ mod $m$ to be: 
    \[\left[a\right]_m \over{=}{def} \left\{i \in\mathbb{Z} \text{ such that } \congruent{i}{a}{m}\right\}\]
}

\parag{Equivalences}{
    The following propositions are equivalent:
    \begin{enumerate}
        \item $\left[a\right]_m = \left[b\right]_m$
        \item $\congruent{a}{b}{m}$
        \item $a \Mod m = b \Mod m$ 
        \item $\left(a - b\right) \Mod m = 0$
    \end{enumerate}
}

\parag{Definition: $\mathbb{Z}$ mod $m$}{
    The set of all congruence classes modulo $m$, $\mathbb{Z}$ mod $m$, is denoted by $\mathbb{Z} / m\mathbb{Z}$ or $\mathbb{Z}_m$

    \subparag{Remark}{
        Every element has a \important{unique representation} in reduced form: 
        \[\left[r\right]_m, \mathspace \text{where } 0 \leq r \leq m-1\]
    }
}

\parag{Operations in $\mathbb{Z} / m\mathbb{Z}$}{
    We define the \important{sum} the following way:
    \[\left[a\right]_m + \left[b\right]_m = \left[a + b\right]_m\]
    
    Similarly, we can define the product as: 
    \[\left[a\right]_m \left[b\right]_m = \left[ab\right]_m\]
}

\parag{Proprieties of addition}{
    \begin{itemize}[left=0pt]
        \item Associativity: 
        \[\left(\left[a\right]_m + \left[b\right]_m\right) + \left[c\right]_m = \left[a\right]_m + \left(\left[b\right]_m + \left[c\right]_m\right) = \left[a\right]_m + \left[b\right]_m + \left[c\right]_m\]
            
            
        \item Commutativity:
        $\left[a\right]_m + \left[b\right]_m = \left[b\right]_m + \left[a\right]_m$  
        \item Existence of additive identity: 
        $\left[a\right]_m + \left[0\right]_m = \left[0\right]_m + \left[a\right]_m = \left[a\right]_m$
        
        \item Existence of additive inverse:
        $\left[-a\right]_m + \left[a\right]_m = \left[0\right]_m$

    \end{itemize}
}

\parag{Properties of multiplication}{
    \begin{itemize}[left=0pt]
        \item Associativity: 
            \[\left(\left[a\right]_m \cdot \left[b\right]_m\right) \cdot \left[c\right]_m = \left[a\right]_m \cdot \left(\left[b\right]_m \cdot \left[c\right]_m\right) = \left[a\right]_m \cdot \left[b\right]_m \cdot \left[c\right]_m\]
            
        \item Commutativity: 
        $\left[a\right]_m \cdot \left[b\right]_m = \left[b\right]_m \cdot \left[a\right]_m$
            
        \item Existence of multiplicative identity:
        $\left[a\right]_m \cdot \left[1\right]_m = \left[1\right]_m \cdot \left[a\right]_m = \left[a\right]_m$
    \end{itemize}

    However, there is not always a multiplicative inverse.
}

\parag{Distributivity}{
    We also have the following property: 
    \[\left[a\right]_m \left(\left[b\right]_m + \left[c\right]_m\right) = \left[a\right]_m \left[b\right]_m + \left[a\right]_m \left[c\right]_m\]
}

\parag{Definition}{
    Let $k \in \mathbb{N}$, where $k \geq 1$. We define the following notation: 
    \[k\left[a\right]_m = \underbrace{\left[a\right]_m + \ldots + \left[a\right]_m}_{\text{$k$ times}}\]
    
    \subparag{Remark}{
        We also have: 
        \[k\left[a\right]_m = \left[k\right]_m \left[a\right]_m = \left[ka\right]_m\]
    }
}

\parag{Definition: Multiplicative inverse}{
    If it exists, the multiplicative inverse of $\left[a\right]_m$ is an element $\left[b_m\right]$ such that:
    \[\left[a\right]_m \left[b\right]_m = \left[b\right]_m \left[a\right]_m = \left[1\right]_m\]
}

\parag{Theorem: Unicity of the multiplicative inverse}{
    If it exists, the multiplicative inverse is unique. 
}

\parag{Definition: Power}{
    Let $k \in \mathbb{N}^*$. We define: 
    \[\left(\left[a\right]_m\right)^k = \underbrace{\left[a\right]_m \cdots \left[a\right]_m}_{\text{$k$ times}}\]
    
    When $k = 0$, we define: 
    \[\left(\left[a\right]_m\right)^{0} = \left[1\right]_m\]
    
    \subparag{Remark}{
        Avoid working with negative powers since this is very dangerous: inverses do not always exist.
    }
}

\parag{Proposition}{
    Let $\left[a\right]_m$ be a congruence class which has an inverse. Then, there exist no number $k \in \mathbb{N}$ such that: 
    \[\left(\left[a\right]_m\right)^k = 0\]    
}

\parag{Theorem}{
    In $\mathbb{Z} / m\mathbb{Z}$, the following three propositions are equivalent:
    \begin{enumerate}
        \item $\left[a\right]_m$ has an inverse.
        \item For all $\left[b\right]_m$, $\left[a\right]_m x = \left[b\right]_m$ has a unique solution.
        \item There exists a $\left[b\right]_m$ such that $\left[a\right]_m x = \left[b\right]_m$ has a unique solution
    \end{enumerate}
}

\parag{Lemma}{
    The following identity holds: 
    \[\gcd\left(a, b\right) = \gcd\left(a - kb, b\right)\]
}

\parag{Euclid algorithm}{
    We can write $a = bq + r$, where $0 \leq r < b$. Thus, from our lemma: 
    \[\gcd\left(a, b\right) = \gcd\left(b, r\right) = \gcd\left(b, a \Mod b\right)\]
    
    We can continue that way recursively.
}

\parag{Theorem: Bézout's identity}{
    Let $a$ and $b$ be integers, not both zero. 

    Then, there exist integers $u$ and $v$ (possibly negative), such that: 
    \[\gcd\left(a, b\right) = au + bv\]
}

\parag{Theorem}{
    $\left[a\right]_m$ has a multiplicative inverse \textit{if and only if} $a$ and $m$ are coprime (meaning that $\gcd\left(a, m\right) = 1$).    

    \subparag{Finding the inverse}{
        We suppose by hypothesis that $\gcd\left(a, m\right) = 1$.

        By Bézout identity, we know $\exists u,v$ such that: 
        \[1 = au + mv \implies \left[1\right]_m = \left[au\right]_m + \underbrace{\left[mv\right]_m}_{= 0} = \left[a\right]_m \left[u\right]_m\]
        since $mv$ is a multiple of $m$.

        We have found our inverse, $\left[u\right]_m$.
    }
}

\parag{Corollary 1}{
    $\gcd\left(a, m\right) = 1$ if and only if there exist integers $u$ and $v$ such that $1 = au + mv$.
}

\parag{Corollary 2}{
    If $p$ is a prime, all elements of $\mathbb{Z} / p\mathbb{Z}$, except for $\left[0\right]_p$, have a multiplicative inverse.
}

\subsection{Interesting applications}


\parag{Procedure mod 97--10}{
    This algorithm allows us to check easily if two numbers have been swapped in a number.

    We start with a number $n$. We multiply our number by 100, getting $100n$, compute $r = 100n \Mod 98$, then add a check $c = 98 - r$ to our number multiplied by 100. In other words, we compute:
    \[m = 100n + \left(98 - 100n \Mod 97\right)\]

    Then, to check if our number was modified, we can verify that the result modulo 97 is 1. In other words, the test passes if:
    \[\left[\text{number with check digits}\right]_{97} = \left[1\right]_{97}\]
   
    \subparag{Remark}{
        We know that $97$ is the largest prime under 100, this is why we picked it.      
    }
}

\parag{IBAN}{
    An IBAN is constructed the following way. The first 5 numbers represent the bank, the next 12 the account, the next 4 the country (we convert the two letters of the country, CH for Switzerland for instance, using $A \mapsto 10, \ldots, Z \mapsto 35$, leading to 1217 for CH), and then do a MOD97--10 procedure for the last two digits. We finally replace the four digits of the country by its letters, and reposition the two letters and the two digits of the MOD97--10 procedure to the front, leading to, for instance: 
    \[\text{CH}{\color{red}54}\,00243\,0001\,2345\,6789\]

    This means that, we can verify the user did not make a mistake writing his or her IBAN thanks to the MOD97--10 procedure. With our example, we can verify that: 
    \[00243\,0001\,2345\,6789\,{\color{red}1217\,54} \Mod 97 = 1\]
}


\end{document}

\documentclass[a4paper]{article}

% Expanded on 2022-04-21 at 11:09:31.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}


\begin{document}
\maketitle

\section{Source Coding}
\subsection{Entropy}


\parag{Definition: Entropy}{
    Let's define by convention that $p_S\left(s\right) \log\left(p_S\left(s\right)\right) = 0$ when $p_S\left(s\right) = 0$. Then we can define entropy as:

    \[H_b\left(S\right) = -\sum_{s \in A}^{} p_S\left(s\right) \log_b p_S\left(s\right)\]
    
    We can also see it the following way: 
    \[H\left(S\right) = \exval\left[-\log\left(p_S\left(S\right)\right)\right]\]

    \subparag{Unit}{
        The choice of the base $b$ determines the unit. Typically, $b = 2$ leads to bits.
    }
        
    \subparag{Intuition}{
        Entropy can be seen as a quantity indicating how random a source is.
    }
}

\parag{Uniform distribution}{
    The entropy of a uniform distribution $p_S\left(s\right) = \frac{1}{\left|A\right|}$ is $H_b\left(S\right) = \log_b \left|A\right|$.
}

\parag{Binary entropy function}{
    Special case when $\left|A\right| = 2$. 

    $p_S$ has only two possible values, $p$ and $\left(1 - p\right)$. This corresponds to the entropy: 
    \[H\left(S\right) = h\left(p\right) = -p\log_2\left(p\right) - \left(1 - p\right) \log_2\left(1 - p\right)\]

    We can draw the function:
    \svghere[0.5]{BinaryEntropyFunction.svg}

    We call $h\left(p\right)$ the \important{binary entropy function}.
}

\parag{Lemma: IT-inequality}{
    Let $r \in \mathbb{R}_+$. Then: 
    \[\log_b\left(r\right) \leq \left(r - 1\right) \log_b\left(e\right)\]
    with equality if and only if $r = 1$.
    
    \subparag{Intuition}{
        We can get some intuition using the following graph:
        \svghere[0.8]{ITInequalityGraph.svg}
    }
}

\parag{Theorem: Entropy bound}{
    The entropy of a discrete random variable $S \in A$ satisfies: 
    \[0 \leq H_b\left(S\right) \leq \log_b\left|A\right|\]
    with equality on the left if and only if $p_S\left(s\right) = 1$ for some $s$ (certainty), an with equality on the right if and only if $p_S\left(s\right) = \frac{1}{\left|A\right|}$ for all $s$ (uniform distribution).
}

\parag{Two random variables}{
    We can also define entropy for two random variables: 
    \[H\left(X, Y\right) = -\sum_{\left(x, y\right) \in \mathcal{X} \times \mathcal{Y}}^{} p_{X,Y}\left(x, y\right) \log\left(p_{X,Y}\left(x,y\right)\right)\]

    We can also define it using expected values: 
    \[H\left(X, Y\right) = \exval\left[-\log\left(p_{X,Y}\left(X, Y\right)\right)\right]\]
}

\parag{Theorem}{
    Let $S_1, \ldots S_n$ be discrete random variables. Then: 
    \[H\left(S_1, \ldots, S_n\right)\leq H\left(S_1\right) + \ldots+ H\left(S_n\right)\]
    with equality if and only if $S_1, \ldots, S_n$ are independent.
}

\subsection{Coding}

\parag{Definition}{
    The \important{encoder} is specified by the input alphabet $\mathcal{A}$, the output alphabet $\mathcal{D}$ (typically $\mathcal{D} = \left\{0, 1\right\}$ since computers love binary), the codebook $\mathcal{C}$ which consists of finite sequences over $\mathcal{D}$, by the one-to-one encoding map $\Gamma : \mathcal{A}^k \mapsto C$, where $k$ is a positive integer (for now we will take $k = 1$).
}

\parag{Definition: Prefix-free codes}{
    If no codeword is a prefix of another codeword, the code is said to be \important{prefix-free}.


    \subparag{Terminology}{
        A prefix-free code is also called \important{instantaneous code}.
    }
}

\parag{Definition: uniquely decodable}{
    A code is \important{uniquely decodable} if every concatenation of codewords has a unique parsing into a sequence of codewords.
    
    \subparag{Rermarks}{
        \begin{itemize}[left=0pt]
            \item A prefix-free code is uniquely decodable.
            \item A code is uniquely decodable if and only if its reverse (the reverse of 100 is 001) is uniquely decodable.

        \end{itemize}
    }
}

\parag{Complete trees}{
    Let's consider all binary sequences of length less than or equal to $\ell_{max}$. We can draw a tree representing it, and then our code in it:
    \imagehere[0.2]{CompleteTreeA.png}

    We can do that for any $D$-ary sequences of length less than or equal to $\ell_{max}$.

    \subparag{Observation}{
        We can see that, for a prefix-free code, then there is no letter before another (there is no letter on the path between the root and any path). Recall that code $\Gamma_B$ is prefix free, whereas $\Gamma_C$ is not.
        \imagehere{CompleteTreeBC.png}
    }
}

\parag{Decoding trees}{
    We can remove nodes that are not necessary to make a decoding tree. For example:
    \imagehere[0.35]{DecodingTreeB.png}
}

\parag{Definition: Codeword length}{
    We define the \important{codeword length} to be the size of the codeword (such a beautiful definition).

    We want the average codeword length to be as small as possible.
}

\parag{Theorem: Kraft-McMillan part 1}{
    If a $D$-ary code is uniquely decodable, then its codeword lengths $\ell_1, \ldots, \ell_M$ satisfy: 
    \[D^{-\ell_1} + \ldots + D^{-\ell_M} \leq 1\]

    This is called the Kraft inequality.
    
    \subparag{Contrapositive}{
        If a $D$-ary code's codeword lengths do no satisfy $D^{-\ell_1} + \ldots + D^{-\ell_M} \leq 1$, then this code is not uniquely decodable.
    }    
}

\parag{Theorem: Kraft-McMillan part 2}{
    If the positive integers $\ell_1, \ldots, \ell_M$ satisfy Kraft's inequality for some positive integer $D$, there exists a $D$-ary prefix-free code (hence uniquely decodable) that has codeword lengths $\ell_1, \ldots, \ell_M$.
}

\parag{Definition: Average Codeword Length}{
    Let $\ell \left(\Gamma\left(s\right)\right)$ be the length of the codeword associated to $s \in \mathcal{A}$. The \important{average codeword-length} is: 
    \[L\left(S, \Gamma\right) = \sum_{s \in \mathcal{A}}^{} p_S\left(s\right) \ell \left(\Gamma\left(s\right)\right)\]
}

\parag{Theorem}{
    Let $\Gamma : \mathcal{A} \mapsto \mathcal{C}$ be the encoding map of a $D$-ary code for the random variable $S \in \mathcal{A}$. 

    If the code is uniquely decodable, then: 
    \[H_{D}\left(S\right) \leq L\left(S, \Gamma\right)\]
    with equality if and only if $p_s\left(s\right) = D^{-\ell\left(\Gamma\left(s\right)\right)}$ for all $s \in \mathcal{A}$. 

    Note that an equivalent condition is $\ell\left(\Gamma\left(s\right)\right) = \log_D\left(\frac{1}{p_S\left(s\right)}\right)$ for all $s \in \mathcal{A}$
    
    \subparag{Remark}{
        If $D = 2$, then we have equality if and only if all $p_i$ are of the form $\frac{1}{2^k}$ for some $k \in \mathbb{Z}_+$.
    }
}

\parag{Theorem}{
    For every random variable $S \in \mathcal{A}$ and every integer $D \geq 2$, there exists a prefix-free $D$-ary code for $S$ such that, for all $s \in \mathcal{A}$:
    \[\ell\left(\Gamma\left(s\right)\right) = \left\lceil \log_D\left(\frac{1}{p_S\left(s\right)}\right) \right\rceil\]

    Such codes are called \important{$D$-ary Shannon-Fano codes}.

    Note that these codes aren't optimal nor unique.
}

\parag{Theorem}{
    The average codeword-length of a $D$-ary Shannon-Fano code for the random variable $S$ fulfills: 
    \[H_D\left(S\right) \leq L\left(S, \Gamma_{SF}\right) < H_D\left(S\right) + 1\]
}

\parag{Optimal Code: Huffman Code}{
    Let's start from the leaf. We merge the two least likely, compute the sum of the probability, and treat this as a new symbol. Then, we can start again recursively:
    \imagehere{HuffmanCodeTreeExample.png}

    When we have our tree, we can put our 0 and 1 where we want on the tree, but it's better to be consistent for simplicity.

    They are also not unique. We can choose where we put 0s and 1s when our tree is built, but can also sometimes choose which probabilities to link together (if three of them are the lowest, we can pick the two that we want).
}

\parag{Path-length lemma}{
    The average path length of a tree with probabilities is the sum of probabilities of the intermediate nodes (root included): 
    \[\sum_{i}^{} p_i \ell_i = \sum_{j}^{} q_j\]
    where $p_i$ is the probability of the $i$\Th leaf, $\ell_i$ the distance of the $i$\Th leaf, and $q_j$ the probability of the $j$\Th node (except for leafs).

    \subparag{Example}{
        \imagehere[0.25]{HuffmanCodeTreeExample2.png}
        In this picture right before, we can compute: 
        \[\sum_{i}^{} p_i \ell_i = 0.2 + 0.6 + 0.4 + 1 = 2.2\]
        which is very fast to compute.
    }
}

\parag{Theorem: Huffman Code optimality}{
    Huffman code are optimal. In other words, if $\Gamma_H$ is a Huffman code (which is prefix-free by construction) and $\Gamma$ is another uniquely decodable code for the same source $S$, then it is guaranteed that: 
    \[L\left(S, \Gamma_H\right) \leq L\left(S, \Gamma\right)\]
}


\parag{D-ary Huffman Code}{
    Generally, in the Huffman strategy, we reduce the alphabet by $D - 1$ symbols every step. Moreover, at the end, we want exactly $D$ symbols, meaning we need $D + k\left(D - 1\right)$ symbols to begin with, where $k \in \mathbb{Z}$. Thus, we can fake it by adding a bunch of symbols having probability 0. Doing this with a ternary tree for exemple:
    \svghere[0.5]{HuffmanCodeTernaryRight.svg}

    This is the optimal strategy.
}

\parag{Corollary}{
    Since we know that Huffman is more optimal than the Shannon-Fano code (since it is the most optimal code) and that it is bounded below by entropy as all codes, we also have the following inequality for Huffman codes:

\[H_D\left(S\right) \leq L\left(S, \Gamma_{H}\right) \leq L\left(S, \Gamma_{SF}\right) < H_D\left(S\right) + 1\]
}

\subsection{Conditional entropy}
\parag{Theorem}{
    The per-letter average codeword length of a $D$-ary Shannon-Fano code for the random variables $\left(S_1, S_2\right)$ fulfills:
    \[\frac{H_D\left(S_1, S_2\right)}{2} \leq \frac{L\left(\left(S_1, S_2\right), \Gamma_{SF}\right)}{2} \leq \frac{H_D\left(S_1, S_2\right) + 1}{2}\]

    If $S_1$ and $S_2$ are independent and have the same distribution, we get $H_{D}\left(S_1, S_2\right)= 2H_D\left(S\right)$. Thus:
    \[H_D\left(S\right) \leq \frac{L\left(\left(S_1, S_2\right), \Gamma_{SF}\right)}{2} < H_D\left(S\right) + \frac{1}{2}\]

    More generally, with $n$ symbols:
    \[H_D\left(S\right) \leq \frac{L\left(\left(S_1, \ldots, S_n\right), \Gamma_{SF}\right)}{n} < H_D\left(S\right) + \frac{1}{n}\]
}

\parag{Theorem}{
    The per-letter average codeword-length of a $D$-ary Shannon-Fano code for the random variable $\left(S_1, \ldots, S_n\right)$ fulfills:
    \[\frac{H_D\left(S_1, \ldots, S_n\right)}{n} \leq \frac{L\left(\left(S_1, \ldots, S_n\right), \Gamma_{SF}\right)}{n} < \frac{H_D\left(S_1, \ldots, S_n\right)}{n} + \frac{1}{n}\]
}

\parag{Definition}{
    The conditional expectation of $X$ given $Y = y$ is defined as:
    \[\exval\left[X |Y = y\right] \over{=}{def} \sum_{x \in X}^{} xp_{X|Y}\left(x | y\right)\]
}


\parag{Definition: Condition entropy}{
    The conditional entropy of $X$ given $Y = y$ is defined as:
    \[H_D\left(X|Y=y\right) \over{=}{def} \sum_{x \in \mathcal{X}}^{} p_{X|Y}\left(x|y\right) \log_D\left(p_{X|Y}\left(x|y\right)\right)\]
}

\parag{Theorem}{
    The conditional entropy of a discrete random variable $X \in \mathcal{X}$conditioned on $Y = y$ satisfies:
    \[0 \leq H_D\left(X | Y = y\right) \leq \log_D\left|\mathcal{X}\right|\]
    with equality on the left if and only if the probability distribution is degenerate (there is 100\% chance of an event happening and 0\% for the others) and equality on the right if and only if the probability distribution is uniform.
}

\parag{Definition}{
    We define the \important{conditional entropy} of $X$ given $Y$ is defined as:
    \[H_D\left(X|Y\right) \over{=}{def} \sum_{y \in \mathcal{Y}}^{} p_Y\left(y\right) H\left(X|Y=y\right) = \sum_{y \in \mathcal{Y}}^{} p_Y\left(y\right) \left[-\sum_{x \in \mathcal{X}}^{} p_{X|Y}\left(x|y\right) \log_D\left(p_{X|Y}\left(x|y\right)\right)\right]\]
}

\parag{Theorem}{
    The conditional entropy of a discrete random variable $X \in \mathcal{X}$ conditioned on $Y$ satisfies:
    \[0 \leq H_D\left(X|Y\right) \leq \log_D\left|\mathcal{X}\right|\]
    with equality on the left if and only if for every $y$ there exists an $x$ such that $p_{X|Y}\left(x|y\right) = 1$, and with equality if and only if $p_{X|Y}\left(x|y\right) = \frac{1}{\left|\mathcal{X}\right|}$ for all $x$ and $y$.
}

\parag{Theorem}{
    For any two discrete random variables $X$ and $Y$, we have:
    \[H_D\left(X|Y\right) \leq H_D\left(X\right)\]
    with equality if and only if $X$ and $Y$ are independent random variables.

    \subparag{Intuition}{
        \textit{Note bene}, as we have seen, this is not true point-wise. We may have $H_D\left(X | Y=y\right) > H_D\left(X\right)$ for some values of $y$.
    }
}

\parag{Theorem: Chain rule for entropy}{
    Let $X$ and $Y$ be two random variables. Then:
    \[H_D\left(X, Y\right) = H_D\left(X\right) + H_D\left(Y|X\right)\]

}

\parag{Theorem: Chain rule for entropies}{
    Let $S_1, \ldots, S_n$ be random variables. Then: 
    \[H_D\left(S_1, \ldots, S_n\right) = H_D\left(S_1\right) + H_D\left(S_2 | S_1\right) + \ldots + H_D\left(S_n|S_1, \ldots, S_{n-1}\right)\]
    
    \subparag{Remark}{
        We have that: 
        \[H\left(S_4 | S_3, S_2, S_1\right) = H\left(S_4 | S_1, S_2, S_3\right)\]
        Which means we can use this theorem multiple times with the same values and get useful information.
        
    }
}

\parag{Theorem}{
    Let $X$ be an arbitrary random variable. Let $f\left(X\right)$ be a (deterministic) function of $X$. Then: 
    \[H\left(f\left(X\right) | X\right) = 0\]
}

\parag{Theorem}{
    Let $S_1, \ldots, S_n$ be discrete  random variables. Then: 
    \[H\left(S_1, \ldots, S_n\right) \leq H\left(S_1\right) + \ldots + H\left(S_n\right)\]
    with equality if and only if $S_1, \ldots, S_n$ are independent.    
}

\parag{Corollary}{
    We get the two following inequalities: 
    \[H\left(X, Y\right) \geq H\left(X\right)\] 
    \[H\left(X, Y\right) \geq H\left(Y\right)\]
}

\subsection{Random processes}
\parag{Proposition}{
    Let $X, Y, Z$ be three random variables. Then:
    \[p\left(X, Y, Z\right) = p\left(X\right)p\left(Y|X\right)p\left(Z|X, Y\right)\]    
}

\parag{Definition}{
    We define the \important{entropy of a symbol} as:
    \[H\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n\right)\]

    We also define the \important{entropy rate of the source} $\mathcal{S}$:
    \[H^*\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n | S_1, \ldots, S_{n-1}\right)\]

    The source is said to be \important{regular} if both exist and are finite.    
}

\parag{Coin flip source}{
    The source models a sequence $S_1, \ldots, S_n$ of $n$ coin flips. So, $S_i \in \mathcal{A} = \left\{H, T\right\}$ for $i = 1, 2, \ldots, n$.

    We know that $p_{S_i}\left(H\right) = p_{S_i}\left(T\right) = \frac{1}{2}$ for all $i$, and that coinflips are independent. Thus we have: 
    \[p_{S_1, \ldots, S_n} \left(s_1, \ldots, s_n\right) = \frac{1}{2^{n}}, \mathspace \forall \left(s_1, \ldots, s_n\right) \in \mathcal{A}^{n}\]
   
}

\parag{Coin flip source entropy}{
    \[p_{S_i}\left(s_i\right) = \frac{1}{2} \implies H\left(S_i\right) = \SI{1}{\bit}, \mathspace \forall i\]
    \[p_{S_i | S_1, \ldots, S_{i-1}}\left(s_i | s_1, \ldots, s_{i-1}\right) = \frac{1}{2} \implies H\left(S_i | S_{1}, \ldots, S_{i-1}\right) = \SI{1}{\bit}, \mathspace \forall i\]

    Thus, we know that: 
    \[H\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n\right) = \SI{1}{\bit}\]
    \[H^*\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n | S_1, \ldots, S_{n-1}\right) = \SI{1}{\bit}\]

    Which means that this source is regular.

}


\parag{Sunny-rainy source}{
    Let's say that we know that the weather, which can be sunny or rainy, only depends on the day right before. The weather has probability $q$ of not changing, and $1 - q$ of switching. The weather of the first day is chosen uniformly.

    We have: 
    \begin{functionbypart}{p_{S_i | S_1, \ldots S_{i-1}} \left(s_i | s_1, \ldots, s_{i-1}\right) = p_{S_i |S_{i-1}} \left(s_i |s_{i-1}\right)}
    q, \mathspace s_i = s_{i-1} \\
    1 - q, \mathspace s_i \neq s_{i-1}
    \end{functionbypart}
    
    Very generally, we have: 
    \[p_{S_i}\left(s_i\right) = \frac{1}{2}\]

    Note that $S_i$ and $S_{i-1}$ are not independent, neither are $S_i$ and $S_{i-2}$.    
    Moreover, if we set $q = \frac{1}{2}$, we get back to the coin-flipping source.
}

\parag{Sunny-rainy source entropy}{
    In the sunny-rainy source, we know that: 
    \[p_{S_i}\left(s_i\right) = \frac{1}{2} \implies H\left(S_i\right) = \SI{1}{\bit}, \mathspace \forall i\]

    We also have:
    \[H\left(S_i | S_1, \ldots, S_{i-1}\right) = H\left(S_i | S_{i-1}=s_{i-1}\right)\]
    And 
    \[H\left(S_i | S_{i-1} = 0\right) = h\left(q\right) = H\left(S_i | S_{i-1} = 1\right)\]
    Thus
    \[H\left(S_i | S_{i-1}\right) = p\left(S_{i-1} = 1\right) h\left(q\right) + p\left(S_{i-1} = 0\right)h\left(q\right) = h\left(q\right)\]
 
    Entropy of a symbol and the entropy rate of the source:
    \[H\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n\right) = \SI{1}{\bit}\]
    \[H^*\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n | S_1, \ldots, S_{n-1}\right) = h\left(q\right)\]

    Which means that this source is also regular.
}

\parag{Green-blue source}{
    This source models a sequence $S_1, \ldots, S_n$ of a person's votes from the alphabet $\mathcal{A} = \left\{G, B\right\}$. The first vote is chosen uniformly in $\mathcal{A}$, and the next votes are identical to the initial vote.

    We have that: 
    \[p_{S_n}\left(s_n\right) = \frac{1}{2}\]
    
    Indeed, we do not know anything, and the $n$\Th vote is defined by the first vote, with probability $\frac{1}{2}$.
}

\parag{Green-blue source entropy}{
    The entropy of a symbol is 1: 
    \[H\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n\right) = \lim_{n \to \infty} 1 = 1\]
    

    The entropy rate is clearly 0, since $p_{S_n | S_{n-1}}$ is a degenerate random distribution:
    \[H^*\left(\mathcal{S}\right) = \lim_{n \to \infty} H\left(S_n | S_{n-1}\right) = \lim_{n \to \infty} 0 = 0\]
    
    We deduce that the source is regular. Note that this is the sunny-rainy source with $q = 0$.

}

\parag{Weekly-coin-flip source}{
    The source models a sequence $S_1, S_2, \ldots$ of coin flips $\mathcal{A} = \left\{H, T\right\}$ such that: 
    \[p_{S_{i + 7k}}\left(T\right) = \frac{1}{i}, \mathspace i \in \left\{1, \ldots, 7\right\}, k \in \mathbb{N}\]
    
    Thus, for example: 
    \[p_{S_1}\left(T\right) = 1 = p_{S_8}\left(T\right)\]
    \[p_{S_2}\left(T\right) = \frac{1}{2}\]

    This is not regular, since the symbol entropy does not converge. On Mondays, the symbol entropy is $\SI{0}{\bit}$, on Tuesday it is $\SI{1}{\bit}$ and so on. It clearly oscillates.
}


\parag{Stationary sources}{
    Let us say that $\left(S_1, \ldots, S_n\right)$ are distributed according to $p_{S_1, \ldots, S_n}\left(x_1, \ldots, x_n\right)$, which we can write as: 
    \[\left(S_1, \ldots, S_n\right) \sim p_{S_1, \ldots, S_n}\left(x_1, \ldots, x_n\right)\]
    
    We can also see have a shifted version: 
    \[\left(S_{k+1}, \ldots, S_{k+n}\right) \sim p_{S_{k+1}, \ldots, S_{k+n}}\left(x_1, \ldots, x_n\right)\]
    
    We call the source to be \important{stationary} if for all $n$ and $k$: 
    \[p_{S_1, \ldots, S_n}\left(x_1, \ldots, x_n\right) = p_{S_{k+1}, \ldots, S_{k+n}}\left(x_1, \ldots, x_n\right), \mathspace \forall x_1, \ldots, x_n\]
    
    \subparag{Intuition}{
        Basically, the idea is that if we look at a local information of a stationary source, we have no information on where we are.
    }
    
    \subparag{Implication}{
        A stationary source has the following properties: 
        \[p_{S_1} = p_{S_m}, \mathspace \forall m\]
        \[p_{S_1, S_2} = p_{S_m, S_{m+1}}, \mathspace \forall m\]
        \[p_{S_1, S_2, S_3} = p_{S_m, S_{m+1}, S_{m+2}}, \mathspace \forall m\]
        \[p_{S_m, S_n} = p_{S_{m+k}, S_{n+k}}\]

        Thus, for any subset $\mathcal{I}$ of indices (even indices that do not follow each other ($\mathcal{I} = \left\{0, 3, 7\right\}$ for instance)): 
        \[p_{S_\mathcal{I}} = p_{S_{\mathcal{I} + k}}\]
    }

    Note that the coin-flip, the sunny-rainy and the green-blue sources are stationary, but not the weekly-coin-flip.
}

\parag{Theorem: Conditioning reduces entropy 2}{
    For any three discrete random variables $X, Y$ and $Z$: 
    \[H_D\left(X|Y, Z\right) \leq H_D\left(X | Z\right)\]
    with equality if and only if $X$ and $Y$ are conditionally independent random variables given $Z$ (meaning $p\left(x, y|z\right) = p\left(x|z\right)p\left(y|z\right)$ for all $x, y, z$).
}

\parag{Theorem}{
    A stationary source is regular.

    \subparag{Converse}{
        Note that the converse is not true: a regular source is not necessarily stationary.
    }
}

\parag{Theorem}{
    For a stationary source, we have:
    \[H^*\left(\mathcal{S}\right) \leq H\left(\mathcal{S}\right)\]
    with equality if and only if the symbols are independent.
}

\parag{Theorem: Cesàro means}{
    Let $a_1, a_2, \ldots$ be a real-valued sequence and let $c_1, c_2, \ldots$ be the sequence of running average defined by:
    \[c_n = \frac{a_1 + \ldots + a_n}{n}\]
    
    If $\lim_{n \to \infty} a_n$ exists, then $c_n$ converges as well, and: 
    \[\lim_{n \to \infty} c_n = \lim_{n \to \infty} a_n\]
}

\parag{Theorem}{
    For a stationary source, we have: 
    \[\lim_{n \to \infty} \frac{H_D\left(S_1, \ldots, S_n\right)}{n} = H^*\left(\mathcal{S}\right)\]
    
    Moreover, $a_n = \frac{H_D\left(S_1, \ldots, S_n\right)}{n}$ is a non-increasing sequence.
}

\parag{Fundamental theorem of Source Coding}{
    For the compression of a source $S_1, S_2, \ldots$ the fundamental limit is:
    \[\lim_{n \to \infty} \frac{H_D\left(S_1, \ldots, S_n\right)}{n} = H_D^*\left(\mathcal{S}\right)\]
    if this limit exists (if it does not, then we do not know anything).

    In other words, there exist uniquely decodable codes which average codeword-length per symbol gets arbitrarily close to $H^*_D\left(\mathcal{S}\right)$ (such as the Huffman code), and no uniquely decodable code can go below.
}

\end{document}

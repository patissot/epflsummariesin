\documentclass[a4paper]{article}

% Expanded on 2022-06-10 at 11:18:50.

\usepackage{../../../style}

\title{Résumé d'AICC 2 \\ \large Tiré des notes de cours de Joachim Favre}
\author{Paul Tissot-Daguette}

\begin{document}
\maketitle

\section{Error detection and error correction codes}
\subsection{Introduction}

\parag{Introduction}{
    We have a channel which takes an input and gives an output, which is not generally the same. The result can have some bit flipped, some bit added or removed, and so on.

    \svghere[0.8]{ChannelSchema.svg}
}

\parag{Channel}{
    We will consider the two following channels:

    \subparag{Erasure channel}{
        An erasure channel takes a sequence of bits as input, and maps it to some sequence where some of the bits were replaced by a question mark. For instance, we could have: 
        \[0100111 \mapsto 0{\color{red}?}001{\color{red}?}1\]
        
        Examples: Disks and flash drives, package dropped by routers, flash drives dying in data centers.
    }
    
    \subparag{Error channel}{
        This channel takes a sequence of bits as input, and flips some of them. For instance, we could have: 
        \[0100111 \mapsto {\color{red}1}10{\color{red}10}11\]
        
        The difference with the erasure channel is that we do not know which bits are incorrect.

        Example: Electromagnetic noise in telecommunication
    }
}

\parag{Definition: Erasure weight}{
    The number of erasures done in an erasure channel is the \important{erasure weight}, $p$. 
}

\parag{Definition: Error weight}{
    The number of errors done in an error channel is the \important{error weight}, $p$.
}

\subsection{Channel coding}
\parag{Key idea: Channel coding}{
    To be able to do channel coding, we will have an encoder before the channel and a decoder after it.

    The goal of the decoder is to do \important{error detection} (detecting that errors or erasures happened on the channel) and \important{error correction} (fix the error or erasure that happened).
}

\parag{Terminology (binary case)}{
    \begin{itemize}[left=0pt]
        \item The \important{code} is the set of all codewords: 
        \[\mathcal{C} \subseteq \left\{0, 1\right\}^n\]
        \item $n$ is called the \important{block length}.
        \item $k$ is the \important{number of information symbols}, the number of symbols we are really transmitting. This can be computed by doing:
        \[k = \log_2 \left|\mathcal{C}\right|\]
        \item The \important{rate} of the code is: 
        \[R = \frac{k}{n}\]
        This represents the amount of redundancy we have.
    \end{itemize}
}

\parag{Terminology (general case)}{
    \begin{itemize}[left=0pt]
        \item The \important{code} is the set of all codewords: 
        \[\mathcal{C} \subseteq \mathcal{A}^n\]
        \item $n$ is called the \important{block length}.
        \item $k$ is the \important{number of information symbol}, the number of symbols we are really use. This is defined to be:
        \[k \over{=}{def}  \log_{\left|\mathcal{A}\right|} \left|\mathcal{C}\right|\]
        \item The \important{rate} of the code is: 
        \[R = \frac{k}{n}\]
        This represents the amount of redundancy we have.
    \end{itemize}
}

\parag{Definition: Hamming distance}{
    Let $x$ and $y$ be sequences of the same length.

    Then, the \important{hamming distance} between $x$ and $y$, $d\left(x, y\right)$, is the number of places where $x$ and $y$ differ.

}

\parag{Remark: Distance}{
    In math, a function of two variables $x$ and $y$ is a \important{distance}, if it satisfies: 
    \begin{enumerate}
        \item $d\left(x, y\right) \geq 0, \forall x, y$
        \item $d\left(x, y\right) = 0 \iff x = y$
        \item Symmetry: $d\left(x, y\right) = d\left(y, x\right)$ 
        \item Triangle inequality (taking a detour cannot be faster than the direct path): 
            \[d\left(x, y\right) \leq d\left(x, z\right) + d\left(z, y\right)\]
    \end{enumerate}
}

\parag{Theorem}{
    The Hamming distance is a distance.
}

\parag{Definition: Minimum distance decoding}{
    For any code $\mathcal{C}$ and for any channel yielding $y$, the Minimum Distance (MD)-Decoder outputs:
    \[\hat{c} \in \arg \min_{x \in \mathcal{C}} d_H\left(y, x\right)\]

    The arg maps back to the codeword. We are having a set since there might be multiple minimums.

    In other words, the decoder makes a table, measuring the distance between what it gets and all the possible codewords. Then, it just picks the one with the smallest Hamming distance.

}

\parag{Definition: Minimum distance of a code}{
    Let's say we have a code $\mathcal{C}$. Then, the \important{minimum distance} of this code, is defined as:
    \[d_{min}\left(\mathcal{C}\right) \over{=}{def} \min_{\substack{x, y \in \mathcal{C} \\ x \neq y}} d_H\left(x, y\right)\]
}

\parag{Theorem: Erasure channel}{
    Let's consider an erasure channel.

    If the erasure weight is such that $p < d_{min}\left(\mathcal{C}\right)$, then the MD-decoder is guaranteed to find the correct codeword.

    \subparag{Remark}{
        The greatest $p$, $p_{max}$, such that for any erasure weight $p \leq p_{max}$, we can be certain that the correct codeword can be found, is given by: 
        \[p_{max} = d_{min} - 1\]
    }
    
}

\parag{Theorem: Error channel}{
    Let's consider an error channel.

    If the error weight is such that $p < \frac{d_{min}\left(\mathcal{C}\right)}{2}$, then the MD-decoder is guaranteed to find the correct codeword.

    
    \subparag{Remark 1}{
        The greatest $p$, $p_{max}$, such that for any error weight $p \leq p_{max}$, we can be certain that the correct codeword can be found, is given by: 
        \[p_{max} = \left\lfloor \frac{d_{min} - 1}{2} \right\rfloor\]
    }

    \subparag{Remark 2}{
        The greatest $p$, $p_{max}$, such that for any error weight $p \leq p_{max}$, we can be certain that an error \textit{can be detected} is given by:
        \[p_{max} = d_{min} - 1\]

        Indeed, if $p \leq d_{min} - 1$, then we cannot mistake what we get with another codeword.
    }
    
}

\parag{Observation}{
    We want $d_{min}\left(\mathcal{C}\right)$ to be large in a code, since, in both channel, the larger it is, the surer we are to get the good result. Then, we also want $n$ to be small, since it would mean less to transmit; and we want to have $\left|\mathcal{C}\right|$ to also be large. Those two last conditions are equivalent to wanting $R = \frac{k}{n}$ to be large.

    We want both $d_{min}\left(\mathcal{C}\right)$ and $R$ to be large, but there is some tension between them: increasing them decreases the other. 
}

\parag{Theorem: Singleton's bound}{
    For any code $\mathcal{C}$, regardless of the alphabet $\mathcal{A}$, we have:
    \[d_{min}\left(\mathcal{C}\right) \leq n -k + 1\]
    where $n$ is the block length and $k = \log_{\left|\mathcal{A}\right|}\left(\left|\mathcal{C}\right|\right)$.

}

\parag{Definition: MDS code}{
    A code $\mathcal{C}$ that attains Singleton's Bound with equality is called \important{Maximum Distance Separable (MDS) code}.
}

\subsection{Linear codes}

\parag{Definition: Linear code}{
    A code $\mathcal{C} \subseteq \mathbb{F}^n$ is \important{linear} if $\mathcal{C}$ is a subspace of $\mathbb{F}^n$.

    \subparag{Terminology}{
        We call a $\left(n, k\right)$ linear block code over $\mathbb{F}_p$ to be a $k$-dimensional subspace of $\mathbb{F}_p^n$.
    }
    
}

\parag{Definition: Dimension}{
    The \important{dimension} of a linear code $\mathcal{C} \subseteq \mathbb{F}^n$ is the dimension of the subspace $\mathcal{C}.$
}

\parag{Lemma}{
    The number of codewords in a linear code $\mathcal{C} \subseteq \mathbb{F}_q^n$ must be of the form $q^k$ for some $k \in \left\{0, 1, 2, \ldots, n\right\}$ (where $q$ must be a prime power, meaning it is of the form $q = p^m$ for $p$ being a prime number and $m \in \mathbb{Z}_+$).
}

\parag{Definition: Hamming weight}{
    Let $\bvec{x} \in \mathbb{F}^n$. The \important{Hamming weight} of $\bvec{x}$ is:
    \[w\left(\bvec{x}\right) = d\left(\bvec{x}, \bvec{0}\right)\]

    In other words, $w\left(\bvec{x}\right)$ is the number of non-zero positions in $\bvec{x}$.

    
    \subparag{Property}{
        We have $w\left(\bvec{x} - \bvec{u}\right)= d\left(\bvec{x}, \bvec{u}\right)$.
    }
}

\parag{Theorem}{
    Let $\mathcal{C} \subseteq \mathbb{F}^n$ be a linear code. Then: 
    \[d_{min}\left(\mathcal{C}\right) = \min_{\bvec{x} \in \mathcal{C} \setminus \left\{\bvec{0}\right\}} w\left(\bvec{x}\right)\]
}

\parag{Definition: Generator matrix}{
    We know a linear code can be described by a basis $\left\{\bvec{c_1}, \ldots, \bvec{c_k}\right\}$. 

    We define the following matrix to be the \important{generator matrix} of the code:
    \[G = \begin{pmatrix} & & \bvec{c_1} & & \\ & & \vdots & & \\  & & \bvec{c_k} &  & \end{pmatrix} \in \mathbb{F}^{k \times n} \]

    A generator matrix of a code $\mathcal{C}$ specifies an encoding map:
    \[\begin{split}
    f: \mathbb{F}^k &\longmapsto \mathbb{F}^n \\
    \bvec{u} &\longmapsto \bvec{c} = \bvec{u} G
    \end{split}\]
    where $\bvec{u}$ gives the information symbols.

    The basis is not unique, and thus the generator matrix is not unique.
    
}

\parag{Number of generator matrices}{
    For a certain code $\mathcal{C} \in \mathbb{F}_q^n$ of dimension $k$, there are 
    \[\left(q^k - 1\right)\left(q^k - q\right)\left(q^k - q^2\right)\cdots\left(q^k - q^{k-1}\right)\]   
    different generator matrices.
}

\parag{Definition: Echelon form}{
    A matrix is said to be of \important{echelon form} if it is of the form: 
    \[\begin{pmatrix} \star & \times & \times & \times & \times & \times \\ 0 & \star & \times & \times & \times & \times \\ 0 & 0 & 0 & \star & \times & \times \\ 0 & 0 & 0 & 0 & \star & \times \end{pmatrix} \]
    where the $\times$ represent any number and the $\star$ represent non-zero numbers, named pivots.

    A matrix is said to be of \important{reduced echelon form} if it is of the form:
    \[\begin{pmatrix} 1 & 0 & \times & 0 & 0 & \times \\ 0 & 1 & \times & 0 & 0 & \times \\ 0 & 0 & 0 & 1 & 0 & \times \\ 0 & 0 & 0 & 0 & 1 & \times \end{pmatrix}\]
}

\parag{Theorem}{
    Any matrix can be transformed into a unique reduced echelon form through the Gaussian elimination algorithm.

    \subparag{Remark}{
        Since the line space of a matrix in echelon form is equal to the line space of the matrix, we can use the        Gaussian elimination algorithm to turn our generator matrix into another matrix generating the same space        but which may be easier to use.
    }
}

\parag{Definition: Systematic generator matrices}{
    A \important{systematic generator matrix} is a generator matrix such that its first $k$ columns represent the identity matrix.
    \[G = \begin{pmatrix}  &  &  &  &  &  \\  & I_k &  &  & P &  \\  &  &  &  &  &  \end{pmatrix} \]

    The identity matrix takes $k$ columns, and the matrix $P$, sometimes called the parity check, takes $n-k$ columns. Such a matrix is in its reduced echelon form.    
}

\parag{Definition: Parity-check matrix}{
    A \important{parity-check matrix} $H$ for a linear $\left(n, k\right)$ block code $\mathcal{C}$ is an $\left(n-k\right)\times n$ matrix that contains the coefficients of the $n-k$ linear homogenous equations, the solutions to which construct $\mathcal{C}$.

    This tells us $H \in \mathbb{F}^{\left(n-k\right) \times n}$ is such that:
    \[\bvec{y} H^T = \bvec{0} \iff \bvec{y} \in \mathcal{C}\]
}

\parag{Lemma}{
    Let $G$ and $H$ be full rank matrices (their rank is the highest it can be) and the right dimension.

    $\left(G, H\right)$ are generator/parity-check matrices if and only if: 
    \[GH^T = 0\]
}

\parag{Theorem: Systematic code}{
    Let's say we have the following systematic generator matrix: 
    \[G = \begin{pmatrix}  &  &  &  &  &  \\  & I_k &  &  & P &  \\  &  &  &  &  &  \end{pmatrix} \]
    
    Then, the parity-check matrix is given by: 
    \[H = \begin{pmatrix}  &  &  &  &  &  \\  & -P^T &  &  & I_{n-k} &  \\  &  &  &  &  &  \end{pmatrix} \]    
}

\parag{Definition: Syndrome}{
    Let $\mathcal{C} \subseteq \mathbb{F}^n$  be an $\left(n, k\right)$ linear block code with parity check matrix $H$.

    The vector $\bvec{s} = \bvec{y} H^T \in \mathbb{F}^{n-k}$ is the \important{syndrome} of $\bvec{y}$.

    By definition of the parity-check matrix: 
        \[\bvec{s} = \bvec{0} \iff \bvec{y} \in \mathcal{C}\]

    \subparag{Remark}{
        We have $\bvec{s} = \bvec{y} H^T = \bvec{e} H^T$

        Thus, if there is only one error $\bvec{e} = \bvec{e_i}$
        then $\bvec{s}$ is the $i$\Th column of $H^T$.
    }
}

\parag{Definition: Hamming code}{
    Let $n = 2^m - 1 \in \left\{1, 3, 7, 15, 31, \ldots\right\}$ and $k = n - m$ for some $m \in \mathbb{Z}_+$. A $\left(n, k\right)$ linear code $\mathcal{C} \subseteq \mathbb{F}_2^n$ is named a \important{Hamming code} if the binary representation of every number between $1$ and $2^m$ are represented in the columns exactly once (their order does not matter).
    
    Any Hamming code has $d_{min} = 3$ and is MDS.
}

\parag{Theorem: Minimum distance via parity check matrix}{
    Let $\mathcal{C} \subseteq \mathbb{F}_q^n$ be a linear $\left(n, k\right)$ code and $H$ be a parity check matrix for $\mathcal{C}$. 

    Then, $d_{min}\left(\mathcal{C}\right)$ is the smallest number of linearly dependent columns of $H$.        
}

\subsection{Decoding based on cosets and syndromes}

\parag{Equivalence relation}{
    Let $\left(G, \star\right)$ be a group and $\left(H, \star\right)$ be a subgroup. 

    We define $a \sim b$, for $a, b \in G$, if $b = a \star h$ for some $h \in H$. The equivalence class of $a \in G$ is: 
    \[\left[a\right] = \left\{b : a \sim b\right\} = \left\{a \star h, h \in H\right\} = a + H\]
    
    We call this the \important{coset} of $H$ with respect to $a$.

    \subparag{Properties}{
        \begin{enumerate}[left=0pt]
            \item Every $b \in G$ is in exactly one coset.
            \item All cosets have the same cardinality.
            \item Cosets are either equal or fully distinct. 
            \item The union of all cosets is exactly $G$.
        \end{enumerate}

        In other words, cosets partition the set $G$ in equal shares.
    }
}

\parag{Application to linear codes}{
    Let $G = \left(\mathbb{F}_q^n, +\right)$ and $H = \mathcal{C}$, where $\mathcal{C}$ is a $\left(n, k\right)$ linear block code over $\mathbb{F}_q$. We know that: 
    \[\left|\mathcal{C}\right| = q^k = M\]
    
    The coset of any $\bvec{x} \in \mathcal{C}$ is: 
    \[\left[\bvec{x}\right] = \left\{\bvec{y} : \bvec{y}= \bvec{x} + \bvec{c}\right\} = \bvec{x} + \mathcal{C}\]
    
    All the codewords belong to the coset $\left[\bvec{0}\right] = \mathcal{C}$.
}

\parag{Generalisation}{
    Let's draw the following table, where each $\bvec{t_i}$ is chosen such that no two lines are the same:
    \begin{center}
    \begin{tabular}{c|c|ccc}
        $\bvec{x}$ & Coset of $\mathcal{C}$ with respect to $\bvec{x}$ & $D_0$ & $\cdots$ & $D_{M-1}$\\
        \hline
        $\bvec{0}$ & $\mathcal{C}$ & $\bvec{c_0}$ & $\cdots$ & $\bvec{c_{M-1}}$ \\
        $\vdots$ & $\vdots$ & $\vdots$ & & $\vdots$ \\
        $\bvec{t_{L-1}}$ & $\bvec{t_{L-1}} + \mathcal{C}$ & $\bvec{t_{L-1}} + \bvec{c_0}$ & $\cdots$ & $\bvec{t_{L-1}} + \bvec{c_{M-1}}$
    \end{tabular}
    \end{center}

    We have $M = q^k$, and $L = q^{n-k}$, so that $ML = \left|\mathbb{F}^n\right| = q^n$.
    
    Let's define the set of elements in the $i$\Th column to be $D_i$, and call the $\bvec{t_i}$ the coset leaders. We can pick any element of the line as its leader
}

\parag{Coset decoder}{
    We receive $\bvec{y} \in \mathbb{F}_q^n$. We first identify the coset of $\bvec{y}$. This has a leader, let's call it $\bvec{t_i}$. Then, we output $\bvec{y} - \bvec{t_i}$.
}

\parag{Theorem}{
    Choosing the leader of each coset to be one of the ones with the smallest Hamming weight, leads the coset decoder to be a minimum distance decoder. 
}

\parag{Theorem}{
    Two $\bvec{y} \in \mathbb{F}^n$ have the same syndrome if and only if they are in the same coset.
}

\parag{Summary}{
    We can sum up the syndrome decoder as the following algorithm.

    \begin{enumerate}
        \item Pre-compute and store the syndrome of each coset leader.
        \item Receive $\bvec{y} \in \mathbb{F}_q^n$, and compute its syndrome $\bvec{s} = \bvec{y} H^T$.
        \item Find $\bvec{t}$ by looking at the lookup table, knowing that its syndrome is also $\bvec{s}$.
        \item Output $\bvec{y} - \bvec{t}$.
    \end{enumerate}

    A useful observation is that the coset leaders are the errors we are able to correct.
}

\parag{Conclusion}{
    Even though the syndrome decoding is conceptually interesting, it is still to expensive because of the lookup table. For better results, see Reed-Solomon codes.
}

\subsection{Error probability}

\parag{Binary symmetric channel}{
   We are working over the field $\mathbb{F}_2$. We say that every bit has a flip probability $\epsilon$ (supposedly small), and thus a non-flip probability of $1 - \epsilon$. This is called the binary symmetric channel (BSC). 
    Note that the flips are independent.

    \imagehere[0.7]{BinarySymmetricChannel.png}
}

\parag{Linear codes}{
    Now, let us use a linear code $\mathcal{C}$. Let's define $P_C\left(i\right)$ to be the probability that the Minimum-Distance decoder decodes correctly if the transmitted codeword is $\bvec{c_i}$.

    It is given by 
    \[P_C\left(j\right) = P_C\left(0\right) = \sum_{\bvec{e} \in D_0}^{} \epsilon^{w\left(e\right)} \left(1 - \epsilon\right)^{n - w\left(\bvec{e}\right)}\ = \sum_{\bvec{y} \in D_0}^{} \left(\frac{\epsilon}{1 - \epsilon}\right)^{w\left(\bvec{y}\right)} \left(1 - \epsilon\right)^n, \mathspace \forall j\]
}

\subsection{Polynomials over $\mathbb{F}_q$}
\parag{Definition}{
    Let $\bvec{u} = \left(u_1, \ldots, u_m\right) \in \mathbb{F}_q^m$ be a vector. 

    We define the following polynomial: 
    \[p_{\bvec{u}}\left(x\right) = u_1 + u_2 x + \ldots + u_m x^{m-1}, \mathspace \forall x \in \mathbb{F}_q\]
}

\parag{Definition: Degree of a polynomial}{
    The \important{degree of a polynomial} is the highest exponent $i$ for which $x^i$ has a non-zero coefficient.

    By convention, we define the degree of the polynomial $0$ to be $-\infty$: 
    \[\deg\left(0\right) = -\infty\]
}

\parag{Polynomial interpolation}{
    Lagrange's interpolation allows us, given the set of points $\left\{\left(a_1, y_1\right), \ldots, \left(a_k, y_k\right)\right\}$, where $\left(a_i, y_i\right) \in \mathbb{F}_q^2$, to find a polynomial such that it goes through all those points.
    In other words, we can find a polynomial such that
    \[p_{\bvec{u}}\left(a_i\right) = y_i, \mathspace i = 1, \ldots, k\]

    Firstly we define $Q_i\left(x\right)$, a polynomial with coefficients in $\mathbb{F}_q$ such that
                \begin{functionbypart}{Q_i\left(x\right)}
                    1, \mathspace \text{when } x = a_i \\
                    0, \mathspace \text{when } x = a_j \text{ for } j \neq i
                \end{functionbypart}

    It is given by
    \[Q_i\left(x\right) = \frac{\prod_{j \neq i}^{} \left(x - a_j\right)}{\prod_{j\neq i}^{} \left(a_i - a_j\right)}\]
    
    Secondly and finally, we get our polynomial with the following expression:
    \[p\left(x\right) = \sum_{i=1}^{k} y_i Q_i\left(x\right)\]

    This is the best polynomial if we want to minimise the degree, which will be at most $k - 1$.
}

\parag{Definition: Roots of a polynomial}{
    The \important{roots} of a polynomial $p\left(x\right)$ over $\mathbb{F}_q$ are numbers $b \in \mathbb{F}_q$ such that: 
    \[p\left(b\right) = 0\]
}

\parag{Fundamental theorem of algebra}{
    Let $p\left(x\right)$ be a polynomial over $\mathbb{F}$ of degree at most $m-1$, meaning $\deg\left(p\left(x\right)\right) \leq m -1$.

    If this is not the all-zero polynomial ($\exists x \in \mathbb{F} : p\left(x\right) \neq 0$), then the number of its distinct roots is at most $m - 1$. 
}

\subsection{Reed-Solomon codes}
\parag{Reed-Solomon codes}{
    The Reed-Solomon codes (RS codes) are constructed using the following algorithm:
    \begin{enumerate}
        \item We choose a finite field $\mathbb{F} = \mathbb{F}_q$ (where $q$ is a prime power).
        \item We choose $n$ and $k$ such that:  
        \[0 < k \leq n \leq q\]
        \item We choose $n$ \textit{distinct} elements: 
        \[a_1, \ldots, a_n \in \mathbb{F}\]
        \item The codeword associated to the information word $\bvec{u} \in \mathbb{F}^k$ is given by the following mapping: 
            \[\bvec{u} \mapsto \bvec{c} = \left(p_{\bvec{u}}\left(a_1\right), \ldots, p_{\bvec{u}}\left(a_n\right)\right) \in \mathbb{F}\]
    \end{enumerate}
    
    We notice that $p_{\bvec{u}}\left(x\right)$ is a polynomial of degree $\deg\left(p_{\bvec{u}}\left(x\right)\right) \leq k - 1$.
}

\parag{Properties}{
    \begin{enumerate}[left=0pt]
        \item RS codes are linear.
        \item RS codes have dimension $k$, meaning they have $q^k$ different codewords.
        \item RS codes are MDS, meaning $d_{min}\left(\mathcal{C}\right) = n - k + 1$.
    \end{enumerate}
}

\parag{Decoding}{
    To decode $\bvec{y}$, which was not modified by the channel, we can use a Lagrange interpolation to find $\bvec{u}$ back.
}

\parag{Generator matrix for RS codes}{
    The generator matrix of an RS code with elements $a_1, a_2, \ldots, a_n$ is given by    
 
    \[G = \begin{pmatrix} 1 & 1 & \cdots & 1 \\ a_1 & a_2 & \cdots & a_n \\ \vdots & \vdots & \ddots & \vdots \\ a_{1}^{k-1} & a_2^{k-2} & \cdots & a_n^{k-1} \end{pmatrix} \]



    }


\end{document}

